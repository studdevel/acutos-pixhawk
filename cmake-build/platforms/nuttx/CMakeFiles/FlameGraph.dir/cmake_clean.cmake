file(REMOVE_RECURSE
  "CMakeFiles/FlameGraph"
  "CMakeFiles/FlameGraph-complete"
  "../../external/Stamp/FlameGraph/FlameGraph-install"
  "../../external/Stamp/FlameGraph/FlameGraph-mkdir"
  "../../external/Stamp/FlameGraph/FlameGraph-download"
  "../../external/Stamp/FlameGraph/FlameGraph-update"
  "../../external/Stamp/FlameGraph/FlameGraph-patch"
  "../../external/Stamp/FlameGraph/FlameGraph-configure"
  "../../external/Stamp/FlameGraph/FlameGraph-build"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/FlameGraph.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
