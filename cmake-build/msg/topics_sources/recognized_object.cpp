/****************************************************************************
 *
 *   Copyright (C) 2013-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/* Auto-generated by genmsg_cpp from file recognized_object.msg */


#include <cinttypes>
#include <px4_log.h>
#include <px4_defines.h>
#include <uORB/topics/recognized_object.h>
#include <drivers/drv_hrt.h>
#include <lib/drivers/device/Device.hpp>

constexpr char __orb_recognized_object_fields[] = "uint64_t timestamp;float center_x;float center_y;float[6] border_x;float[6] border_y;uint8_t num_of_border_points;uint8_t[7] _padding0;";

ORB_DEFINE(recognized_object, struct recognized_object_s, 65, __orb_recognized_object_fields);


void print_message(const recognized_object_s& message)
{
	PX4_INFO_RAW(" recognized_object_s\n");
	if (message.timestamp != 0) {
		PX4_INFO_RAW("\ttimestamp: %" PRIu64 "  (%.6f seconds ago)\n", message.timestamp, hrt_elapsed_time(&message.timestamp) / 1e6);
	} else {
		PX4_INFO_RAW("\n");
	}
	PX4_INFO_RAW("\tcenter_x: %.4f\n", (double)message.center_x);
	PX4_INFO_RAW("\tcenter_y: %.4f\n", (double)message.center_y);
	PX4_INFO_RAW("\tborder_x: [%.4f, %.4f, %.4f, %.4f, %.4f, %.4f]\n", (double)message.border_x[0], (double)message.border_x[1], (double)message.border_x[2], (double)message.border_x[3], (double)message.border_x[4], (double)message.border_x[5]);
	PX4_INFO_RAW("\tborder_y: [%.4f, %.4f, %.4f, %.4f, %.4f, %.4f]\n", (double)message.border_y[0], (double)message.border_y[1], (double)message.border_y[2], (double)message.border_y[3], (double)message.border_y[4], (double)message.border_y[5]);
	PX4_INFO_RAW("\tnum_of_border_points: %u\n", message.num_of_border_points);
	
}
