# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/terehin/build/px4_1/src/modules/mavlink/mavlink.c" "/home/terehin/build/px4_1/cmake-build/src/modules/mavlink/CMakeFiles/modules__mavlink_original.dir/mavlink.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "CONFIG_ARCH_BOARD_PX4FMU_V5"
  "MODULE_NAME=\"mavlink_original\""
  "PX4_MAIN=mavlink_app_main"
  "__DF_NUTTX"
  "__PX4_NUTTX"
  "__STDC_FORMAT_MACROS"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "NuttX/nuttx/include/cxx"
  "NuttX/nuttx/include"
  "."
  "src"
  "src/lib"
  "src/modules"
  "../src"
  "../src/drivers/boards/px4fmu-v5"
  "../src/include"
  "../src/lib"
  "../src/lib/DriverFramework/framework/include"
  "../src/lib/matrix"
  "../src/modules"
  "../src/ulet"
  "../src/platforms"
  "NuttX/nuttx/arch/arm/src/armv7-m"
  "NuttX/nuttx/arch/arm/src/chip"
  "NuttX/nuttx/arch/arm/src/common"
  "NuttX/apps/include"
  "external/Install/include"
  "../mavlink/include/mavlink"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/terehin/build/px4_1/src/modules/mavlink/mavlink_command_sender.cpp" "/home/terehin/build/px4_1/cmake-build/src/modules/mavlink/CMakeFiles/modules__mavlink_original.dir/mavlink_command_sender.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/mavlink/mavlink_ftp.cpp" "/home/terehin/build/px4_1/cmake-build/src/modules/mavlink/CMakeFiles/modules__mavlink_original.dir/mavlink_ftp.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/mavlink/mavlink_high_latency2.cpp" "/home/terehin/build/px4_1/cmake-build/src/modules/mavlink/CMakeFiles/modules__mavlink_original.dir/mavlink_high_latency2.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/mavlink/mavlink_log_handler.cpp" "/home/terehin/build/px4_1/cmake-build/src/modules/mavlink/CMakeFiles/modules__mavlink_original.dir/mavlink_log_handler.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/mavlink/mavlink_main.cpp" "/home/terehin/build/px4_1/cmake-build/src/modules/mavlink/CMakeFiles/modules__mavlink_original.dir/mavlink_main.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/mavlink/mavlink_messages.cpp" "/home/terehin/build/px4_1/cmake-build/src/modules/mavlink/CMakeFiles/modules__mavlink_original.dir/mavlink_messages.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/mavlink/mavlink_mission.cpp" "/home/terehin/build/px4_1/cmake-build/src/modules/mavlink/CMakeFiles/modules__mavlink_original.dir/mavlink_mission.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/mavlink/mavlink_orb_subscription.cpp" "/home/terehin/build/px4_1/cmake-build/src/modules/mavlink/CMakeFiles/modules__mavlink_original.dir/mavlink_orb_subscription.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/mavlink/mavlink_parameters.cpp" "/home/terehin/build/px4_1/cmake-build/src/modules/mavlink/CMakeFiles/modules__mavlink_original.dir/mavlink_parameters.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/mavlink/mavlink_rate_limiter.cpp" "/home/terehin/build/px4_1/cmake-build/src/modules/mavlink/CMakeFiles/modules__mavlink_original.dir/mavlink_rate_limiter.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/mavlink/mavlink_receiver.cpp" "/home/terehin/build/px4_1/cmake-build/src/modules/mavlink/CMakeFiles/modules__mavlink_original.dir/mavlink_receiver.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/mavlink/mavlink_shell.cpp" "/home/terehin/build/px4_1/cmake-build/src/modules/mavlink/CMakeFiles/modules__mavlink_original.dir/mavlink_shell.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/mavlink/mavlink_simple_analyzer.cpp" "/home/terehin/build/px4_1/cmake-build/src/modules/mavlink/CMakeFiles/modules__mavlink_original.dir/mavlink_simple_analyzer.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/mavlink/mavlink_stream.cpp" "/home/terehin/build/px4_1/cmake-build/src/modules/mavlink/CMakeFiles/modules__mavlink_original.dir/mavlink_stream.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/mavlink/mavlink_timesync.cpp" "/home/terehin/build/px4_1/cmake-build/src/modules/mavlink/CMakeFiles/modules__mavlink_original.dir/mavlink_timesync.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/mavlink/mavlink_ulog.cpp" "/home/terehin/build/px4_1/cmake-build/src/modules/mavlink/CMakeFiles/modules__mavlink_original.dir/mavlink_ulog.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CONFIG_ARCH_BOARD_PX4FMU_V5"
  "MODULE_NAME=\"mavlink_original\""
  "PX4_MAIN=mavlink_app_main"
  "__DF_NUTTX"
  "__PX4_NUTTX"
  "__STDC_FORMAT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "NuttX/nuttx/include/cxx"
  "NuttX/nuttx/include"
  "."
  "src"
  "src/lib"
  "src/modules"
  "../src"
  "../src/drivers/boards/px4fmu-v5"
  "../src/include"
  "../src/lib"
  "../src/lib/DriverFramework/framework/include"
  "../src/lib/matrix"
  "../src/modules"
  "../src/ulet"
  "../src/platforms"
  "NuttX/nuttx/arch/arm/src/armv7-m"
  "NuttX/nuttx/arch/arm/src/chip"
  "NuttX/nuttx/arch/arm/src/common"
  "NuttX/apps/include"
  "external/Install/include"
  "../mavlink/include/mavlink"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
