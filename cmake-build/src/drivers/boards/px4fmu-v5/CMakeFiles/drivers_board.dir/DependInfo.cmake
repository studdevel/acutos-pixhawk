# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/terehin/build/px4_1/src/drivers/boards/px4fmu-v5/init.c" "/home/terehin/build/px4_1/cmake-build/src/drivers/boards/px4fmu-v5/CMakeFiles/drivers_board.dir/init.c.obj"
  "/home/terehin/build/px4_1/src/drivers/boards/px4fmu-v5/led.c" "/home/terehin/build/px4_1/cmake-build/src/drivers/boards/px4fmu-v5/CMakeFiles/drivers_board.dir/led.c.obj"
  "/home/terehin/build/px4_1/src/drivers/boards/px4fmu-v5/manifest.c" "/home/terehin/build/px4_1/cmake-build/src/drivers/boards/px4fmu-v5/CMakeFiles/drivers_board.dir/manifest.c.obj"
  "/home/terehin/build/px4_1/src/drivers/boards/px4fmu-v5/sdio.c" "/home/terehin/build/px4_1/cmake-build/src/drivers/boards/px4fmu-v5/CMakeFiles/drivers_board.dir/sdio.c.obj"
  "/home/terehin/build/px4_1/src/drivers/boards/px4fmu-v5/spi.c" "/home/terehin/build/px4_1/cmake-build/src/drivers/boards/px4fmu-v5/CMakeFiles/drivers_board.dir/spi.c.obj"
  "/home/terehin/build/px4_1/src/drivers/boards/px4fmu-v5/timer_config.c" "/home/terehin/build/px4_1/cmake-build/src/drivers/boards/px4fmu-v5/CMakeFiles/drivers_board.dir/timer_config.c.obj"
  "/home/terehin/build/px4_1/src/drivers/boards/px4fmu-v5/usb.c" "/home/terehin/build/px4_1/cmake-build/src/drivers/boards/px4fmu-v5/CMakeFiles/drivers_board.dir/usb.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "CONFIG_ARCH_BOARD_PX4FMU_V5"
  "MODULE_NAME=\"drivers_board\""
  "__DF_NUTTX"
  "__PX4_NUTTX"
  "__STDC_FORMAT_MACROS"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "NuttX/nuttx/include/cxx"
  "NuttX/nuttx/include"
  "."
  "src"
  "src/lib"
  "src/modules"
  "../src"
  "../src/drivers/boards/px4fmu-v5"
  "../src/include"
  "../src/lib"
  "../src/lib/DriverFramework/framework/include"
  "../src/lib/matrix"
  "../src/modules"
  "../src/ulet"
  "../src/platforms"
  "NuttX/nuttx/arch/arm/src/armv7-m"
  "NuttX/nuttx/arch/arm/src/chip"
  "NuttX/nuttx/arch/arm/src/common"
  "NuttX/apps/include"
  "external/Install/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/terehin/build/px4_1/cmake-build/msg/CMakeFiles/uorb_msgs.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake-build/src/lib/drivers/led/CMakeFiles/drivers__led.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake-build/src/lib/parameters/CMakeFiles/parameters.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake-build/src/lib/drivers/device/CMakeFiles/drivers__device.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake-build/src/lib/cdev/CMakeFiles/cdev.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake-build/src/drivers/boards/common/CMakeFiles/drivers_boards_common.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake-build/src/drivers/boards/common/stm32/CMakeFiles/drivers_boards_common_arch.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake-build/src/lib/parameters/flashparams/CMakeFiles/flashparams.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake-build/src/modules/systemlib/CMakeFiles/systemlib.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake-build/src/drivers/stm32/CMakeFiles/drivers_arch.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake-build/src/lib/perf/CMakeFiles/perf.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake-build/src/lib/parameters/tinybson/CMakeFiles/tinybson.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
