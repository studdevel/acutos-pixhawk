# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/terehin/build/px4_1/src/lib/mixer/mixer_load.c" "/home/terehin/build/px4_1/cmake-build/src/lib/mixer/CMakeFiles/mixer.dir/mixer_load.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "CONFIG_ARCH_BOARD_PX4FMU_V5"
  "__DF_NUTTX"
  "__PX4_NUTTX"
  "__STDC_FORMAT_MACROS"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "NuttX/nuttx/include/cxx"
  "NuttX/nuttx/include"
  "."
  "src"
  "src/lib"
  "src/modules"
  "../src"
  "../src/drivers/boards/px4fmu-v5"
  "../src/include"
  "../src/lib"
  "../src/lib/DriverFramework/framework/include"
  "../src/lib/matrix"
  "../src/modules"
  "../src/ulet"
  "../src/platforms"
  "NuttX/nuttx/arch/arm/src/armv7-m"
  "NuttX/nuttx/arch/arm/src/chip"
  "NuttX/nuttx/arch/arm/src/common"
  "NuttX/apps/include"
  "external/Install/include"
  "src/lib/mixer"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/terehin/build/px4_1/src/lib/mixer/mixer.cpp" "/home/terehin/build/px4_1/cmake-build/src/lib/mixer/CMakeFiles/mixer.dir/mixer.cpp.obj"
  "/home/terehin/build/px4_1/src/lib/mixer/mixer_group.cpp" "/home/terehin/build/px4_1/cmake-build/src/lib/mixer/CMakeFiles/mixer.dir/mixer_group.cpp.obj"
  "/home/terehin/build/px4_1/src/lib/mixer/mixer_helicopter.cpp" "/home/terehin/build/px4_1/cmake-build/src/lib/mixer/CMakeFiles/mixer.dir/mixer_helicopter.cpp.obj"
  "/home/terehin/build/px4_1/src/lib/mixer/mixer_multirotor.cpp" "/home/terehin/build/px4_1/cmake-build/src/lib/mixer/CMakeFiles/mixer.dir/mixer_multirotor.cpp.obj"
  "/home/terehin/build/px4_1/src/lib/mixer/mixer_simple.cpp" "/home/terehin/build/px4_1/cmake-build/src/lib/mixer/CMakeFiles/mixer.dir/mixer_simple.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CONFIG_ARCH_BOARD_PX4FMU_V5"
  "__DF_NUTTX"
  "__PX4_NUTTX"
  "__STDC_FORMAT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "NuttX/nuttx/include/cxx"
  "NuttX/nuttx/include"
  "."
  "src"
  "src/lib"
  "src/modules"
  "../src"
  "../src/drivers/boards/px4fmu-v5"
  "../src/include"
  "../src/lib"
  "../src/lib/DriverFramework/framework/include"
  "../src/lib/matrix"
  "../src/modules"
  "../src/ulet"
  "../src/platforms"
  "NuttX/nuttx/arch/arm/src/armv7-m"
  "NuttX/nuttx/arch/arm/src/chip"
  "NuttX/nuttx/arch/arm/src/common"
  "NuttX/apps/include"
  "external/Install/include"
  "src/lib/mixer"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
