# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/terehin/build/px4_1/src/lib/rc/common_rc.cpp" "/home/terehin/build/px4_1/cmake-build/src/lib/rc/CMakeFiles/rc.dir/common_rc.cpp.obj"
  "/home/terehin/build/px4_1/src/lib/rc/crsf.cpp" "/home/terehin/build/px4_1/cmake-build/src/lib/rc/CMakeFiles/rc.dir/crsf.cpp.obj"
  "/home/terehin/build/px4_1/src/lib/rc/dsm.cpp" "/home/terehin/build/px4_1/cmake-build/src/lib/rc/CMakeFiles/rc.dir/dsm.cpp.obj"
  "/home/terehin/build/px4_1/src/lib/rc/sbus.cpp" "/home/terehin/build/px4_1/cmake-build/src/lib/rc/CMakeFiles/rc.dir/sbus.cpp.obj"
  "/home/terehin/build/px4_1/src/lib/rc/st24.cpp" "/home/terehin/build/px4_1/cmake-build/src/lib/rc/CMakeFiles/rc.dir/st24.cpp.obj"
  "/home/terehin/build/px4_1/src/lib/rc/sumd.cpp" "/home/terehin/build/px4_1/cmake-build/src/lib/rc/CMakeFiles/rc.dir/sumd.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CONFIG_ARCH_BOARD_PX4FMU_V5"
  "__DF_NUTTX"
  "__PX4_NUTTX"
  "__STDC_FORMAT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "NuttX/nuttx/include/cxx"
  "NuttX/nuttx/include"
  "."
  "src"
  "src/lib"
  "src/modules"
  "../src"
  "../src/drivers/boards/px4fmu-v5"
  "../src/include"
  "../src/lib"
  "../src/lib/DriverFramework/framework/include"
  "../src/lib/matrix"
  "../src/modules"
  "../src/ulet"
  "../src/platforms"
  "NuttX/nuttx/arch/arm/src/armv7-m"
  "NuttX/nuttx/arch/arm/src/chip"
  "NuttX/nuttx/arch/arm/src/common"
  "NuttX/apps/include"
  "external/Install/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
