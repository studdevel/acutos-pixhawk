# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/terehin/build/px4_1/src/modules/uavcan/uavcan_params.c" "/home/terehin/build/px4_1/cmake/src/modules/uavcan/CMakeFiles/modules__uavcan.dir/uavcan_params.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "CONFIG_ARCH_BOARD_PX4FMU_V5"
  "MODULE_NAME=\"uavcan\""
  "PX4_MAIN=uavcan_app_main"
  "UAVCAN_CPP_VERSION=UAVCAN_CPP03"
  "UAVCAN_DRIVER=uavcan_stm32"
  "UAVCAN_MEM_POOL_BLOCK_SIZE=48"
  "UAVCAN_NO_ASSERTIONS"
  "UAVCAN_PLATFORM=stm32"
  "UAVCAN_STM32_NUM_IFACES=2"
  "UAVCAN_STM32_NUTTX=1"
  "UAVCAN_STM32_TIMER_NUMBER=5"
  "__DF_NUTTX"
  "__PX4_NUTTX"
  "__STDC_FORMAT_MACROS"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "NuttX/nuttx/include/cxx"
  "NuttX/nuttx/include"
  "."
  "src"
  "src/lib"
  "src/modules"
  "../src"
  "../src/drivers/boards/px4fmu-v5"
  "../src/include"
  "../src/lib"
  "../src/lib/DriverFramework/framework/include"
  "../src/lib/matrix"
  "../src/modules"
  "../src/ulet"
  "../src/platforms"
  "NuttX/nuttx/arch/arm/src/armv7-m"
  "NuttX/nuttx/arch/arm/src/chip"
  "NuttX/nuttx/arch/arm/src/common"
  "NuttX/apps/include"
  "external/Install/include"
  "../mavlink/include/mavlink"
  "../src/modules/uavcan/libuavcan/libuavcan/include"
  "../src/modules/uavcan/libuavcan/libuavcan/include/dsdlc_generated"
  "../src/modules/uavcan/libuavcan/libuavcan_drivers/posix/include"
  "../src/modules/uavcan/libuavcan/libuavcan_drivers/stm32/driver/include"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/terehin/build/px4_1/src/modules/uavcan/actuators/esc.cpp" "/home/terehin/build/px4_1/cmake/src/modules/uavcan/CMakeFiles/modules__uavcan.dir/actuators/esc.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/uavcan/actuators/hardpoint.cpp" "/home/terehin/build/px4_1/cmake/src/modules/uavcan/CMakeFiles/modules__uavcan.dir/actuators/hardpoint.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/uavcan/sensors/baro.cpp" "/home/terehin/build/px4_1/cmake/src/modules/uavcan/CMakeFiles/modules__uavcan.dir/sensors/baro.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/uavcan/sensors/gnss.cpp" "/home/terehin/build/px4_1/cmake/src/modules/uavcan/CMakeFiles/modules__uavcan.dir/sensors/gnss.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/uavcan/sensors/mag.cpp" "/home/terehin/build/px4_1/cmake/src/modules/uavcan/CMakeFiles/modules__uavcan.dir/sensors/mag.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/uavcan/sensors/sensor_bridge.cpp" "/home/terehin/build/px4_1/cmake/src/modules/uavcan/CMakeFiles/modules__uavcan.dir/sensors/sensor_bridge.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/uavcan/uavcan_main.cpp" "/home/terehin/build/px4_1/cmake/src/modules/uavcan/CMakeFiles/modules__uavcan.dir/uavcan_main.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/uavcan/uavcan_servers.cpp" "/home/terehin/build/px4_1/cmake/src/modules/uavcan/CMakeFiles/modules__uavcan.dir/uavcan_servers.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CONFIG_ARCH_BOARD_PX4FMU_V5"
  "MODULE_NAME=\"uavcan\""
  "PX4_MAIN=uavcan_app_main"
  "UAVCAN_CPP_VERSION=UAVCAN_CPP03"
  "UAVCAN_DRIVER=uavcan_stm32"
  "UAVCAN_MEM_POOL_BLOCK_SIZE=48"
  "UAVCAN_NO_ASSERTIONS"
  "UAVCAN_PLATFORM=stm32"
  "UAVCAN_STM32_NUM_IFACES=2"
  "UAVCAN_STM32_NUTTX=1"
  "UAVCAN_STM32_TIMER_NUMBER=5"
  "__DF_NUTTX"
  "__PX4_NUTTX"
  "__STDC_FORMAT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "NuttX/nuttx/include/cxx"
  "NuttX/nuttx/include"
  "."
  "src"
  "src/lib"
  "src/modules"
  "../src"
  "../src/drivers/boards/px4fmu-v5"
  "../src/include"
  "../src/lib"
  "../src/lib/DriverFramework/framework/include"
  "../src/lib/matrix"
  "../src/modules"
  "../src/ulet"
  "../src/platforms"
  "NuttX/nuttx/arch/arm/src/armv7-m"
  "NuttX/nuttx/arch/arm/src/chip"
  "NuttX/nuttx/arch/arm/src/common"
  "NuttX/apps/include"
  "external/Install/include"
  "../mavlink/include/mavlink"
  "../src/modules/uavcan/libuavcan/libuavcan/include"
  "../src/modules/uavcan/libuavcan/libuavcan/include/dsdlc_generated"
  "../src/modules/uavcan/libuavcan/libuavcan_drivers/posix/include"
  "../src/modules/uavcan/libuavcan/libuavcan_drivers/stm32/driver/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/terehin/build/px4_1/cmake/src/platforms/common/CMakeFiles/platforms__common.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake/platforms/nuttx/src/px4_layer/CMakeFiles/px4_layer.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake/src/modules/systemlib/CMakeFiles/systemlib.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake/src/lib/mixer/CMakeFiles/mixer.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake/src/modules/uavcan/libuavcan/libuavcan/CMakeFiles/uavcan.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake/src/modules/uavcan/libuavcan/libuavcan_drivers/stm32/driver/CMakeFiles/uavcan_stm32_driver.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake/src/lib/parameters/CMakeFiles/parameters.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake/src/drivers/boards/common/stm32/CMakeFiles/drivers_boards_common_arch.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake/src/lib/parameters/flashparams/CMakeFiles/flashparams.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake/src/drivers/boards/px4fmu-v5/CMakeFiles/drivers_board.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake/src/drivers/stm32/CMakeFiles/drivers_arch.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake/src/drivers/boards/common/CMakeFiles/drivers_boards_common.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake/src/lib/drivers/led/CMakeFiles/drivers__led.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake/src/lib/drivers/device/CMakeFiles/drivers__device.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake/src/lib/cdev/CMakeFiles/cdev.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake/src/lib/perf/CMakeFiles/perf.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake/src/lib/parameters/tinybson/CMakeFiles/tinybson.dir/DependInfo.cmake"
  "/home/terehin/build/px4_1/cmake/msg/CMakeFiles/uorb_msgs.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
