# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/terehin/build/px4_1/src/modules/uavcan/libuavcan/libuavcan_drivers/stm32/driver/src/uc_stm32_can.cpp" "/home/terehin/build/px4_1/cmake/src/modules/uavcan/libuavcan/libuavcan_drivers/stm32/driver/CMakeFiles/uavcan_stm32_driver.dir/src/uc_stm32_can.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/uavcan/libuavcan/libuavcan_drivers/stm32/driver/src/uc_stm32_clock.cpp" "/home/terehin/build/px4_1/cmake/src/modules/uavcan/libuavcan/libuavcan_drivers/stm32/driver/CMakeFiles/uavcan_stm32_driver.dir/src/uc_stm32_clock.cpp.obj"
  "/home/terehin/build/px4_1/src/modules/uavcan/libuavcan/libuavcan_drivers/stm32/driver/src/uc_stm32_thread.cpp" "/home/terehin/build/px4_1/cmake/src/modules/uavcan/libuavcan/libuavcan_drivers/stm32/driver/CMakeFiles/uavcan_stm32_driver.dir/src/uc_stm32_thread.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CONFIG_ARCH_BOARD_PX4FMU_V5"
  "UAVCAN_CPP_VERSION=UAVCAN_CPP03"
  "UAVCAN_DRIVER=uavcan_stm32"
  "UAVCAN_MEM_POOL_BLOCK_SIZE=48"
  "UAVCAN_NO_ASSERTIONS"
  "UAVCAN_PLATFORM=stm32"
  "UAVCAN_STM32_NUM_IFACES=2"
  "UAVCAN_STM32_NUTTX=1"
  "UAVCAN_STM32_TIMER_NUMBER=5"
  "__DF_NUTTX"
  "__PX4_NUTTX"
  "__STDC_FORMAT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "NuttX/nuttx/include/cxx"
  "NuttX/nuttx/include"
  "."
  "src"
  "src/lib"
  "src/modules"
  "../src"
  "../src/drivers/boards/px4fmu-v5"
  "../src/include"
  "../src/lib"
  "../src/lib/DriverFramework/framework/include"
  "../src/lib/matrix"
  "../src/modules"
  "../src/ulet"
  "../src/platforms"
  "NuttX/nuttx/arch/arm/src/armv7-m"
  "NuttX/nuttx/arch/arm/src/chip"
  "NuttX/nuttx/arch/arm/src/common"
  "NuttX/apps/include"
  "external/Install/include"
  "../src/modules/uavcan/libuavcan/./libuavcan/include"
  "../src/modules/uavcan/libuavcan/./libuavcan/include/dsdlc_generated"
  "../src/modules/uavcan/libuavcan/libuavcan_drivers/stm32/driver/./include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
