file(REMOVE_RECURSE
  "CMakeFiles/copy_px4io_bin"
  "../../../romfs_extras/px4io-v2.bin"
  "../../../../build/nuttx_px4io-v2_default/nuttx_px4io-v2_default.elf"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/copy_px4io_bin.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
