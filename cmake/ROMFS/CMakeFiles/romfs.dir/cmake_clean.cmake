file(REMOVE_RECURSE
  "nsh_romfsimg.c"
  "romfs.img"
  "romfs.txt"
  "../genromfs/init.d/rcS"
  "../genromfs/init.d/rc.serial"
  "../genromfs/init.d/rc.autostart"
  "romfs_extras.stamp"
  "CMakeFiles/romfs.dir/nsh_romfsimg.c.obj"
  "libromfs.pdb"
  "libromfs.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang C)
  include(CMakeFiles/romfs.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
