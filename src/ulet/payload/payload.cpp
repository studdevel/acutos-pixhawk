/****************************************************************************
 *
 *   Copyright (c) 2012-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/**
 * @file payload.c
 * Minitest of RTK and GPS. Dumps GPS data to console and optionally to file.
 *
 * @author Vasilevsky Alexander <a.kalaider@yandex.ru>
 */

#include <px4_module.h>
#include <px4_config.h>
#include <px4_tasks.h>
#include <px4_posix.h>
#include <px4_getopt.h>
#include <unistd.h>
#include <stdio.h>
#include <poll.h>
#include <string.h>
#include <math.h>
#include <matrix/math.hpp>
#include <navigator/navigation.h>
#include <commander/px4_custom_mode.h>
#include <dataman/dataman.h>

#include <drivers/drv_hrt.h>
#include <drivers/drv_pwm_output.h>

#include <cstdio>

#include <uORB/uORB.h>
#include <uORB/topics/vehicle_command.h>
#include <uORB/topics/vehicle_global_position.h>
#include <uORB/topics/vehicle_status.h>

#define SERVO_DEVICE_PATH       (PWM_OUTPUT_BASE_DEVICE_PATH "1")

extern "C" __EXPORT int payload_main(int argc, char *argv[]);

class payload : public ModuleBase < payload >
{
public:
    struct config_s
    {
        int poll_interval { 1000 /* ms */ };
    };
public:
    payload(config_s cfg);
    virtual ~payload();
public:
	static int task_spawn(int argc, char *argv[]);
	static payload * instantiate(int argc, char *argv[]);
	static int custom_command(int argc, char *argv[]);
	static int print_usage(const char *reason = nullptr);
	void run() override;
	int print_status() override;
public:
    static int drop_command(int argc, char *argv[]);
public:
    bool do_drop();
private:
    config_s _cfg;
};

payload::payload(payload::config_s cfg)
    : _cfg(cfg)
{
}

payload::~payload()
{
}

int payload::print_usage(const char *reason)
{
	if (reason) {
		PX4_WARN("%s\n", reason);
	}

	PRINT_MODULE_DESCRIPTION(
		R"DESCR_STR(
### Description
Payload control utility.

### Examples
$ payload start -i 500
)DESCR_STR");

	PRINT_MODULE_USAGE_NAME("payload", "utility");
	PRINT_MODULE_USAGE_COMMAND("start");
	PRINT_MODULE_USAGE_PARAM_INT('i', 1000, 50, 1000, "event poll interval", true);
	PRINT_MODULE_USAGE_COMMAND("drop");
	PRINT_MODULE_USAGE_DEFAULT_COMMANDS();

	return 0;
}

int payload::custom_command(int argc, char *argv[])
{
    if (strcmp("drop", argv[0]) == 0)
    {
        return drop_command(argc, argv);
    }

    return print_usage("unknown command");
}

int payload::drop_command(int argc, char *argv[])
{
    // just emit payload control comand

    PX4_INFO("emitting payload control command");
    vehicle_command_s command;
    command.timestamp = hrt_absolute_time();
    command.command = vehicle_command_s::VEHICLE_CMD_PAYLOAD_CONTROL_DEPLOY;
    orb_advertise(ORB_ID(vehicle_command), &command);

    return 0;
}

int payload::print_status()
{
	PX4_INFO("Running");
	return 0;
}

int payload::task_spawn(int argc, char *argv[])
{
    ASSERT(_task_id == -1);

    /* start the task */
    _task_id = px4_task_spawn_cmd("payload",
                    SCHED_DEFAULT,
                    SCHED_PRIORITY_DEFAULT + 15,
                    1500,
                    (px4_main_t)&run_trampoline,
                    (char *const *)argv);

    if (_task_id < 0) {
        PX4_WARN("task start failed");
        return -errno;
    }

    return OK;
}

void payload::run()
{
    int sub = 0;

    struct vehicle_command_s command;
    
    sub = orb_subscribe(ORB_ID(vehicle_command));

    orb_set_interval(sub, _cfg.poll_interval);

    px4_pollfd_struct_t fds[1];

    fds[0].fd = sub;
    fds[0].events = POLLIN;

    PX4_INFO("entering");

    while (!should_exit())
    {
        /* wait for sensor update of 1 file descriptor for 1000 ms (1 second) */
        int poll_ret = px4_poll(fds, 1, 1000);

        if (poll_ret <= 0) continue;

        orb_copy(ORB_ID(vehicle_command), sub, &command);

        PX4_INFO("command accepted: [%d]", (int) command.command);

        print_message(command);

        if (command.command == vehicle_command_s::VEHICLE_CMD_PAYLOAD_CONTROL_DEPLOY)
        {
            PX4_INFO("payload drop command arrived");
            PX4_INFO("requesting payload drop");

            if (do_drop())
            {
                PX4_INFO("done requesting payload drop");
            }
            else
            {
                PX4_ERR("payload drop failed");
            }
        }
    }

    orb_unsubscribe(sub);

    PX4_INFO("exiting");
}

bool payload::do_drop()
{
    bool res = true;
    int ret;

    int aux_n, lock_pwm, unlock_pwm, drop_period_ms;

    param_t param_aux_n = param_find("PLC_SERVO_AUX_N");
    param_t param_lock_pwm = param_find("PLC_LOCK_PWM");
    param_t param_unlock_pwm = param_find("PLC_UNLOCK_PWM");
    param_t param_drop_period_ms = param_find("PLC_DROP_PERIOD");

    param_get(param_aux_n, &aux_n);
    param_get(param_lock_pwm, &lock_pwm);
    param_get(param_unlock_pwm, &unlock_pwm);
    param_get(param_drop_period_ms, &drop_period_ms);

    // use aux output driver directly

    int fd = px4_open(SERVO_DEVICE_PATH, 0);

    if (fd < 0)
    {
        PX4_ERR("error opening pwm output device [%s]", SERVO_DEVICE_PATH);
        return false;
    }

    // enter test mode to disable event-driven control
    if (::ioctl(fd, PWM_SERVO_SET_MODE, PWM_SERVO_ENTER_TEST_MODE) < 0)
    {
        PX4_ERR("error entering servo test mode");
        return false;
    }

    ret = px4_ioctl(fd, PWM_SERVO_SET(aux_n), unlock_pwm);

    if (ret != OK)
    {
        PX4_ERR("error setting pwm value");
        res = false;
    }
    else
    {
        usleep(drop_period_ms * 1000);
    }

    // restore pwm value
    ret = px4_ioctl(fd, PWM_SERVO_SET(aux_n), lock_pwm);

    if (ret != OK)
    {
        PX4_ERR("error setting pwm value");
        res = false;
    }

    // exit test mode when we are done
    if (::ioctl(fd, PWM_SERVO_SET_MODE, PWM_SERVO_EXIT_TEST_MODE) < 0)
    {
        PX4_ERR("error exiting servo test mode");
        res = false;
    }

    px4_close(fd);

    return res;
}

payload * payload::instantiate(int argc, char *argv[])
{
    bool error_flag = false;

    int poll = 1000;

    int myoptind = 1;
	int ch;
    const char * myoptarg = nullptr;

    while ((ch = px4_getopt(argc, argv, "i:", &myoptind, &myoptarg)) != EOF)
    {
		switch (ch) {
        case 'i':
			poll = (int)strtol(myoptarg, nullptr, 10);
            if (poll < 50 || poll > 1000)
            {
                PX4_WARN("polling interval parameter out of range");
                error_flag = true;
            }
            break;
		case '?':
			error_flag = true;
			break;
		default:
			PX4_WARN("unrecognized flag");
			error_flag = true;
			break;
		}
	}

    if (error_flag) return nullptr;
    
    PX4_INFO("spawning");

    payload::config_s cfg;
    cfg.poll_interval = poll;

    payload * instance = new payload(cfg);

	if (instance == nullptr) {
		PX4_ERR("alloc failed");
	}

    return instance;
}

int payload_main(int argc, char *argv[])
{
    return payload::main(argc, argv);
}
