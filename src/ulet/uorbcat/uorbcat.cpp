/****************************************************************************
 *
 *   Copyright (c) 2012-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/**
 * @file uorbcat.c
 * Minitest of RTK and GPS. Dumps GPS data to console and optionally to file.
 *
 * @author Vasilevsky Alexander <a.kalaider@yandex.ru>
 */

#include <px4_module.h>
#include <px4_config.h>
#include <px4_tasks.h>
#include <px4_posix.h>
#include <px4_getopt.h>
#include <unistd.h>
#include <stdio.h>
#include <poll.h>
#include <string.h>
#include <math.h>
#include <matrix/math.hpp>

#include <cstdio>

#include <uORB/uORB.h>
#include <uORB/topics/sensor_combined.h>
#include <uORB/topics/vehicle_gps_position.h>
#include <uORB/topics/vehicle_local_position.h>
#include <uORB/topics/vehicle_global_position.h>
#include <uORB/topics/vehicle_attitude.h>

extern "C" __EXPORT int uorbcat_main(int argc, char *argv[]);

#define uorbcat_INFO(file,quiet,fmt,...) {              \
    if (!quiet) PX4_INFO(fmt, ##__VA_ARGS__);           \
    if (file)                                           \
    {                                                   \
        fprintf(file, fmt "\n", ##__VA_ARGS__);         \
    }                                                   \
}

enum log_mode : size_t
{
    log_mode_gps_position,
    log_mode_global_position,
    log_mode_local_position,
    log_mode_orientation,
    log_mode_compact,
    _log_mode_max
};

class uorbcat : public ModuleBase < uorbcat >
{
public:
    uorbcat(log_mode mode, bool quiet, int pollint, const char * file);
    virtual ~uorbcat();
public:
	static int task_spawn(int argc, char *argv[]);
	static uorbcat * instantiate(int argc, char *argv[]);
	static int custom_command(int argc, char *argv[]);
	static int print_usage(const char *reason = nullptr);
	void run() override;
	int print_status() override;
private:
    bool _quiet;
    log_mode _mode;
    int _poll_interval;
    const char * _logfilename;
    FILE *_logfile; /**< log file handle */
};

uorbcat::uorbcat(log_mode mode, bool quiet, int pollint, const char * file) :
    _quiet(quiet),
    _mode(mode),
    _poll_interval(pollint),
    _logfilename(file),
    _logfile(nullptr)
{
}

uorbcat::~uorbcat()
{
}

int uorbcat::print_usage(const char *reason)
{
	if (reason) {
		PX4_WARN("%s\n", reason);
	}

	PRINT_MODULE_DESCRIPTION(
		R"DESCR_STR(
### Description
The utility dumpms certain uORB topics to stdout and to the specified file, if any.

### Examples
$ uorbcat start
$ uorbcat start -m 2
$ uorbcat start "/fs/microsd/uorbcat/global.log"
$ uorbcat start -m 2 -q -i 500 "/fs/microsd/uorbcat/local.log"
)DESCR_STR");

	PRINT_MODULE_USAGE_NAME("uorbcat", "utility");
	PRINT_MODULE_USAGE_COMMAND("start");
	PRINT_MODULE_USAGE_PARAM_FLAG('m', "Output mode: 0 - raw gps, 1 - global pos, 2 - local pos, 3 - orientation, 4 - all (compact)", true);
	PRINT_MODULE_USAGE_PARAM_FLAG('q', "Don't print to stdout", true);
	PRINT_MODULE_USAGE_PARAM_INT('i', 200, 50, 1000, "Event poll interval", true);
	PRINT_MODULE_USAGE_DEFAULT_COMMANDS();

	return 0;
}

int uorbcat::custom_command(int argc, char *argv[])
{
    return print_usage("unknown command");
}

int uorbcat::print_status()
{
	PX4_INFO("Running");
	return 0;
}

int uorbcat::task_spawn(int argc, char *argv[])
{
    ASSERT(_task_id == -1);

    /* start the task */
    _task_id = px4_task_spawn_cmd("uorbcat",
                    SCHED_DEFAULT,
                    SCHED_PRIORITY_DEFAULT + 15,
                    1500,
                    (px4_main_t)&run_trampoline,
                    (char *const *)argv);

    if (_task_id < 0) {
        PX4_WARN("task start failed");
        return -errno;
    }

    return OK;
}

void uorbcat::run()
{
    if (_logfilename)
    {
        _logfile = fopen(_logfilename, "w");
        if (!_logfile)
        {
            PX4_ERR("File [%s] is inaccessible", _logfilename);
            request_stop();
        }
    }

    int main_sub = 0;
    int supp_sub = 0;

    struct vehicle_gps_position_s gps_data;
    struct vehicle_local_position_s local_data;
    struct vehicle_global_position_s global_data;
    struct vehicle_attitude_s att_data;

    switch (_mode)
    {
    case log_mode_local_position:
        main_sub = orb_subscribe(ORB_ID(vehicle_local_position));
        break;
    case log_mode_global_position:
        main_sub = orb_subscribe(ORB_ID(vehicle_global_position));
        break;
    case log_mode_orientation:
        main_sub = orb_subscribe(ORB_ID(vehicle_attitude));
        break;
    case log_mode_gps_position:
    default:
        main_sub = orb_subscribe(ORB_ID(vehicle_global_position));
        supp_sub = orb_subscribe(ORB_ID(vehicle_attitude));
        break;
    }

    /* limit the update rate to 5 Hz */
    orb_set_interval(main_sub, _poll_interval);

    px4_pollfd_struct_t fds[1];

    fds[0].fd = main_sub;
    fds[0].events = POLLIN;

    bool att_updated = false;

    while (!should_exit())
    {
        /* wait for sensor update of 1 file descriptor for 1000 ms (1 second) */
        int poll_ret = px4_poll(fds, 1, 1000);

        if (poll_ret <= 0) continue;

        switch (_mode)
        {
        case log_mode_local_position:
        {
            /* copy sensors raw data into local buffer */
            orb_copy(ORB_ID(vehicle_local_position), main_sub, &local_data);

            uorbcat_INFO(
                _logfile,
                _quiet,
                "%" PRId64 "\t%.8f\t%.8f\t%.8f\t%d%d\t%" PRId64 "\t%.8f\t%.8f\t%.8f\t%d%d\t%.8f\t%.8f",
                local_data.timestamp,
                (double)local_data.x,
                (double)local_data.y,
                (double)local_data.z,
                (int)local_data.xy_valid,
                (int)local_data.z_valid,
                local_data.ref_timestamp,
                (double)local_data.ref_lat,
                (double)local_data.ref_lon,
                (double)local_data.ref_alt,
                (int)local_data.xy_global,
                (int)local_data.z_global,
                (double)local_data.eph,
                (double)local_data.epv
            );
        }
            break;
        case log_mode_global_position:
        {
            /* copy sensors raw data into local buffer */
            orb_copy(ORB_ID(vehicle_global_position), main_sub, &global_data);

            uorbcat_INFO(
                _logfile,
                _quiet,
                "%" PRId64 "\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f",
                global_data.timestamp,
                (double)global_data.lat,
                (double)global_data.lon,
                (double)global_data.alt,
                (double)global_data.eph,
                (double)global_data.epv
            );
        }
            break;
        case log_mode_orientation:
        {
            /* copy sensors raw data into local buffer */
            orb_copy(ORB_ID(vehicle_attitude), main_sub, &att_data);

            matrix::Eulerf euler(matrix::Quatf(att_data.q));

            uorbcat_INFO(
                _logfile,
                _quiet,
                "%" PRId64 "\t%.8f\t%.8f\t%.8f",
                att_data.timestamp,
                (double)euler.phi(),    // roll
                (double)euler.theta(),  // pitch
                (double)euler.psi()     // yaw
            );
        }
            break;
        case log_mode_gps_position:
        {
            /* copy sensors raw data into local buffer */
            orb_copy(ORB_ID(vehicle_gps_position), main_sub, &gps_data);

            uorbcat_INFO(
                _logfile,
                _quiet,
                "%" PRId64 "\t%d\t%d\t%d\t%u\t%d\t%.8f\t%.8f\t%.8f\t%.8f",
                gps_data.timestamp,
                gps_data.lat,
                gps_data.lon,
                gps_data.alt,
                (int32_t) gps_data.fix_type,
                (int32_t) gps_data.noise_per_ms,
                (double)gps_data.eph,
                (double)gps_data.epv,
                (double)gps_data.hdop,
                (double)gps_data.vdop
            );
        }
            break;
        default:
        {
            orb_copy(ORB_ID(vehicle_global_position), main_sub, &global_data);

            orb_check(supp_sub, &att_updated);

            if (!att_updated) break;

            orb_copy(ORB_ID(vehicle_attitude), supp_sub, &att_data);

            matrix::Eulerf euler(matrix::Quatf(att_data.q));

            uorbcat_INFO(
                _logfile,
                _quiet,
                "%.3f\t%.8f\t%.8f\t%.8f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f",
                (double)global_data.timestamp * 1e-6,
                (double)global_data.lat,
                (double)global_data.lon,
                (double)global_data.alt,
                (double)euler.phi() * 180. / M_PI,    // roll
                (double)euler.theta() * 180. / M_PI,  // pitch
                (double)euler.psi() * 180. / M_PI,    // yaw
                (double)std::sqrt(global_data.vel_n * global_data.vel_n + global_data.vel_e * global_data.vel_e),
                (double)global_data.vel_d
            );
        }
            break;
        }
    }

    PX4_INFO("exiting");

    if (_logfile)
    {
        fclose(_logfile);
    }
}

uorbcat * uorbcat::instantiate(int argc, char *argv[])
{
    bool error_flag = false;

    int myoptind = 1;
	int ch;
    const char * myoptarg = nullptr;

    log_mode mode = log_mode_gps_position;
    bool quiet = false;
    int poll = 200;
    const char * filename = nullptr;

    while ((ch = px4_getopt(argc, argv, "i:qm:", &myoptind, &myoptarg)) != EOF)
    {
		switch (ch) {
		case 'm':
			mode = (log_mode)(int)strtol(myoptarg, nullptr, 10);
			if (mode >= _log_mode_max)
			{
				PX4_WARN("output mode parameter out of range");
				error_flag = true;
			}
			break;
		case 'q':
			quiet = true;
			break;
		case 'i':
			poll = (int)strtol(myoptarg, nullptr, 10);
            if (poll < 50 || poll > 1000)
            {
                PX4_WARN("polling interval parameter out of range");
                error_flag = true;
            }
			break;
		case '?':
			error_flag = true;
			break;
		default:
			PX4_WARN("unrecognized flag");
			error_flag = true;
			break;
		}
	}

    if (error_flag) return nullptr;

    if (myoptind + 1 <= argc)
    {
        filename = argv[argc - 1];
    }
    
    PX4_INFO("Mode: %d, quiet: %d, poll: %d, file: [%s]", (int)mode, (int) quiet, poll, filename);

    uorbcat * instance = new uorbcat(mode, quiet, poll, filename);

	if (instance == nullptr) {
		PX4_ERR("alloc failed");
	}

    return instance;
}

int uorbcat_main(int argc, char *argv[])
{
    return uorbcat::main(argc, argv);
}
