/****************************************************************************
 *
 *   Copyright (c) 2012-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#pragma once

/**
 * @file target_repository.hpp
 *
 * Helper class that holds all available targets.
 *
 * @author Vasilevsky Alexander <a.kalaider@yandex.ru>
 */

#include <precnav/target_estimator.hpp>

#include <drivers/drv_hrt.h>

#define PRECNAV_MAX_TARGETS 4

class target_repository : public precnav_plugin
{
public:
    struct target_config_s
    {
        uint8_t type;                   // user-defined
        const char * name;              // user-defined display name
        uint64_t ttl;                   // time-to-live
        uint8_t capacity;               // max number of targets of the given type
    };
    struct target_s
    {
        int id;                         // monotonically increasing
        uint8_t type;                   // user-defined
        target_estimator * estimator;
    };
public:
    static const size_t max_targets = PRECNAV_MAX_TARGETS;
private:
    target_estimator _local_estimator;
    int _current;
    target_config_s _configs[max_targets];
    target_s _targets[max_targets];
    int _sequence;
public:
    target_repository(precnav * parent)
        : precnav_plugin(parent)
        , _local_estimator(parent)
        , _current(-1)
        , _sequence(-1)
    {
        for (size_t i = 0; i < max_targets; ++i)
        {
            _targets[i] = {};
            _targets[i].id = -1; // invalid
            _targets[i].estimator = new target_estimator(parent);
            _configs[i] = {};
            _configs[i].type = (uint8_t)-1; // empty
        }
    }
    ~target_repository()
    {
        for (size_t i = 0; i < max_targets; ++i) delete _targets[i].estimator;
    }
public:
    const target_config_s * configs() const { return _configs; }
    target_config_s * configs() { return _configs; }
    target_estimator & get_default_estimator() { return _local_estimator; }
    target_s * current() { return (_current == -1) ? nullptr : target(_current); }
    const target_s * current() const { return (_current == -1) ? nullptr : target(_current); }
    int get_current() const { return _current; }
    void set_current(int cur) { _current = cur; }
    const target_s * targets() const { return _targets; }
    target_s * targets() { return _targets; }
    size_t count() const
    {
        size_t c = 0;
        for (size_t i = 0; i < max_targets; ++i)
            if (_targets[i].id != -1) ++c;
        return c;
    }
    size_t count(uint8_t type) const
    {
        size_t c = 0;
        for (size_t i = 0; i < max_targets; ++i)
            if (_targets[i].id != -1 && _targets[i].type == type) ++c;
        return c;
    }
    const target_s * target(int id) const
    {
        for (size_t i = 0; i < max_targets; ++i)
            if (_targets[i].id == id) return &_targets[i];
        return nullptr;
    }
    target_s * target(int id)
    {
        for (size_t i = 0; i < max_targets; ++i)
            if (_targets[i].id == id) return &_targets[i];
        return nullptr;
    }
    const target_config_s * config(uint8_t type) const
    {
        for (size_t i = 0; i < max_targets; ++i)
            if (_configs[i].type == type) return &_configs[i];
        return nullptr;
    }
    target_config_s * config(uint8_t type)
    {
        for (size_t i = 0; i < max_targets; ++i)
            if (_configs[i].type == type) return &_configs[i];
        return nullptr;
    }
    target_s * alloc(uint8_t type)
    {
        for (size_t i = 0; i < max_targets; ++i)
        {
            if (_targets[i].id == -1)
            {
                // ensure there is no constraint violation
                target_config_s * cfg = config(type);
                if (cfg != nullptr && cfg->capacity != 0 &&
                    count(type) >= cfg->capacity)
                {
                    return nullptr;
                }
                _targets[i].id = ++_sequence;
                _targets[i].type = type;
                _targets[i].estimator->reset();
                return &_targets[i];
            }
        }
        return nullptr;
    }
    bool release(int id)
    {
        auto tgt = target(id);
        if (tgt != nullptr)
        {
            tgt->id = -1;
            return true;
        }
        return false;
    }
public:
    void run(bool active) override;
	void describe(size_t lvl) const override;
protected:
    void on_deactivate() override;
public:
    void reset()
    {
        for (size_t i = 0; i < max_targets; ++i) _targets[i].id = -1;
        _current = -1;
    }
};
