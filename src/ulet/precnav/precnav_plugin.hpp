/****************************************************************************
 *
 *   Copyright (c) 2012-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#pragma once

/**
 * @file precnav_plugin.hpp
 *
 * Precnav module interface.
 *
 * @author Vasilevsky Alexander <a.kalaider@yandex.ru>
 */

#include <px4_module_params.h>

#include <precnav/precnav_fwd.hpp>
#include <precnav/precnav_util.hpp>

class precnav_plugin : public ModuleParams
{
public:
    precnav_plugin(precnav * parent)
        :  ModuleParams(detail::as_module_params(parent))
        , _core(parent)
        , _active(false)
    {
    }
    virtual ~precnav_plugin()
    {
    }
public:
	virtual void run(bool activate)
    {
        if (activate && !_active) on_activate();
        if (!activate && _active) on_deactivate();

        // don't run these methods right now --
        // give them a chance to see updated topic data, etc.
        if (activate && _active) on_active();
        if (!activate && !_active) on_inactive();

        _active = activate;
    }
	virtual void describe(size_t lvl) const {}
protected:
    virtual void on_activate() {}
    virtual void on_deactivate() {}
    virtual void on_active() {}
    virtual void on_inactive() {}
protected:
    precnav * _core;
    bool _active;
};
