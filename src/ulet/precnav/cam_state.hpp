/****************************************************************************
 *
 *   Copyright (c) 2012-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#pragma once

/**
 * @file cam_state.hpp
 * 
 * Camera state transitions.
 *
 * @author Vasilevsky Alexander <a.kalaider@yandex.ru>
 */

#include <drivers/drv_hrt.h>

enum cam_mode_t
{
    CAM_INVALID,
    CAM_OFF,
    CAM_DOWN,
    CAM_FRONT
};

struct cam_state_s
{
    cam_mode_t desired_mode;   // desired mode for camera
    cam_mode_t committed_mode; // committed but not actuatualized yet mode
    hrt_abstime timestamp;     // time mode switch started
    cam_mode_t actual_mode;    // actual camera mode

    cam_state_s()
        : desired_mode(CAM_INVALID)
        , committed_mode(CAM_INVALID)
        , timestamp(0)
        , actual_mode(CAM_INVALID) {}

    bool is_trans_active()
    {
        return committed_mode != CAM_INVALID;
    }
    bool can_end_trans(hrt_abstime delay)
    {
        return !is_trans_active() || (hrt_absolute_time() - timestamp) > delay;
    }

    void trans_prepare(cam_mode_t desired)
    {
        desired_mode = desired;
    }
    /**
     * @return true if <b>new</b> commit accepted
     */
    bool trans_commit()
    {
        if (desired_mode == CAM_INVALID) return false;
        if (desired_mode == committed_mode ||
            desired_mode == actual_mode)
        {
            desired_mode = CAM_INVALID;
            return false;
        }
        actual_mode = CAM_INVALID;
        committed_mode = desired_mode;
        desired_mode = CAM_INVALID;
        timestamp = hrt_absolute_time();
        return true;
    }
    void trans_advance()
    {
        if (!is_trans_active()) return;
        timestamp = hrt_absolute_time();
    }
    void trans_end()
    {
        actual_mode = committed_mode;
        timestamp = 0;
        committed_mode = CAM_INVALID;
    }
    void reset()
    {
        // leave desired mode as is
        committed_mode = CAM_INVALID;
        timestamp = 0;
        actual_mode = CAM_INVALID;
    }
};
