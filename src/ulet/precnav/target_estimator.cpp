/****************************************************************************
 *
 *   Copyright (c) 2012-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/**
 * @file target_estimator.cpp
 *
 * Target estimator
 *
 * @author Vasilevsky Alexander <a.kalaider@yandex.ru>
 */

#include <precnav/target_estimator.hpp>
#include <precnav/precnav.hpp>

void target_estimator::describe(size_t lvl) const
{
    PNP_PRINT(lvl, "target estimator: [%s], %f m to target",
        valid() && visible() && locked() ?
            "VISIBLE" :
        valid() && visible() && !locked() ?
            "LOCKING" :
        valid() && !visible() && locked() ?
            "LOCKED" :
        valid() ?
            "VALID" :
            "INVALID",
        (double)hypot(_state.rel.x, _state.rel.y));
    PNP_PRINT(lvl + 1, "local pos (%.3f, %.3f, %.3f)",
        _state.local.x,
        _state.local.y,
        _state.local.z);
    PNP_PRINT(lvl + 1, "rel pos   (%.3f, %.3f, %.3f)",
        _state.rel.x,
        _state.rel.y,
        _state.rel.z);
    PNP_PRINT(lvl + 1, "self pos  (%.3f, %.3f, %.3f)",
        _state.self.x,
        _state.self.y,
        _state.self.z);
    if (size_valid())
    {
        PNP_PRINT(lvl + 1, "size %.3f (%.3f x %.3f)",
            _state.size.d,
            _state.size.w,
            _state.size.h);
    }
    else
    {
        PNP_PRINT(lvl + 1, "size invalid");
    }
    if (dist_valid())
    {
        PNP_PRINT(lvl + 1, "estimated distance %.3f", _state.dist);
    }
    else
    {
        PNP_PRINT(lvl + 1, "estimated distance invalid");
    }
}

void target_estimator::supply(local_pos_s rel, size_s size, double dist)
{
    ++_state.count;

    _state.timestamp = hrt_absolute_time();
    _state.visible = true;

    _state.rel = rel;
    _state.size = size;
    _state.dist = dist;

    _state.self.x = (double)_core->get_local_position().x;
    _state.self.y = (double)_core->get_local_position().y;
    _state.self.z = (double)_core->get_local_position().z;

    _state.local.x = rel.x + _state.self.x;
    _state.local.y = rel.y + _state.self.y;
    _state.local.z = rel.z + _state.self.z;

    map_projection_reproject(&_core->get_current_reference(),
        _state.rel.x, _state.rel.y, &_state.glob.lat, &_state.glob.lon);
    _state.glob.alt = _state.local.z;
}
