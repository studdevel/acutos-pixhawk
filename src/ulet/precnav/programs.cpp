/****************************************************************************
 *
 *   Copyright (c) 2012-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/**
 * @file programs.cpp
 * 
 * Test program definitions.
 *
 * @author Vasilevsky Alexander <a.kalaider@yandex.ru>
 */

#include <precnav/programs.hpp>
#include <precnav/precnav.hpp>

void takeoff_job::on_activate()
{
    PX4_INFO("takeoff job: activation");
    _status = JOB_STATUS_ACTIVE;
    ((ulet_navigator *)_core->get_navigator())
        ->set_nav_mode(NAV_MODE_TAKEOFF);
}

void takeoff_job::on_active()
{
    nav_result_t result = ((ulet_navigator *)_core->get_navigator())
        ->get_nav_mode(NAV_MODE_TAKEOFF)->get_last_result();
    if (result == NAV_RESULT_SUCCESS)
    {
        PX4_INFO("takeoff job: success");
        _status = JOB_STATUS_SUCCESS;
    }
    else if (result == NAV_RESULT_FAILURE)
    {
        PX4_ERR("takeoff job: failure");
        _status = JOB_STATUS_FAILURE;
    }
}

bool takeoff_job::check_conditions()
{
    return _core->get_status().is_armed() &&
        _core->claim_ack(ack_t::ACK_TAKEOFF);
}

void find_land_job::on_activate()
{
    _status = JOB_STATUS_ACTIVE;
    ((ulet_navigator *)_core->get_navigator())
        ->set_nav_mode(NAV_MODE_MISSION);
}

void find_land_job::on_active()
{
    // ensure camera looks down
    if (!_core->get_camera_controller().is_down())
        _core->get_camera_controller().switch_down();

    nav_result_t result = ((ulet_navigator *)_core->get_navigator())
        ->get_nav_mode(NAV_MODE_MISSION)->get_last_result();
    if (result == NAV_RESULT_SUCCESS)
    {
        PX4_INFO("find land job: success");
        _status = JOB_STATUS_SUCCESS;
    }
    else if (result == NAV_RESULT_FAILURE)
    {
        PX4_ERR("find land job: failure");
        _status = JOB_STATUS_FAILURE;
    }
    else
    {
        for (size_t i = 0; i < target_repository::max_targets; ++i)
        {
            auto & target = _core->get_target_repository().targets()[i];
            if (target.type == optical_recognition_result_s::OBJECT_TYPE_LANDING_POS &&
                target.estimator->locked() && target.estimator->visible())
            {
                ((ulet_navigator *)_core->get_navigator())
                    ->set_nav_mode(NAV_MODE_IDLE);
                PX4_INFO("find land job: success");
                _status = JOB_STATUS_SUCCESS;
                break;
            }
        }
    }
}

bool find_land_job::check_conditions()
{
    return true;
}

void land_job::on_activate()
{
    _status = JOB_STATUS_ACTIVE;
    ((ulet_navigator *)_core->get_navigator())
        ->set_nav_mode(NAV_MODE_PRECLAND);
}

void land_job::on_active()
{
    // ensure camera looks down
    if (!_core->get_camera_controller().is_down())
        _core->get_camera_controller().switch_down();

    _core->get_target_repository().set_current(-1);

    for (size_t i = 0; i < target_repository::max_targets; ++i)
    {
        if (_core->get_target_repository().targets()[i].type ==
            optical_recognition_result_s::OBJECT_TYPE_LANDING_POS)
        {
            _core->get_target_repository().set_current(
                _core->get_target_repository().targets()[i].id);
            break;
        }
    }

    nav_result_t result = ((ulet_navigator *)_core->get_navigator())
        ->get_nav_mode(NAV_MODE_PRECLAND)->get_last_result();
    if (result == NAV_RESULT_SUCCESS)
    {
        PX4_INFO("precland job: success");
        _status = JOB_STATUS_SUCCESS;
    }
    else if (result == NAV_RESULT_FAILURE)
    {
        PX4_ERR("precland job: failure");
        _status = JOB_STATUS_FAILURE;
    }
}

bool land_job::check_conditions()
{
    return _core->claim_ack(ack_t::ACK_LAND);
}

void kill_ball_job::on_activate() {}
void kill_ball_job::on_active() {}
bool kill_ball_job::check_conditions() { return true; }

// use simplified version for now
bool kill_ball_program::choose_next_job()
{
    if (!_jobs[0]->is_finished())
        return activate(_jobs[0]);
    if (_jobs[0]->is_failed())
        return activate(nullptr);
    if (!_jobs[1]->is_finished())
        return activate(_jobs[1]);
    if (_jobs[1]->is_failed())
        return activate(nullptr);
    if (!_jobs[_count - 1]->is_finished())
        return activate(_jobs[_count - 1]);
    return activate(nullptr);
}
