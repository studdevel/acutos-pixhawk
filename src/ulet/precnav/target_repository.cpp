/****************************************************************************
 *
 *   Copyright (c) 2012-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/**
 * @file target_repository.cpp
 *
 * Target repository implementation
 *
 * @author Vasilevsky Alexander <a.kalaider@yandex.ru>
 */

#include <precnav/target_repository.hpp>
#include <precnav/precnav.hpp>

void target_repository::describe(size_t lvl) const
{
    PNP_PRINT(lvl, "target repository: %d active targets", (int)count());
    for (size_t i = 0; i < max_targets; ++i)
    {
        if (_targets[i].id == -1) continue;
        const target_config_s * cfg = config(_targets[i].type);
        PNP_PRINT(lvl + 1, "[%d] of type [%d/%s]:",
            _targets[i].id, _targets[i].type,
            cfg == nullptr ? "unknown" : cfg->name);
        _targets[i].estimator->describe(lvl + 2);
    }
}

void target_repository::run(bool active)
{
    for (size_t i = 0; i < max_targets; ++i)
        _targets[i].estimator->run(active);

    if (active)
    {
        for (size_t i = 0; i < max_targets; ++i)
        {
            if (_targets[i].id == -1) continue;
            if (_targets[i].estimator->state().timestamp == 0) continue;

            target_config_s * cfg = config(_targets[i].type);
            if (cfg == nullptr) continue;
            if (cfg->ttl == 0) continue;

            if (hrt_absolute_time() - _targets[i].estimator->state().timestamp < cfg->ttl) continue;

            release(_targets[i].id);
        }
    }
}

void target_repository::on_deactivate()
{
    reset();
}
