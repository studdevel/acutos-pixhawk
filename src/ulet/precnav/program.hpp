/****************************************************************************
 *
 *   Copyright (c) 2012-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#pragma once

/**
 * @file program.hpp
 * 
 * Program definition.
 *
 * @author Vasilevsky Alexander <a.kalaider@yandex.ru>
 */

#include <precnav/precnav_plugin.hpp>

#include <px4_log.h>

enum job_status_t
{
    JOB_STATUS_PENDING,
    JOB_STATUS_ACTIVE,
    JOB_STATUS_SUCCESS,
    JOB_STATUS_FAILURE
};

class job : public precnav_plugin
{
public:
    virtual bool check_conditions() { return true; }
public:
    job_status_t get_status() const { return _status; }
    bool is_pending()   const { return _status == JOB_STATUS_PENDING; }
    bool is_active()    const { return _status == JOB_STATUS_ACTIVE;  }
    bool is_succeeded() const { return _status == JOB_STATUS_SUCCESS; }
    bool is_failed()    const { return _status == JOB_STATUS_FAILURE; }
    bool is_finished()  const { return !is_active() && !is_pending(); }
protected:
    job_status_t _status;
public:
    job(precnav * core)
        : precnav_plugin(core)
        , _status(JOB_STATUS_PENDING)
    {
    }
    virtual ~job() {}
};

class program : public precnav_plugin
{
public:
    void describe(size_t lvl) const override
    {
        PNP_PRINT(lvl, "program: valid");
        if (_active != nullptr) _active->describe(lvl + 1);
    }
protected:
    void on_active() override
    {
        choose_next_job();
        for (size_t i = 0; i < _count; ++i)
        {
            if (_jobs[i] != _active) if (_jobs[i] != nullptr) _jobs[i]->run(false);
        }
        if (_active != nullptr) _active->run(true);
    }
public:
    virtual bool choose_next_job() = 0;
protected:
    virtual bool check_conditions(job * j)
    {
        if (j == nullptr) return false;
        if (j->is_finished()) return false;
        if (j->is_pending()) return j->check_conditions();
        return true;
    }
    virtual bool activate(job * j)
    {
        if (check_conditions(j)) { _active = j; return true; }
        else { _active = nullptr; return false; }
    }
public:
    virtual bool is_completed() const
    {
        for (size_t i = 0; i < _count; ++i) if (!_jobs[i]->is_finished()) return false;
        return true;
    }
    job *  get_active_job() const { return _active; }
    job ** get_all()        const { return _jobs;   }
    size_t get_count()      const { return _count;  }
protected:
    job * _active;
    job ** _jobs;
    size_t _count;
public:
    program(precnav * core)
         : precnav_plugin(core)
         , _active(nullptr)
         , _jobs(nullptr)
         , _count(0)
    {
    }
    virtual ~program()
    {
    }
};

class linear_program : public program
{
public:
    linear_program(precnav * core, job ** jobs, size_t count)
        : program(core)
        , _active_idx(0)
    {
        _jobs = jobs;
        _count = count;
    }
    virtual ~linear_program() {}
private:
    size_t _active_idx;
public:
    virtual bool is_completed() const override { return _active_idx == _count; }
    bool choose_next_job() override
    {
        if (is_completed()) return false;
        if (_active != nullptr && !_active->is_finished()) return false;
        size_t candidate_idx = (_active == nullptr) ? 0 : _active_idx + 1;
        // ignore conditions checking since there is only one candidate
        job * candidate = _jobs[candidate_idx];
        _active_idx = candidate_idx;
        _active = candidate;
        return true;
    }
};
