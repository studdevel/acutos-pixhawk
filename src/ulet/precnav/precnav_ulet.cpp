/****************************************************************************
 *
 *   Copyright (c) 2012-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/**
 * @file precnav_ulet.cpp
 * 
 * Ulet-specific functionality.
 *
 * @author Vasilevsky Alexander <a.kalaider@yandex.ru>
 */

#include <precnav/precnav.hpp>
#include <precnav/nav_modes.hpp>
#include <precnav/programs.hpp>

enum program_t
{
    PROGRAM_NO,
    PROGRAM_MIN,
    PROGRAM_TEST,
    PROGRAM_FULL,
    _PROGRAM_MAX
};

static const char * _program_names[] = {
    "no",
    "min",
    "test",
    "full"
};

const char * precnav::ulet_get_program_name(size_t idx)
{
    return idx >= ulet_get_total_programs() ? nullptr : _program_names[idx];
}

size_t precnav::ulet_get_total_programs()
{
    return (size_t)_PROGRAM_MAX;
}

size_t precnav::ulet_get_program_idx(const char * name)
{
    for (size_t i = 0; i < (size_t)ulet_get_total_programs(); ++i)
    {
        if (strcmp(_program_names[i], name) == 0) return i;
    }
    return (size_t)-1;
}

bool precnav::ulet_set_program(size_t idx)
{
    if (idx >= _PROGRAM_MAX) return false;
    if (_program != nullptr)
    {
        _program->run(false);
        delete _program;
    }
    switch ((program_t)idx)
    {
        case PROGRAM_MIN:
            _program = new kill_ball_program(this, 0);
            break;
        default:
            _program = nullptr;
            break;
    }
    return true;
}

void precnav::ulet_init()
{
    _navigator = new ulet_navigator(this);
    ulet_set_program((size_t)PROGRAM_NO);
    _target_repository->configs()[0].type =
        optical_recognition_result_s::OBJECT_TYPE_BALLOON;
    _target_repository->configs()[0].name = "BALLOON";
    _target_repository->configs()[0].capacity = 3;
    _target_repository->configs()[1].type =
        optical_recognition_result_s::OBJECT_TYPE_LANDING_POS;
    _target_repository->configs()[1].name = "LANDING";
    _target_repository->configs()[1].capacity = 1;
    _target_repository->configs()[1].ttl = 1000 * 1000 * 10; // 10s
}

void precnav::ulet_disarm()
{
    ulet_set_program((size_t)PROGRAM_NO);
}

void precnav::ulet_dispose()
{
    delete _program;
    delete _navigator;
}
