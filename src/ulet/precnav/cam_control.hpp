/****************************************************************************
 *
 *   Copyright (c) 2012-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#pragma once

/**
 * @file cam_control.hpp
 * 
 * Camera-controlling.
 *
 * Sends commands to companion computer. Waits for acks.
 *
 * @author Vasilevsky Alexander <a.kalaider@yandex.ru>
 */

#include <drivers/drv_hrt.h>

#include <uORB/uORB.h>
#include <uORB/Subscription.hpp>
#include <uORB/Publication.hpp>
#include <uORB/topics/precnav_command.h>
#include <uORB/topics/vehicle_command.h>
#include <uORB/topics/vehicle_command_ack.h>

#include <precnav/precnav_plugin.hpp>
#include <precnav/cam_state.hpp>

class cam_control : public precnav_plugin
{
private:
    enum stage_t { STAGE_NONE, STAGE_MODE, STAGE_ON_OFF };
public:
	void describe(size_t lvl) const override;
protected:
	void on_active() override { on_any(); }
	void on_inactive() override { on_any(); }
    void on_deactivate() override;
private:
    void on_any();
public:
    bool is_valid() const { return _cam.actual_mode != CAM_INVALID; }
    bool is_front() const { return _cam.actual_mode == CAM_FRONT;   }
    bool is_down()  const { return _cam.actual_mode == CAM_DOWN;    }
    bool is_off()   const { return _cam.actual_mode == CAM_OFF;     }
    bool is_on()    const { return is_front() || is_down();         }
    cam_mode_t get_mode() const { return _cam.actual_mode; }
public:
    void switch_front() { _cam.trans_prepare(CAM_FRONT); }
    void switch_down()  { _cam.trans_prepare(CAM_DOWN);  }
    void switch_off()   { _cam.trans_prepare(CAM_OFF);   }
private:
    uORB::Publication < vehicle_command_s > _command_pub;
    uORB::Subscription < vehicle_command_ack_s > _command_ack_sub;
private:
    cam_state_s _cam;
    stage_t _stage;
private:
    DEFINE_PARAMETERS(
        (ParamInt<px4::params::PN_CAM_DELAY>) _cam_delay,
        (ParamInt<px4::params::PN_CC_SYS_ID>) _companion_sys_id,
        (ParamInt<px4::params::PN_CC_COMP_ID>) _companion_comp_id
    )
public:
    cam_control(precnav * parent)
        :  precnav_plugin(parent)
        , _command_pub(ORB_ID(vehicle_command))
        , _command_ack_sub(ORB_ID(vehicle_command_ack))
    {
    }
    virtual ~cam_control()
    {
    }
private:
    uint16_t get_expected_command() const;
    void prepare_expected_command();
    void prepare_streaming_command(bool on);
    bool advance_stage();
    void reset_stage();
};
