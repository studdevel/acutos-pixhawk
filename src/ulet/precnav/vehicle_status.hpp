/****************************************************************************
 *
 *   Copyright (c) 2012-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#pragma once

/**
 * @file vehicle_status.hpp
 * 
 * Vehicle status helper.
 *
 * @author Vasilevsky Alexander <a.kalaider@yandex.ru>
 */

#include <px4_log.h>

#include <uORB/uORB.h>
#include <uORB/Subscription.hpp>
#include <uORB/Publication.hpp>
#include <uORB/topics/vehicle_land_detected.h>

#include <precnav/precnav_plugin.hpp>

class vehicle_status : public precnav_plugin
{
public:
    void describe(size_t lvl) const override;
    void run(bool) override;
public:
    bool is_armed() const { return _armed; }
    bool is_landed() const { return _landed; }
    bool is_taken_off() const { return !_landed; }
    bool is_just_landed() const { return valid() && !_was_landed && _landed; }
    bool is_just_taken_off() const { return valid() && _was_landed && !_landed; }
    bool valid() const { return _was_updated; }
private:
    uORB::Subscription < vehicle_land_detected_s > _land_sub;
private:
    bool _was_updated;
    bool _was_landed;
    bool _landed;
    bool _armed;
public:
    vehicle_status(precnav * core)
        : precnav_plugin(core)
        , _land_sub(ORB_ID(vehicle_land_detected))
        , _was_updated(false)
    {
    }
    virtual ~vehicle_status()
    {
    }
};
