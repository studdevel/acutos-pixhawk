/****************************************************************************
 *
 *   Copyright (c) 2012-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/**
 * @file optical_rec.cpp
 * 
 * Optical target estimator.
 *
 * @author Vasilevsky Alexander <a.kalaider@yandex.ru>
 */

#include <precnav/optical_rec.hpp>
#include <precnav/precnav.hpp>

#include <limits>

void optical_rec::describe(size_t lvl) const
{
}

void optical_rec::on_active()
{
    if (!_core->get_camera_controller().is_valid())
    {
        reset(RESET_BOTH);
        return;
    }

    // bottom distance is required only if camera looks down
    // however it is better to update it in both camera modes to
    // detect possible errors or limitations of distance sensor
    if (!update_altitude())
    {
        static bool warned = false;
        if (!warned)
        {
            PX4_WARN("bottom distance is invalid, waiting for valid distance\n"
                "this warning will not appear again");
            warned = true;
        }
    }

    if (is_frame_valid())
    {
        estimate();
    }
    else
    {
        // mark all as invisible, invalidate all non-locked targets
        reset(RESET_BOTH);
    }
}

// TODO: use gps approximation if above XXX m or below YYY m
bool optical_rec::update_altitude()
{
    if (_core->is_local_position_updated())
    {
        _rel_alt = -_core->get_local_position().z;
        if (_prefer_rng.get() != 0)
        {
            if (!_core->get_local_position().dist_bottom_valid)
            {
                ++_sens_fail_count;
                // TODO: probably use timestamp-based limit
                // since distance sensor update rate may differ
                // from other sensors rate
                if (_sens_fail_count >= (size_t)_max_sensor_failures.get())
                {
                    return false;
                }
                // use stale but not too bad values
            }
            else
            {
                _sens_fail_count = 0;
            }
            _rel_alt = _core->get_local_position().dist_bottom;
        }
    }
    return true;
}

void optical_rec::reset(reset_t req)
{
    if (req == RESET_FULL)
    {
        for (size_t i = 0; i < target_repository::max_targets; ++i)
            _core->get_target_repository().targets()[i].estimator->reset();
    }
    else if (req == RESET_VISIBILITY)
    {
        for (size_t i = 0; i < target_repository::max_targets; ++i)
            _core->get_target_repository().targets()[i].estimator->set_visible(false);
    }
    else // if (req == RESET_LOCK || req == RESET_BOTH)
    {
        for (size_t i = 0; i < target_repository::max_targets; ++i)
        {
            if (!_core->get_target_repository().targets()[i].estimator->locked() &&
                !_core->get_target_repository().targets()[i].estimator->visible())
            {
                _core->get_target_repository().targets()[i].estimator->reset();
            }
            if (req == RESET_BOTH)
                _core->get_target_repository().targets()[i].estimator->set_visible(false);
        }
    }
}

void optical_rec::accumulate(uint8_t tag)
{
    _core->get_target_repository()
        .get_default_estimator().supply(
        _estimated_position,
        _estimated_size,
        _estimated_dist);

    auto & cur = _core->get_target_repository()
        .get_default_estimator().state().local;

    target_repository::target_s * target = nullptr;
    switch (tag)
    {
        case optical_recognition_result_s::OBJECT_TYPE_LANDING_POS:
        {
            // search for existing target
            for (size_t i = 0; i < target_repository::max_targets; ++i)
            {
                auto tgt = &_core->get_target_repository().targets()[i];
                if (tgt->id == -1) continue;
                if (tgt->type != tag) continue;
                target = tgt; break;
            }
            if (target == nullptr)
            {
                target = _core->get_target_repository().alloc(
                    optical_recognition_result_s::OBJECT_TYPE_LANDING_POS);
            }
            break;
        }
        case optical_recognition_result_s::OBJECT_TYPE_BALLOON:
        default:
        {
            // search for the closest target
            float min_dist = (std::numeric_limits < float > :: max)();
            for (size_t i = 0; i < target_repository::max_targets; ++i)
            {
                auto tgt = &_core->get_target_repository().targets()[i];
                if (tgt->id == -1) continue;
                if (tgt->type != tag) continue;
                if (!tgt->estimator->valid()) continue;

                auto & their = tgt->estimator->state().local;

                float dist =
                    (cur.x - their.x) * (cur.x - their.x) +
                    (cur.y - their.y) * (cur.y - their.y) +
                    (cur.z - their.z) * (cur.z - their.z);

                if (dist < min_dist)
                {
                    min_dist = dist;
                    target = tgt;
                }
            }
            // if found, try to match the target to the current target
            if (target != nullptr)
            {
                const float tol = _balloon_tolerance.get();

                if (min_dist > tol * tol)
                    target = nullptr;
            }
            // otherwise try to allocate new target
            if (target == nullptr)
            {
                target = _core->get_target_repository().alloc(
                    optical_recognition_result_s::OBJECT_TYPE_BALLOON);
                if (target == nullptr)
                {
                    // inability to allocate new target indicates
                    // either that there is more targets than it should be
                    PX4_WARN("no free channel for new target");
                    _core->get_target_repository()
                        .get_default_estimator().describe(1);
                }
            }
        }
    }
    if (target == nullptr) // no free channel found
        return;
    target->estimator->supply(
        _estimated_position,
        _estimated_size,
        _estimated_dist);
}

bool optical_rec::is_frame_valid() const
{
    return _core->get_recognition_result().num_of_objects > 0;
}

void optical_rec::estimate()
{
    // clear visibility flags of all targets
    reset(RESET_VISIBILITY);

    if (_core->get_camera_controller().is_front())
    {
        if (_core->get_recognition_result().object_type !=
            optical_recognition_result_s::OBJECT_TYPE_BALLOON) return;
        estimate_front();
    }
    else if (_core->get_camera_controller().is_down())
    {
        if (_core->get_recognition_result().object_type !=
            optical_recognition_result_s::OBJECT_TYPE_LANDING_POS) return;
        estimate_down();
    }

    // reset lock of all invisible but not yet locked targets
    reset(RESET_LOCK);
}

void optical_rec::estimate_down()
{
    project_down(
        _core->get_recognition_result().objects[0].center_x,
        _core->get_recognition_result().objects[0].center_y,
        _rel_alt,
        _estimated_position);

    _estimated_distance = (float)hypot(
        _estimated_position.x, _estimated_position.y
    );

    _num_of_estimated_borders = _core->get_recognition_result().objects[0].num_of_border_points;
    for (size_t i = 0; i < _num_of_estimated_borders; ++i)
    {
        project_down(
            _core->get_recognition_result().objects[0].border_x[i],
            _core->get_recognition_result().objects[0].border_y[i],
            _rel_alt,
            _estimated_borders[i]);
        project_down(
            _core->get_recognition_result().objects[0].border_x[i],
            _core->get_recognition_result().objects[0].border_y[i],
            1,
            _estimated_borders_near[i]);
    }
    get_size_down(_estimated_borders, _num_of_estimated_borders, _estimated_size);
    get_size_down(_estimated_borders_near, _num_of_estimated_borders, _estimated_size_near);
    if (_estimated_size.d > 0)
    {
        // distance D - S0 size
        // distance 1 - S  size
        // D = S0 / S
        _estimated_dist = (double)_landing_pos_size.get() /
            ((_estimated_size_near.w + _estimated_size_near.h) / 2);
    }

    accumulate(optical_recognition_result_s::OBJECT_TYPE_LANDING_POS);
}

void optical_rec::estimate_front()
{
    for (size_t o = 0; o < _core->get_recognition_result().num_of_objects; ++o)
    {
        project_front(
            _core->get_recognition_result().objects[o].center_x,
            _core->get_recognition_result().objects[o].center_y,
            1,
            _estimated_position);

        _num_of_estimated_borders = _core->get_recognition_result().objects[0].num_of_border_points;
        for (size_t i = 0; i < _num_of_estimated_borders; ++i)
        {
            project_front(
                _core->get_recognition_result().objects[o].border_x[i],
                _core->get_recognition_result().objects[o].border_y[i],
                1,
                _estimated_borders[i]);
        }
        get_size_front(_estimated_borders, _num_of_estimated_borders, _estimated_size_near);
        if (_estimated_size_near.d > 0)
        {
            double s0 = (double)_balloon_size.get();
            double d0 = std::sqrt(
                _estimated_position.x * _estimated_position.x +
                _estimated_position.y * _estimated_position.y +
                _estimated_position.z * _estimated_position.z);

            // distance D  - S0 size
            // distance D0 - S  size
            // D = D0 * S0 / S

            _estimated_dist = d0 * s0 / _estimated_size_near.d;
            _estimated_size = { s0, s0, s0 };
            _estimated_distance = _estimated_dist;

            project_front(
                _core->get_recognition_result().objects[o].center_x,
                _core->get_recognition_result().objects[o].center_y,
                _estimated_dist,
                _estimated_position);
        }

        accumulate(optical_recognition_result_s::OBJECT_TYPE_BALLOON);
    }
}

void optical_rec::project_down(float x, float y, float d, local_pos_s & p) const
{
    const float fovx = _cam_fovx.get();
    const float fovy = _cam_fovy.get();
    const float aspect = std::tan(fovx / 2) / std::tan(fovy / 2);

    float fvh = 0.5f / (float)std::tan(fovx / 2.f);

    // translate from top left corner originated frame
    // to FRD (Front Right Down) XYZ body frame
    //
    //    X' (XYZ frame)         |
    // +--^--> X (image frame)   |   Y' =  X   =def= cx
    // |  |  |                   |   X' = -Y   =def= cy
    // |  +--> Y' (XYZ frame)    |
    // |     |
    // v-----+ Y (image frame)
    //
    float cx = -(y - .5f);
    float cy = (x - .5f) / aspect;

    // NOTICE on XYZ body frame and NED frame:
    //   NED = (North, East, Down), origin is an Earth center
    //   XYZ = (X, Y, Down), origin is a mass center
    // X is a natural local X axis (from mass center
    // to vehicle heading)

    // _att.q is a quaternion describing rotation from NED
    // to local body XYZ. Assume camera is in (0,0,0) i.e.
    // in the vehicle mass center. TODO: camera displacement
    matrix::Dcmf rot = matrix::Quatf(_att_sub.get().q);
    matrix::Vector3f vct(cx, cy, fvh);
    vct = rot * vct;

    // intersect (0,0,0)-vct line with Z=d plane
    float ax = vct(0) * d / vct(2);
    float ay = vct(1) * d / vct(2);

    p.x = (double)ax;
    p.y = (double)ay;
    p.z = (double)d;
}

void optical_rec::project_front(float x, float y, float d, local_pos_s & p) const
{
    const float fovx = _cam_fovx.get();
    const float fovy = _cam_fovy.get();
    const float aspect = std::tan(fovx / 2) / std::tan(fovy / 2);

    float fvh = 0.5f / (float)std::tan(fovx / 2.f);

    // translate from top left corner originated frame
    // to FRD (Front Right Down) XYZ body frame
    //
    //                           |
    // +-----> X (image frame)   |   Y' = X   =def= cy
    // |     |                   |   Z' = Y   =def= cz
    // |  +--> Y' (XYZ frame)    |
    // |  |  |
    // v--v--+ Y (image frame)
    //    Z' (XYZ frame)
    //
    float cz = (y - .5f);
    float cy = (x - .5f) / aspect;

    matrix::Dcmf rot = matrix::Quatf(_att_sub.get().q);
    matrix::Vector3f vct(fvh, cy, cz);
    vct = rot * vct;

    p.x = (double)(vct(0) * d);
    p.y = (double)(vct(1) * d);
    p.z = (double)(vct(2) * d);
}

void optical_rec::get_size_down(const local_pos_s * borders, size_t n_borders, size_s & size) const
{
    // landing position is identified with 4 corner points
    if (n_borders == 4)
    {
        // vertices
        matrix::Vector2f
            v0(borders[0].x, borders[0].y),
            v1(borders[1].x, borders[1].y),
            v2(borders[2].x, borders[2].y),
            v3(borders[3].x, borders[3].y);

        // edges and diagonals
        matrix::Vector2f
            e0 = (v0 - v1), e1 = (v1 - v2),
            e2 = (v2 - v3), e3 = (v3 - v0),
            d1 = (v0 - v2), d2 = (v1 - v3);

        // average metrics
        size = {
            (e0.norm() + e2.norm()) / 2,
            (e1.norm() + e3.norm()) / 2,
            (d1.norm() + d2.norm()) / 2
        };
    }
    else
    {
        size = { 0, 0, 0 };
    }
}

void optical_rec::get_size_front(const local_pos_s * borders, size_t n_borders, size_s & size) const
{
    // calculate target size
    if (n_borders >= 4)
    {
        // simplest algorithm for now, assume w = h = max dimension
        double sz = 0;
        for (size_t i = 0; i < n_borders; ++i)
        for (size_t j = 0; j < n_borders; ++j)
        {
            if (i == j) continue;
            sz = fmax(
                sz,
                (borders[i].x - borders[j].x) * (borders[i].x - borders[j].x) +
                (borders[i].y - borders[j].y) * (borders[i].y - borders[j].y) +
                (borders[i].z - borders[j].z) * (borders[i].z - borders[j].z)
            );
        }
        sz = std::sqrt(sz);
        size = { sz, sz, sz };
    }
    else
    {
        size = { 0, 0, 0 };
    }
}
