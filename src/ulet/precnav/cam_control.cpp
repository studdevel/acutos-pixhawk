/****************************************************************************
 *
 *   Copyright (c) 2012-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/**
 * @file cam_control.cpp
 * 
 * Camera-controlling.
 *
 * @author Vasilevsky Alexander <a.kalaider@yandex.ru>
 */

#include <precnav/cam_control.hpp>
#include <precnav/precnav.hpp>

void cam_control::on_deactivate()
{
    _cam.trans_prepare(CAM_OFF);
}

void cam_control::on_any()
{
    if (_core->is_command_updated() &&
        _core->get_command().command_type == precnav_command_s::TYPE_CMD)
    {
        if (_core->get_command().command == precnav_command_s::CMD_SET_CAM_MODE)
        {
            _cam.trans_prepare(_core->get_command().param0 == precnav_command_s::CAM_MODE_FRONT
                ? CAM_FRONT : (_core->get_command().param0 == precnav_command_s::CAM_MODE_DOWN)
                ? CAM_DOWN  : CAM_OFF);
        }
        else if (_core->get_command().command == precnav_command_s::CMD_SET_STREAMING_MODE)
        {
            prepare_streaming_command(_core->get_command().param0 == precnav_command_s::STREAMING_MODE_ON);
            // don't expect any ack, just send and forget
            _command_pub.update();
        }

        PX4_DEBUG("cam_control: got command %d, resulted in mode %d", (int)_core->get_command().param0, (int)_cam.desired_mode);
    }

    bool need_message = true;

    // if new transition required
    if (!_cam.trans_commit()) // in-progress or noop
    {
        need_message = false; // no need to send any message now
        if (_cam.is_trans_active())
        {
            // check if there is an ack for the current stage
            while (_command_ack_sub.update())
            {
                // if command was acked in time, advance the stage
                if ((_command_ack_sub.get().command == get_expected_command()) &&
                    /**
                     * NOTE on how commands and acks are handled
                     *
                     * on real hardware mavlink module polls for
                     * from_external=false commands and sends
                     * them via all available channels. it then
                     * listens for acks and matches them with queued
                     * commands. ack must arrive from the (
                     * target_system/ target_component) specified
                     * in the command. mavlink takes care of
                     * retransmission if it is required. ack is
                     * then published with from_external=true.
                     *
                     * in simulator no (queued) command-ack matcher
                     * exist, but there is only one system that can
                     * accept commands (simulator). simulator itself
                     * also does not match system or component id
                     * with its internal ids. so any external ack
                     * may be accepted.
                     */
                    (_command_ack_sub.get().from_external) &&
                    (_command_ack_sub.get().timestamp > _cam.timestamp) &&
                    ((_command_ack_sub.get().timestamp - _cam.timestamp) < 1000 * (uint64_t)_cam_delay.get()))
                {
                    // if the final stage reached, end the transaction
                    if (!advance_stage())
                    {
                        _cam.trans_end();
                        PX4_DEBUG("cam_control: got answer, done");
                        return;
                    }
                    // otherwise, advance the transaction
                    _cam.trans_advance();
                    need_message = true; // need to send new message
                    PX4_DEBUG("cam_control: got answer, advance stage");
                }
            }
            // if no ack received in time, reset transaction time and retry
            if (_cam.is_trans_active() &&
                _cam.can_end_trans(1000 * _cam_delay.get()))
            {
                reset_stage();
                _cam.trans_advance();
                need_message = true; // need to resend message
                PX4_DEBUG("cam_control: got no answer for too long");
            }
        }
    }
    else // new transaction
    {
        reset_stage();
    }

    if (need_message)
    {
        prepare_expected_command();
        _command_pub.get().timestamp = hrt_absolute_time();
        _command_pub.update();
        PX4_DEBUG("cam_control: sending mew message, stage is %d, mode is %d", (int)_stage, (int)_cam.committed_mode);
    }
}

void cam_control::reset_stage()
{
    switch (_cam.committed_mode)
    {
        case CAM_OFF:   { _stage = STAGE_ON_OFF; break; }
        case CAM_DOWN:
        case CAM_FRONT: { _stage = STAGE_MODE; break;   }
        default:        { _stage = STAGE_NONE; break;   } // should not occur ever
    }
}

bool cam_control::advance_stage()
{
    if (_stage == STAGE_MODE)
    {
        _stage = STAGE_ON_OFF;
        return true; // non-final stage reached
    }
    // otherwise stage is STAGE_ON_OFF
    return false; // final stage reached
}

uint16_t cam_control::get_expected_command() const
{
    switch (_cam.committed_mode)
    {
        case CAM_OFF: return vehicle_command_s::VEHICLE_CMD_VIDEO_STOP_CAPTURE;
        case CAM_DOWN:
        case CAM_FRONT: {
            if (_stage == STAGE_ON_OFF)
                return vehicle_command_s::VEHICLE_CMD_VIDEO_START_CAPTURE;
            else
                return vehicle_command_s::VEHICLE_CMD_SET_CAMERA_MODE;
        }
        default: return 0; // should not occur ever
    }
}

void cam_control::prepare_expected_command()
{
    _command_pub.get().command = get_expected_command();
    _command_pub.get().param1 = (
        (_cam.committed_mode == CAM_DOWN) ? 0 : 1
    );
    _command_pub.get().from_external = false;
    _command_pub.get().target_system = _companion_sys_id.get();
    _command_pub.get().target_component = _companion_comp_id.get();
}

void cam_control::prepare_streaming_command(bool on)
{
    _command_pub.get().command = on ?
        vehicle_command_s::VEHICLE_CMD_VIDEO_START_STREAMING :
        vehicle_command_s::VEHICLE_CMD_VIDEO_STOP_STREAMING;
    _command_pub.get().from_external = false;
    _command_pub.get().target_system = _companion_sys_id.get();
    _command_pub.get().target_component = _companion_comp_id.get();
}

void cam_control::describe(size_t lvl) const
{
    PNP_PRINT(lvl, "camera: %s", !is_valid()
        ? "unknown"
        : is_front() ? "front"
        : is_down()  ? "down"
        : "off");
}
