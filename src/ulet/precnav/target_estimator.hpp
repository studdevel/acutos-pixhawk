/****************************************************************************
 *
 *   Copyright (c) 2012-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#pragma once

/**
 * @file target_estimator.hpp
 *
 * Helper class that holds current target position.
 *
 * @author Vasilevsky Alexander <a.kalaider@yandex.ru>
 */

#include <math.h>
#include <matrix/math.hpp>
#include <lib/ecl/geo/geo.h>

#include <precnav/precnav_plugin.hpp>
#include <precnav/precnav_util.hpp>

class target_estimator : public precnav_plugin
{
public:
    struct state_s
    {
        size_t count;
        bool visible;
        uint64_t timestamp;
        pos_s glob;
        local_pos_s local;
        local_pos_s rel;
        local_pos_s self;
        size_s size;
        double dist;
    };
private:
    map_projection_reference_s _home_ref;
    state_s _state;
private:
    DEFINE_PARAMETERS(
        (ParamInt<px4::params::PN_INIT_LOCK_CNT>) _lock_count
    )
public:
    target_estimator(precnav * parent)
        : precnav_plugin(parent)
    {
        reset();
    }
public:
    const state_s & state() const { return _state; }
    bool valid() const { return _state.count > 0; }
    bool visible() const { return _state.visible; }
    bool locked() const { return _state.count >= (size_t)_lock_count.get(); }
    bool size_valid() const { return (_state.size.w > 0) && (_state.size.h > 0); }
    bool dist_valid() const { return _state.dist > 0; }
public:
	void describe(size_t lvl) const override;
public:
    void reset()
    {
        _state = {};
    }

    void set_visible(bool visible)
    {
        _state.visible = visible;
    }

    void supply(local_pos_s rel, size_s size, double dist);
};
