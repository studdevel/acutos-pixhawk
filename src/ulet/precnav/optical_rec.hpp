/****************************************************************************
 *
 *   Copyright (c) 2012-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#pragma once

/**
 * @file optical_rec.hpp
 * 
 * Optical target estimator.
 *
 * @author Vasilevsky Alexander <a.kalaider@yandex.ru>
 */

#include <drivers/drv_hrt.h>

#include <uORB/uORB.h>
#include <uORB/Subscription.hpp>
#include <uORB/Publication.hpp>
#include <uORB/topics/vehicle_attitude.h>
#include <uORB/topics/optical_recognition_result.h>
#include <uORB/topics/precnav_command.h>
#include <uORB/topics/actuator_controls.h>

#include <precnav/precnav_plugin.hpp>
#include <precnav/cam_state.hpp>
#include <precnav/precnav_util.hpp>

class optical_rec : public precnav_plugin
{
private:
    enum reset_t { RESET_FULL, RESET_LOCK, RESET_VISIBILITY, RESET_BOTH };
public:
	void describe(size_t lvl) const override;
protected:
	void on_active() override;
public:
    bool is_valid() const { return true; }
    float get_distance() const { return _estimated_distance; }
private:
    void reset(reset_t req);
    void estimate();
    void estimate_down();
    void estimate_front();
    void project_down(float x, float y, float h, local_pos_s & p) const;
    void project_front(float x, float y, float h, local_pos_s & p) const;
    void get_size_down(const local_pos_s * borders, size_t n_borders, size_s & size) const;
    void get_size_front(const local_pos_s * borders, size_t n_borders, size_s & size) const;
    void accumulate(uint8_t tag);
    bool is_frame_valid() const;
    bool update_altitude();
private:
    uORB::Subscription < vehicle_attitude_s > _att_sub;
private:
    cam_mode_t _prev_mode;
    size_t _lock_count;
    size_t _fail_count;
    size_t _sens_fail_count;
    float _rel_alt;
    float _estimated_distance;
    local_pos_s _estimated_position;
    local_pos_s _estimated_borders[recognized_object_s::MAX_BORDER_POINTS];
    local_pos_s _estimated_borders_near[recognized_object_s::MAX_BORDER_POINTS];
    size_t _num_of_estimated_borders;
    size_s _estimated_size;
    size_s _estimated_size_near;
    double _estimated_dist;
private:
    DEFINE_PARAMETERS(
        (ParamFloat<px4::params::PN_CAM_FOVX>) _cam_fovx,
        (ParamFloat<px4::params::PN_CAM_FOVY>) _cam_fovy,
        (ParamFloat<px4::params::PN_CAM_DOFF_X>) _cam_doffx,
        (ParamFloat<px4::params::PN_CAM_DOFF_Y>) _cam_doffy,
        (ParamFloat<px4::params::PN_CAM_DOFF_Z>) _cam_doffz,
        (ParamFloat<px4::params::PN_CAM_FOFF_X>) _cam_foffx,
        (ParamFloat<px4::params::PN_CAM_FOFF_Y>) _cam_foffy,
        (ParamFloat<px4::params::PN_CAM_FOFF_Z>) _cam_foffz,
        (ParamFloat<px4::params::PN_TGT_DIM>) _landing_pos_size,
        (ParamFloat<px4::params::PN_BALL_DIM>) _balloon_size,
        (ParamFloat<px4::params::PN_BALL_TOL>) _balloon_tolerance,
        (ParamInt<px4::params::PN_MAX_SENS_FAIL>) _max_sensor_failures,
        (ParamInt<px4::params::PN_PREFER_RNG>) _prefer_rng
    )
public:
    optical_rec(precnav * parent)
        :  precnav_plugin(parent)
        , _att_sub(ORB_ID(vehicle_attitude))
        , _sens_fail_count(0)
        , _rel_alt(0)
        , _estimated_distance(0)
        , _estimated_position({ 0, 0, 0 })
        , _num_of_estimated_borders(0)
    {
    }
    virtual ~optical_rec()
    {
    }
};
