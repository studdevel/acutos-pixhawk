/****************************************************************************
 *
 *   Copyright (c) 2012-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/**
 * Parameters for precnav module.
 *
 * @author Vasilevsky Alexander <a.kalaider@yandex.ru>
 */

#include <px4_config.h>
#include <parameters/param.h>

/**
 * Minimal update rate.
 *
 * @unit ms
 * @min 1
 * @max 1000
 * @group precnav
 */
PARAM_DEFINE_INT32(PN_UPDATE_RATE, 50);

/**
 * Target acceptance radius.
 *
 * @unit m
 * @min 0.01
 * @max 3
 * @group precnav
 */
PARAM_DEFINE_FLOAT(PN_TGT_ACC_R, 0.5f);

/**
 * Safe Z distance over drop target.
 *
 * @unit m
 * @min 1
 * @max 10
 * @group precnav
 */
PARAM_DEFINE_FLOAT(PN_TGT_SAFE_H, 2.0f);

/**
 * Size (max of width/height) of the target position.
 *
 * @unit m
 * @min 0.1
 * @max 10
 * @group precnav
 */
PARAM_DEFINE_FLOAT(PN_TGT_DIM, 0.7f);

/**
 * Balloon diameter.
 *
 * @unit m
 * @min 0.1
 * @max 10
 * @group precnav
 */
PARAM_DEFINE_FLOAT(PN_BALL_DIM, 0.4f);

/**
 * Minimal allowed distance between balloon targets
 * i.e. static target matching tolerance.
 *
 * @unit m
 * @min 0.1
 * @max 1000
 * @group precnav
 */
PARAM_DEFINE_FLOAT(PN_BALL_TOL, 5.0f);

/**
 * Limit of sequential distance sensor failures.
 *
 * @min 1
 * @max 1000
 * @group precnav
 */
PARAM_DEFINE_INT32(PN_MAX_SENS_FAIL, 20);

/**
 * Limit of sequential recognition failures
 * after which target lock needs to be reobtained.
 *
 * @min 1
 * @max 1000
 * @group precnav
 */
PARAM_DEFINE_INT32(PN_MAX_LOCK_FAIL, 20);

/**
 * Number of initial valid recognition messages
 * required to obtain target lock.
 *
 * @min 1
 * @max 1000
 * @group precnav
 */
PARAM_DEFINE_INT32(PN_INIT_LOCK_CNT, 20);

/**
 * Camera mode switch channel.
 *
 * @min 0
 * @max 6
 * @group precnav
 */
PARAM_DEFINE_INT32(PN_CAM_CHAN, 0);

/**
 * Camera mode switch duration.
 *
 * @unit ms
 * @min 100
 * @max 5000
 * @group precnav
 */
PARAM_DEFINE_INT32(PN_CAM_DELAY, 1000);

/**
 * Camera horizontal field of view.
 *
 * @unit rad
 * @min 0
 * @max 3.1415
 * @group precnav
 */
PARAM_DEFINE_FLOAT(PN_CAM_FOVX, 0.25f);

/**
 * Camera vertial field of view.
 *
 * @unit rad
 * @min 0
 * @max 3.1415
 * @group precnav
 */
PARAM_DEFINE_FLOAT(PN_CAM_FOVY, 0.25f);

/**
 * Camera X offset (relative to local body XYZ frame)
 * in mode DOWN (looks in Z direction).
 *
 * @unit m
 * @min 0
 * @max 0.5
 * @group precnav
 */
PARAM_DEFINE_FLOAT(PN_CAM_DOFF_X, 0);

/**
 * Camera Y offset (relative to local body XYZ frame)
 * in mode DOWN (looks in Z direction).
 *
 * @unit m
 * @min 0
 * @max 0.5
 * @group precnav
 */
PARAM_DEFINE_FLOAT(PN_CAM_DOFF_Y, 0);

/**
 * Camera Z offset (relative to local body XYZ frame)
 * in mode DOWN (looks in Z direction).
 *
 * @unit m
 * @min 0
 * @max 0.5
 * @group precnav
 */
PARAM_DEFINE_FLOAT(PN_CAM_DOFF_Z, 0);

/**
 * Camera X offset (relative to local body XYZ frame)
 * in mode FORWARD (looks in X direction).
 *
 * @unit m
 * @min 0
 * @max 0.5
 * @group precnav
 */
PARAM_DEFINE_FLOAT(PN_CAM_FOFF_X, 0);

/**
 * Camera Y offset (relative to local body XYZ frame)
 * in mode FORWARD (looks in X direction).
 *
 * @unit m
 * @min 0
 * @max 0.5
 * @group precnav
 */
PARAM_DEFINE_FLOAT(PN_CAM_FOFF_Y, 0);

/**
 * Camera Z offset (relative to local body XYZ frame)
 * in mode FORWARD (looks in X direction).
 *
 * @unit m
 * @min 0
 * @max 0.5
 * @group precnav
 */
PARAM_DEFINE_FLOAT(PN_CAM_FOFF_Z, 0);

/**
 * Whether automatic actions allowed. If 1,
 * vehicle will do nothing without ack.
 *
 * @unit bool
 * @min 0
 * @max 1
 * @group precnav
 */
PARAM_DEFINE_INT32(PN_ACK_ALL, 0);

/**
 * Whether automatic takeoff allowed.
 *
 * @unit bool
 * @min 0
 * @max 1
 * @group precnav
 */
PARAM_DEFINE_INT32(PN_ACK_TAKEOFF, 0);

/**
 * Whether automatic landing allowed.
 *
 * @unit bool
 * @min 0
 * @max 1
 * @group precnav
 */
PARAM_DEFINE_INT32(PN_ACK_LAND, 0);

/**
 * Whether automatic progress allowed.
 *
 * @unit bool
 * @min 0
 * @max 1
 * @group precnav
 */
PARAM_DEFINE_INT32(PN_ACK_JOB, 0);

/**
 * System id of companion computer.
 *
 * @min 0
 * @max 255
 * @group precnav
 */
PARAM_DEFINE_INT32(PN_CC_SYS_ID, 0);

/**
 * Component id of companion computer.
 *
 * @min 0
 * @max 255
 * @group precnav
 */
PARAM_DEFINE_INT32(PN_CC_COMP_ID, 200);

/**
 * Prefer range finder bottom distance estimation.
 *
 * @unit bool
 * @min 0
 * @max 1
 * @group precnav
 */
PARAM_DEFINE_INT32(PN_PREFER_RNG, 1);
