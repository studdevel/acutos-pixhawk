/****************************************************************************
 *
 *   Copyright (c) 2012-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/**
 * @file nav_modes.cpp
 * 
 * Navigation modes.
 *
 * @author Vasilevsky Alexander <a.kalaider@yandex.ru>
 */

#include <precnav/nav_modes.hpp>
#include <precnav/precnav.hpp>

#include <drivers/drv_hrt.h>
#include <commander/px4_custom_mode.h>

void nav_mode_base::fail_fast(nav_mode_t fallback_to)
{
    _result = NAV_RESULT_FAILURE;
    ((ulet_navigator *)_core->get_navigator())->set_nav_mode(fallback_to);
}

void nav_mode_base::report_success(nav_mode_t switch_to)
{
    _result = NAV_RESULT_SUCCESS;
    ((ulet_navigator *)_core->get_navigator())->set_nav_mode(switch_to);
}

bool nav_mode_base::ensure_armed()
{
    if (_core->get_status().is_armed()) return true;

    PX4_ERR("%s: cannot operate while disarmed", self_name());
    fail_fast(NAV_MODE_LANDED);
    return false;
}

void nav_mode_base::on_active()
{
    _mission_updated = _mission_sub.update();
    _ack_updated = _ack_sub.update();
    _sp_updated = _sp_sub.update();
}

// claim takeoff mode
void nav_mode_takeoff::on_activate()
{
    _result = NAV_RESULT_UNKNOWN;
    _got_ack = false;
    _got_mission_ack = false;
    _ack_fails = 0;

    if (!ensure_armed()) return;
    if (_core->get_status().is_taken_off())
    {
        PX4_INFO("takeoff: already done");
        report_success(NAV_MODE_IDLE);
        return;
    }

    int32_t sys_id = 0;  param_get(param_find("MAV_SYS_ID"), &sys_id);
    int32_t comp_id = 0; param_get(param_find("MAV_COMP_ID"), &comp_id);

    _cmd_pub.get().target_system = sys_id;
    _cmd_pub.get().target_component = comp_id;
    _cmd_pub.get().param7 = _core->get_global_position().alt + 10; // altitude // TODO: param
    _cmd_pub.get().param4 = NAN; // current yaw
    _cmd_pub.get().param5 = NAN; // current lat
    _cmd_pub.get().param6 = NAN; // current lon
    _cmd_pub.get().command = vehicle_command_s::VEHICLE_CMD_NAV_TAKEOFF;
    _cmd_pub.get().timestamp = hrt_absolute_time();
    _cmd_pub.update();
}

void nav_mode_takeoff::on_active()
{
    nav_mode_base::on_active();

    if (!_core->get_status().is_armed()) return;

    // try to get ack from commander / navigator
    if (!_got_ack && _ack_updated)
    {
        if (_ack_sub.get().command == vehicle_command_s::VEHICLE_CMD_NAV_TAKEOFF &&
            _ack_sub.get().result == vehicle_command_s::VEHICLE_CMD_RESULT_ACCEPTED)
        {
                PX4_INFO("%s: got ack from commander/navigator", self_name());
                _got_ack = true;
                _ack_fails = 0;
        }
        else if (++_ack_fails >= 10)
        {
            PX4_ERR("%s: no ack from commander/navigator", self_name());
            fail_fast(NAV_MODE_LANDED);
        }
    }

    // wait for ack from the underlying navigation mode itself
    if (_got_ack && _sp_updated)
    {
        if (_sp_sub.get().current.valid &&
            _sp_sub.get().current.type == position_setpoint_s::SETPOINT_TYPE_LOITER)
        {
            PX4_INFO("%s: takeoff finished", self_name());
            report_success(NAV_MODE_IDLE);
        }
        else
        {
            if (++_ack_fails >= 10)
            {
                // TODO: check altitude is valid and continue
                PX4_ERR("%s: cannot detect takeoff finish", self_name());
                fail_fast(NAV_MODE_IDLE);
            }
        }
    }
}

void nav_mode_mission::on_activate()
{
    _result = NAV_RESULT_UNKNOWN;
    _got_ack = false;
    _got_mission_ack = false;
    _ack_fails = 0;

    if (!ensure_armed()) return;

    _cmd_pub.get().param1 = 1; // (VEHICLE|MAV)_MODE_FLAG_CUSTOM_MODE_ENABLED
    _cmd_pub.get().timestamp = hrt_absolute_time();

    int32_t sys_id = 0;  param_get(param_find("MAV_SYS_ID"), &sys_id);
    int32_t comp_id = 0; param_get(param_find("MAV_COMP_ID"), &comp_id);

    _cmd_pub.get().target_system = sys_id;
    _cmd_pub.get().target_component = comp_id;
    _cmd_pub.get().param1 = PX4_CUSTOM_MAIN_MODE_AUTO;
    _cmd_pub.get().param2 = PX4_CUSTOM_SUB_MODE_AUTO_MISSION;
    _cmd_pub.get().param3 = NAN;
    _cmd_pub.get().param4 = NAN; // current yaw
    _cmd_pub.get().param5 = NAN; // current lat
    _cmd_pub.get().param6 = NAN; // current lon
    _cmd_pub.get().param7 = NAN; // current altitude
    _cmd_pub.get().command = vehicle_command_s::VEHICLE_CMD_DO_SET_MODE;
    _cmd_pub.get().timestamp = hrt_absolute_time();
    _cmd_pub.update();
}

void nav_mode_mission::on_active()
{
    nav_mode_base::on_active();

    if (!_core->get_status().is_armed()) return;

    // try to get ack from commander / navigator
    if (!_got_ack && _ack_updated)
    {
        if (_ack_sub.get().command == vehicle_command_s::VEHICLE_CMD_DO_SET_MODE &&
            _ack_sub.get().result == vehicle_command_s::VEHICLE_CMD_RESULT_ACCEPTED)
        {
                PX4_INFO("%s: got ack from commander/navigator", self_name());
                _got_ack = true;
                _ack_fails = 0;
        }
        else if (++_ack_fails >= 10)
        {
            PX4_ERR("%s: no ack from commander/navigator", self_name());
            fail_fast(NAV_MODE_LANDED);
        }
    }

    // wait for ack from the underlying navigation mode itself
    if (_got_ack && _sp_updated)
    {
        if (_sp_sub.get().current.valid &&
            _mission_sub.get().finished &&
            _sp_sub.get().current.type == position_setpoint_s::SETPOINT_TYPE_LOITER)
        {
            PX4_INFO("%s: mission finished", self_name());
            report_success(NAV_MODE_IDLE);
        }
        else
        {
            if (++_ack_fails >= 10)
            {
                // TODO: check altitude is valid and continue
                PX4_ERR("%s: cannot detect mission finish", self_name());
                fail_fast(NAV_MODE_IDLE);
            }
        }
    }
}

void nav_mode_mission::on_deactivate()
{
    fail_fast(NAV_MODE_IDLE);
}

void nav_mode_precland::on_activate()
{
    _result = NAV_RESULT_UNKNOWN;
    _got_ack = false;
    _got_mission_ack = false;
    _ack_fails = 0;

    if (!ensure_armed()) return;

    int32_t sys_id = 0;  param_get(param_find("MAV_SYS_ID"), &sys_id);
    int32_t comp_id = 0; param_get(param_find("MAV_COMP_ID"), &comp_id);

    _cmd_pub.get().target_system = sys_id;
    _cmd_pub.get().target_component = comp_id;
    _cmd_pub.get().param1 = 1;  // VEHICLE_MODE_FLAG_CUSTOM_MODE_ENABLED
    _cmd_pub.get().param2 = PX4_CUSTOM_MAIN_MODE_AUTO;
    _cmd_pub.get().param3 = PX4_CUSTOM_SUB_MODE_AUTO_PRECLAND;
    _cmd_pub.get().command = vehicle_command_s::VEHICLE_CMD_DO_SET_MODE;
    _cmd_pub.get().timestamp = hrt_absolute_time();
    _cmd_pub.update();
    _pose_pub.get().abs_pos_valid = false;
}

void nav_mode_precland::on_active()
{
    nav_mode_base::on_active();

    // already landed (or just disarmed? not our problem)
    if (!_core->get_status().is_armed() || _core->get_status().is_landed())
    {
        PX4_INFO("%s: %s", self_name(), !_core->get_status().is_armed() ? "disarmed" : "landed");
        report_success(NAV_MODE_LANDED);
        return;
    }

    // try to get ack from commander / navigator
    if (!_got_ack && _ack_updated)
    {
        if (_ack_sub.get().command == vehicle_command_s::VEHICLE_CMD_DO_SET_MODE &&
            _ack_sub.get().result == vehicle_command_s::VEHICLE_CMD_RESULT_ACCEPTED)
        {
            PX4_INFO("%s: got ack from commander/navigator", self_name());
            _got_ack = true;
        }
        else if (++_ack_fails >= 10)
        {
            PX4_ERR("%s: no ack from commander/navigator", self_name());
            fail_fast(NAV_MODE_LANDED);
        }
    }

    if (_got_ack && _sp_updated)
    {
        // expected landing process termination
        if (_sp_sub.get().current.valid &&
            _sp_sub.get().current.type == position_setpoint_s::SETPOINT_TYPE_IDLE)
        {
            PX4_INFO("%s: landing finished", self_name());
            report_success(NAV_MODE_LANDED);
        }
    }
    // otherwise, send estimated position to navigation algorithm
    else
    {
        // TODO: sync precland parameters with ours
        auto target = _core->get_target_repository().current();
        float dist = -_core->get_local_position().z;
        if (_prefer_rng.get() != 0 && _core->get_local_position().dist_bottom_valid)
            dist = _core->get_local_position().dist_bottom;
        if (dist < _safe_dist.get()) return;
        _pose_pub.get().abs_pos_valid =
            _core->get_optical_recognizer().is_valid() &&
            (target != nullptr) &&
            target->estimator->valid() &&
            target->estimator->locked() &&
            target->estimator->visible();
        if (_pose_pub.get().abs_pos_valid)
        {
            local_pos_s pos = target->estimator->state().local;
            _pose_pub.get().x_abs = pos.x;
            _pose_pub.get().y_abs = pos.y;
            _pose_pub.get().z_abs = pos.z;
            _pose_pub.get().timestamp = hrt_absolute_time();
            _pose_pub.update();
        }
    }
}

void nav_mode_kill_ball::on_activate() {}
void nav_mode_kill_ball::on_active() {}
