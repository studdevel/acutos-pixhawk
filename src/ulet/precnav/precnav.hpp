/****************************************************************************
 *
 *   Copyright (c) 2012-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#pragma once

/**
 * @file precnav.hpp
 * 
 * Optical recognition-based precise navigation.
 *
 * @author Vasilevsky Alexander <a.kalaider@yandex.ru>
 */

#include <px4_module.h>
#include <px4_module_params.h>
#include <px4_config.h>
#include <px4_tasks.h>
#include <px4_posix.h>
#include <px4_getopt.h>
#include <lib/ecl/geo/geo.h>

#include <drivers/drv_hrt.h>

#include <uORB/uORB.h>
#include <uORB/Subscription.hpp>
#include <uORB/Publication.hpp>
#include <uORB/topics/vehicle_status.h>
#include <uORB/topics/vehicle_global_position.h>
#include <uORB/topics/vehicle_local_position.h>
#include <uORB/topics/optical_recognition_result.h>
#include <uORB/topics/home_position.h>
#include <uORB/topics/parameter_update.h>
#include <uORB/topics/precnav_command.h>

#include <precnav/cam_control.hpp>
#include <precnav/optical_rec.hpp>
#include <precnav/navigator.hpp>
#include <precnav/program.hpp>
#include <precnav/navigator.hpp>
#include <precnav/target_repository.hpp>
#include <precnav/vehicle_status.hpp>

// from msg file
enum ack_t
{
    ACK_ANY     = precnav_command_s::ACK_ANY,
    ACK_TAKEOFF = precnav_command_s::ACK_TAKEOFF,
    ACK_JOB     = precnav_command_s::ACK_JOB,
    ACK_LAND    = precnav_command_s::ACK_LAND,
    _ACK_MAX
};

class precnav : public ModuleBase < precnav >, public ModuleParams
{
public:
	static int task_spawn(int argc, char *argv[]);
	static precnav * instantiate(int argc, char *argv[]);
	static int custom_command(int argc, char *argv[]);
	static int print_usage(const char *reason = nullptr);
	void run() override;
	int print_status() override;
public:
    static int ack_command(int argc, char *argv[]);
    static int set_command(int argc, char *argv[]);
public:
    const vehicle_status_s & get_vehicle_status() const { return _status_sub.get(); }
    const vehicle_global_position_s & get_global_position() const { return _pos_sub.get(); }
    const vehicle_local_position_s & get_local_position() const { return _rel_pos_sub.get(); }
    const home_position_s & get_home_position() const { return _home_sub.get(); }
    const precnav_command_s & get_command() const { return _pcmd_sub.get(); }
    const optical_recognition_result_s & get_recognition_result() const { return _rec_sub.get(); }
    bool is_vehicle_status_updated() const { return _status_updated; }
    bool is_global_position_updated() const { return _pos_updated; }
    bool is_local_position_updated() const { return _rel_pos_updated; }
    bool is_home_position_updated() const { return _home_updated; }
    bool is_command_updated() const { return _pcmd_updated; }
    bool is_recognition_result_updated() const { return _rec_updated; }
    const vehicle_status & get_status() const { return *_status; }
    cam_control & get_camera_controller() { return *_cam; }
    optical_rec & get_optical_recognizer() { return *_opt; }
    const map_projection_reference_s & get_current_reference() const { return _ref; }
    const map_projection_reference_s & get_home_reference() const { return _home_ref; }
    program * get_program() const { return _program; }
    navigator * get_navigator() const { return _navigator; }
    target_repository & get_target_repository() const { return *_target_repository; }
public:
    bool claim_ack(ack_t ack);
private:
    void update_projection();
    void update_params();
    void process_command();
    bool has_ack(ack_t ack) const;
    void reset_ack();
private:
    void ulet_init();
    void ulet_dispose();
    void ulet_disarm();
    static size_t ulet_get_total_programs();
    static const char * ulet_get_program_name(size_t idx);
    static size_t ulet_get_program_idx(const char * name);
    bool ulet_set_program(size_t idx);
private:
    uORB::Subscription < vehicle_status_s > _status_sub;
    uORB::Subscription < vehicle_global_position_s > _pos_sub;
    uORB::Subscription < vehicle_local_position_s > _rel_pos_sub;
    uORB::Subscription < home_position_s > _home_sub;
    uORB::Subscription < parameter_update_s > _param_sub;
    uORB::Subscription < precnav_command_s > _pcmd_sub;
    uORB::Subscription < optical_recognition_result_s > _rec_sub;
private:
    bool _status_updated;
    bool _pos_updated;
    bool _rel_pos_updated;
    bool _home_updated;
    bool _param_updated;
    bool _pcmd_updated;
    bool _rec_updated;
private:
    vehicle_status * _status;
    cam_control * _cam;
    optical_rec * _opt;
    navigator * _navigator;
    program * _program;
    target_repository * _target_repository;
    bool _force_ack_all;
    ack_t _cur_ack;
    map_projection_reference_s _ref;
    map_projection_reference_s _home_ref;
private:
    DEFINE_PARAMETERS(
        (ParamInt<px4::params::PN_UPDATE_RATE>) _update_rate,
        (ParamFloat<px4::params::PN_TGT_ACC_R>) _target_acceptance_radius,
        (ParamFloat<px4::params::PN_TGT_SAFE_H>) _target_safe_bottom_distance,
        (ParamFloat<px4::params::PN_TGT_DIM>) _target_dimension,
        (ParamInt<px4::params::PN_ACK_ALL>) _ack_all,
        (ParamInt<px4::params::PN_ACK_TAKEOFF>) _ack_takeoff,
        (ParamInt<px4::params::PN_ACK_LAND>) _ack_land,
        (ParamInt<px4::params::PN_ACK_JOB>) _ack_job
    )
public:
    precnav()
        : ModuleParams(nullptr)
        , _status_sub(ORB_ID(vehicle_status))
        , _pos_sub(ORB_ID(vehicle_global_position))
        , _rel_pos_sub(ORB_ID(vehicle_local_position))
        , _home_sub(ORB_ID(home_position))
        , _param_sub(ORB_ID(parameter_update))
        , _pcmd_sub(ORB_ID(precnav_command))
        , _rec_sub(ORB_ID(optical_recognition_result))
        , _navigator(nullptr)
        , _program(nullptr)
        , _force_ack_all(false)
        , _cur_ack(_ACK_MAX)
    {
        _status = new vehicle_status(this);
        _cam = new cam_control(this);
        _opt = new optical_rec(this);
        _target_repository = new target_repository(this);
        ulet_init();
    }
    virtual ~precnav()
    {
        ulet_dispose();
        delete _cam;
        delete _opt;
        delete _status;
        delete _target_repository;
    }
};
