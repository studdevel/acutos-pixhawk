/****************************************************************************
 *
 *   Copyright (c) 2012-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#pragma once

/**
 * @file programs.hpp
 * 
 * Test program definitions.
 *
 * @author Vasilevsky Alexander <a.kalaider@yandex.ru>
 */

#include <precnav/program.hpp>
#include <precnav/nav_modes.hpp>

class ulet_job_base : public job
{
public:
    ulet_job_base(precnav * core)
        : job(core)
    {
    }
    virtual ~ulet_job_base() {}
};

class takeoff_job : public ulet_job_base
{
public:
    void describe(size_t lvl) const override { PNP_PRINT(lvl, "job: takeoff"); }
public:
    bool check_conditions() override;
protected:
    void on_activate() override;
    void on_active() override;
public:
    takeoff_job(precnav * core)
        : ulet_job_base(core)
    {
    }
    virtual ~takeoff_job() {}
};

class find_land_job : public ulet_job_base
{
public:
    void describe(size_t lvl) const override { PNP_PRINT(lvl, "job: find_land"); }
public:
    bool check_conditions() override;
protected:
    void on_activate() override;
    void on_active() override;
public:
    find_land_job(precnav * core)
        : ulet_job_base(core)
    {
    }
    virtual ~find_land_job() {}
};

class land_job : public ulet_job_base
{
public:
    void describe(size_t lvl) const override { PNP_PRINT(lvl, "job: land"); }
public:
    bool check_conditions() override;
protected:
    void on_activate() override;
    void on_active() override;
public:
    land_job(precnav * core)
        : ulet_job_base(core)
    {
    }
    virtual ~land_job() {}
};

class kill_ball_job : public ulet_job_base
{
public:
    void describe(size_t lvl) const override { PNP_PRINT(lvl, "job: kill_ball #%d", (int)_idx); }
public:
    bool check_conditions() override;
    float get_quality() const { return _quality; }
protected:
    void on_activate() override;
    void on_active() override;
private:
    size_t _idx;
    float _quality;
public:
    kill_ball_job(precnav * core, size_t idx)
        : ulet_job_base(core)
        , _idx(idx)
        , _quality(0)
    {
    }
    virtual ~kill_ball_job() {}
};

class kill_ball_program : public program
{
public:
    void describe(size_t lvl) const override
    {
        PNP_PRINT(lvl, "program: kill_ball, %d balls", (int)_count - 3);
        if (_active != nullptr) _active->describe(lvl + 1);
    }
public:
    kill_ball_program(precnav * core, size_t n_balls)
        : program(core)
    {
        _count = n_balls + 3;
        _jobs = new job * [_count];
        _jobs[0] = new takeoff_job(core);
        _jobs[1] = new find_land_job(core);
        for (size_t i = 0; i < n_balls; ++i)
        {
            _jobs[i + 2] = new kill_ball_job(core, i);
        }
        _jobs[_count - 1] = new land_job(core);
    }
    virtual ~kill_ball_program()
    {
        for (size_t i = 0; i < _count; ++i) delete _jobs[i];
        delete[] _jobs;
    }
public:
    virtual bool is_completed() const override { return _jobs[_count - 1]->is_finished(); }
    bool choose_next_job() override;
};
