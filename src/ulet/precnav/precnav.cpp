/****************************************************************************
 *
 *   Copyright (c) 2012-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/**
 * @file precnav.c
 * 
 * Optical recognition-based precise navigation.
 *
 * @author Vasilevsky Alexander <a.kalaider@yandex.ru>
 */

#include <precnav/precnav.hpp>

extern "C" __EXPORT int precnav_main(int argc, char *argv[]);

// precnav_fwd.hpp declarations
namespace detail
{
    ModuleParams * as_module_params(precnav * ptr)
    {
        return static_cast < ModuleParams * > (ptr);
    }
}

void precnav::update_params()
{
    updateParams();
}

int precnav::print_usage(const char *reason)
{
	if (reason) {
		PX4_WARN("%s\n", reason);
	}

	PRINT_MODULE_DESCRIPTION(
		R"DESCR_STR(
### Description
precnav control utility.

### Examples
$ precnav start
$ precnav set ack_all 1
$ precnav set cam front
$ precnav set program min
$ precnav ack takeoff
)DESCR_STR");

	PRINT_MODULE_USAGE_NAME("precnav", "utility");
	PRINT_MODULE_USAGE_COMMAND("start");
	PRINT_MODULE_USAGE_COMMAND_DESCR("set", "set value of some variable");
	PRINT_MODULE_USAGE_DEFAULT_COMMANDS();

	return 0;
}

int precnav::custom_command(int argc, char *argv[])
{
    if (strcmp("ack", argv[0]) == 0)
    {
        return ack_command(argc, argv);
    }
    else if (strcmp("set", argv[0]) == 0)
    {
        return set_command(argc, argv);
    }

    return print_usage("unknown command");
}

int precnav::ack_command(int argc, char *argv[])
{
    uint8_t ack = precnav_command_s::ACK_ANY;

    if (argc > 1)
    {
        if (strcmp("any", argv[1]) == 0)
        {
            // do nothing
        }
        else if (strcmp("takeoff", argv[1]) == 0)
        {
            ack = precnav_command_s::ACK_TAKEOFF;
        }
        else if (strcmp("job", argv[1]) == 0)
        {
            ack = precnav_command_s::ACK_JOB;
        }
        else if (strcmp("land", argv[1]) == 0)
        {
            ack = precnav_command_s::ACK_LAND;
        }
        else
        {
            PX4_ERR("unknown ack type");
            return 1;
        }
    }

    uORB::Publication < precnav_command_s > pub(ORB_ID(precnav_command));
    pub.get().command = ack;
    pub.get().command_type = precnav_command_s::TYPE_ACK;
    pub.get().timestamp = hrt_absolute_time();
    pub.update();

    return 0;
}

int precnav::set_command(int argc, char *argv[])
{
    uint8_t cmd;
    uint8_t param;

    if (argc > 2)
    {
        if (strcmp("ack_all", argv[1]) == 0)
        {
            cmd = precnav_command_s::CMD_SET_ACK_ALL;
            param = (strcmp("1", argv[2]) == 0) ? 1 : 0;
        }
        else if (strcmp("cam", argv[1]) == 0)
        {
            cmd = precnav_command_s::CMD_SET_CAM_MODE;
            if (strcmp("front", argv[2]) == 0)
                param = precnav_command_s::CAM_MODE_FRONT;
            else if (strcmp("down", argv[2]) == 0)
                param = precnav_command_s::CAM_MODE_DOWN;
            else if (strcmp("off", argv[2]) == 0)
                param = precnav_command_s::CAM_MODE_OFF;
            else
            {
                PX4_ERR("invalid camera mode, should be: front|down|off");
                return 1;
            }
        }
        else if (strcmp("stream", argv[1]) == 0)
        {
            cmd = precnav_command_s::CMD_SET_STREAMING_MODE;
            if (strcmp("on", argv[2]) == 0)
                param = precnav_command_s::STREAMING_MODE_ON;
            else if (strcmp("off", argv[2]) == 0)
                param = precnav_command_s::STREAMING_MODE_OFF;
            else
            {
                PX4_ERR("invalid streaming mode, should be: on|off");
                return 1;
            }
        }
        else if (strcmp("program", argv[1]) == 0)
        {
            cmd = precnav_command_s::CMD_SET_PROGRAM;
            size_t idx = ulet_get_program_idx(argv[2]);
            if (idx == (size_t)-1)
            {
                PX4_ERR("unknown command, must be one of");
                for (size_t i = 0; i < ulet_get_total_programs(); ++i)
                {
                    PX4_ERR("  %s", ulet_get_program_name(i));
                }
                return 1;
            }
            param = idx;
        }
        else
        {
            PX4_ERR("unknown command");
            return 1;
        }
    }
    else
    {
        PX4_ERR("invalid command");
        return 1;
    }

    uORB::Publication < precnav_command_s > pub(ORB_ID(precnav_command));
    pub.get().command = cmd;
    pub.get().param0 = param;
    pub.get().command_type = precnav_command_s::TYPE_CMD;
    pub.get().timestamp = hrt_absolute_time();
    pub.update();

    return 0;
}

int precnav::print_status()
{
	PNP_PRINT(0, "Running");
    PNP_PRINT(1, "force_ack_all: %d", _force_ack_all ? 1 : 0);

    _cam->describe(1);
    _opt->describe(1);
    _target_repository->describe(1);

    if (_program != nullptr) _program->describe(1);
    if (_navigator != nullptr) _navigator->describe(1);

	return 0;
}

int precnav::task_spawn(int argc, char *argv[])
{
    ASSERT(_task_id == -1);

    /* start the task */
    _task_id = px4_task_spawn_cmd("precnav",
                    SCHED_DEFAULT,
                    SCHED_PRIORITY_DEFAULT + 15,
                    1500,
                    (px4_main_t)&run_trampoline,
                    (char *const *)argv);

    if (_task_id < 0) {
        PX4_WARN("task start failed");
        return -errno;
    }

    return OK;
}

void precnav::run()
{
    update_params();

    _cam->switch_off();

    orb_set_interval(_rec_sub.getHandle(), _update_rate.get());

    px4_pollfd_struct_t fds[1];
    fds[0].fd = _rec_sub.getHandle();
    fds[0].events = POLLIN;

    while (!should_exit())
    {
        // ignore result of the operation since we
        // need to perform some housekeeping, respond to
        // commands, etc.
        px4_poll(fds, 1, _update_rate.get());

        // update topic data
        _status_updated = _status_sub.update();
        _rec_updated = _rec_sub.update();
        _pos_updated = _pos_sub.update();
        _rel_pos_updated = _rel_pos_sub.update();
        _home_updated = _home_sub.update();
        _pcmd_updated = _pcmd_sub.update();
        _param_updated = _param_sub.update();

        // reset current ack if any -- acks only valid during
        // one command processing cycle
        reset_ack();

        if (_param_updated) update_params();
        if (_pcmd_updated) process_command();

        update_projection();

        _status->run(true);

        bool armed = this->get_status().is_armed();

        _cam->run(armed);

        if (armed && (!_status->valid())) continue;

        _target_repository->run(armed);
        _opt->run(armed);

        if (_program != nullptr) _program->run(armed);
        if (_navigator != nullptr) _navigator->run(armed);

        // clear active program on disarm
        if (!armed) ulet_disarm();
    }
}

void precnav::process_command()
{
    if (_pcmd_sub.get().command_type == precnav_command_s::TYPE_CMD)
    {
        switch (_pcmd_sub.get().command)
        {
            case precnav_command_s::CMD_SET_ACK_ALL:
            {
                _force_ack_all = (_pcmd_sub.get().param0 == 1);
                break;
            }
            case precnav_command_s::CMD_SET_PROGRAM:
            {
                ulet_set_program((size_t)_pcmd_sub.get().param0);
                break;
            }
        }
    }
    else if (_pcmd_sub.get().command_type == precnav_command_s::TYPE_ACK)
    {
        // acks not commands, we should not expose their updates
        _pcmd_updated = false;
        // no validation here, should be in `ack` command
        _cur_ack = (ack_t)_pcmd_sub.get().param0;
    }
}

bool precnav::has_ack(ack_t ack) const
{
    // we always have such an ack
    if (ack == _ACK_MAX) return true;

    bool permitted = (ack == _cur_ack) ||
        _cur_ack == ACK_ANY;

    // if we have ack regardless of whether it was really needed
    if (permitted) return true;

    bool needs_ack = _force_ack_all || (_ack_all.get() == 1);

    // TODO: enum
    switch (ack)
    {
        case ACK_TAKEOFF:
            needs_ack |= _ack_takeoff.get(); break;
        case ACK_LAND:
            needs_ack |= _ack_land.get(); break;
        case ACK_JOB:
            needs_ack |= _ack_job.get(); break;
        case ACK_ANY:
        default:
            break;
    }

    // if we don't have ack but non-acked actions are permitted
    return !needs_ack;
}

void precnav::reset_ack()
{
    _cur_ack = _ACK_MAX;
}

bool precnav::claim_ack(ack_t ack)
{
    if (has_ack(ack))
    {
        reset_ack();
        return true;
    }
    return false;
}

void precnav::update_projection()
{
    if (_pos_updated || !map_projection_initialized(&_ref))
    {
        map_projection_init(&_ref, _pos_sub.get().lat, _pos_sub.get().lon);
    }
    if (_home_updated || !map_projection_initialized(&_home_ref))
    {
        map_projection_init(&_home_ref, _rel_pos_sub.get().ref_lat, _rel_pos_sub.get().ref_lon);
    }
}

precnav * precnav::instantiate(int argc, char *argv[])
{
    bool error_flag = false;

    int myoptind = 1;
	int ch;
    const char * myoptarg = nullptr;

    while ((ch = px4_getopt(argc, argv, "", &myoptind, &myoptarg)) != EOF)
    {
		switch (ch) {
		case '?':
			error_flag = true;
			break;
		default:
			PX4_WARN("unrecognized flag");
			error_flag = true;
			break;
		}
	}

    if (error_flag) return nullptr;
    
    PX4_INFO("spawning");

    precnav * instance = new precnav();

	if (instance == nullptr) {
		PX4_ERR("alloc failed");
	}

    return instance;
}

int precnav_main(int argc, char *argv[])
{
    return precnav::main(argc, argv);
}
