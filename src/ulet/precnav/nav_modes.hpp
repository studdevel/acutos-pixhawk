/****************************************************************************
 *
 *   Copyright (c) 2012-2016 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#pragma once

/**
 * @file nav_modes.hpp
 * 
 * Navigation modes.
 *
 * @author Vasilevsky Alexander <a.kalaider@yandex.ru>
 */

#include <precnav/navigator.hpp>

#include <uORB/uORB.h>
#include <uORB/Subscription.hpp>
#include <uORB/Publication.hpp>
#include <uORB/topics/vehicle_command.h>
#include <uORB/topics/vehicle_command_ack.h>
#include <uORB/topics/mission_result.h>
#include <uORB/topics/position_setpoint_triplet.h>
#include <uORB/topics/landing_target_pose.h>

enum nav_mode_t
{
    // vehicle is landed or disarmed
    NAV_MODE_LANDED,
    // we need to takeoff in order to start any mission
    NAV_MODE_TAKEOFF,
    // no job provided or current job paused
    NAV_MODE_IDLE,
    // job provided and is in progress
    NAV_MODE_KILL_BALL,
    // job is done, find landing position and land
    NAV_MODE_PRECLAND,
    // continue with the mission supplied
    NAV_MODE_MISSION,
    // number of elements in enum
    _NAV_MODE_MAX
};

inline static const char * nav_mode_t_name(nav_mode_t mode)
{
    switch (mode)
    {
        case NAV_MODE_LANDED: return "landed";
        case NAV_MODE_IDLE: return "idle";
        case NAV_MODE_TAKEOFF: return "takeoff";
        case NAV_MODE_KILL_BALL: return "kill ball";
        case NAV_MODE_PRECLAND: return "precland";
        case NAV_MODE_MISSION: return "mission";
        default: return "unknown";
    }
}

class ulet_navigator;

class nav_mode_base : public nav_mode
{
public:
    void describe(size_t lvl) const override { PNP_PRINT(lvl, "nav_mode: %s", self_name()); }
protected:
    void on_active() override;
protected:
    const char * self_name() const { return nav_mode_t_name(_this_mode); }
    void fail_fast(nav_mode_t fallback_to);
    void report_success(nav_mode_t switch_to);
    bool ensure_armed();
protected:
    const nav_mode_t _this_mode;
    uORB::Subscription < mission_result_s > _mission_sub;
    uORB::Subscription < vehicle_command_ack_s > _ack_sub;
    uORB::Subscription < position_setpoint_triplet_s > _sp_sub;
    uORB::Publication < vehicle_command_s > _cmd_pub;
    bool _mission_updated;
    bool _ack_updated;
    bool _sp_updated;
    bool _ld_updated;
    bool _got_ack;
    bool _got_mission_ack;
    size_t _ack_fails;
public:
    nav_mode_base(precnav * core, nav_mode_t this_mode)
        : nav_mode(core)
        , _this_mode(this_mode)
        , _mission_sub(ORB_ID(mission_result))
        , _ack_sub(ORB_ID(vehicle_command_ack))
        , _sp_sub(ORB_ID(position_setpoint_triplet))
        , _cmd_pub(ORB_ID(vehicle_command))
        , _got_ack(false)
        , _got_mission_ack(false)
        , _ack_fails(0)
    {
    }
    virtual ~nav_mode_base()
    {
    }
};

class nav_mode_takeoff : public nav_mode_base
{
protected:
    void on_activate() override;
    void on_active() override;
public:
    nav_mode_takeoff(precnav * core)
        : nav_mode_base(core, NAV_MODE_TAKEOFF)
    {
    }
    virtual ~nav_mode_takeoff()
    {
    }
};

class nav_mode_mission : public nav_mode_base
{
protected:
    void on_activate() override;
    void on_active() override;
    void on_deactivate() override;
public:
    nav_mode_mission(precnav * core)
        : nav_mode_base(core, NAV_MODE_MISSION)
    {
    }
    virtual ~nav_mode_mission()
    {
    }
};

class nav_mode_precland : public nav_mode_base
{
protected:
    void on_activate() override;
    void on_active() override;
private:
    uORB::Publication < landing_target_pose_s > _pose_pub;
private:
    DEFINE_PARAMETERS(
        (ParamInt<px4::params::PN_PREFER_RNG>) _prefer_rng,
        (ParamFloat<px4::params::PN_TGT_SAFE_H>) _safe_dist
    )
public:
    nav_mode_precland(precnav * core)
        : nav_mode_base(core, NAV_MODE_PRECLAND)
        , _pose_pub(ORB_ID(landing_target_pose))
    {
    }
    virtual ~nav_mode_precland()
    {
    }
};

class nav_mode_kill_ball : public nav_mode_base
{
protected:
    void on_activate() override;
    void on_active() override;
public:
    nav_mode_kill_ball(precnav * core)
        : nav_mode_base(core, NAV_MODE_KILL_BALL)
    {
    }
    virtual ~nav_mode_kill_ball()
    {
    }
};

class ulet_navigator : public navigator
{
protected:
    void describe(size_t lvl) const override
    {
        if (_active == nullptr) PNP_PRINT(lvl, "nav_mode: %s", nav_mode_t_name(_nav_mode));
        else _active->describe(lvl);
    }
private:
    nav_mode_t _nav_mode;
public:
    void set_nav_mode(nav_mode_t mode)
    {
        PX4_INFO("navigator: state transition from [%s] to [%s]",
            nav_mode_t_name(_nav_mode), nav_mode_t_name(mode));
        _nav_mode = mode;
        _active = _modes[(size_t)mode];
    }
    nav_mode_t get_nav_mode() const { return _nav_mode; }
    nav_mode * get_nav_mode(nav_mode_t mode) const { return _modes[(size_t)mode]; }
public:
    ulet_navigator(precnav * core)
        : navigator(core)
        , _nav_mode(NAV_MODE_IDLE)
    {
        _count = (size_t)_NAV_MODE_MAX;
        _modes = new nav_mode * [(size_t)_NAV_MODE_MAX];
        _modes[(size_t)NAV_MODE_IDLE] = nullptr;
        _modes[(size_t)NAV_MODE_LANDED] = nullptr;
        _modes[(size_t)NAV_MODE_TAKEOFF] = new nav_mode_takeoff(core);
        _modes[(size_t)NAV_MODE_PRECLAND] = new nav_mode_precland(core);
        _modes[(size_t)NAV_MODE_KILL_BALL] = new nav_mode_kill_ball(core);
        _modes[(size_t)NAV_MODE_MISSION] = new nav_mode_mission(core);
    }
    virtual ~ulet_navigator()
    {
        for (size_t i = 0; i < _count; ++i) delete _modes[i];
        delete[] _modes;
    }
};
