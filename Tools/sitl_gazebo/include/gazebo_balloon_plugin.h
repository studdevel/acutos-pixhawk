/*
 * Copyright 2019 Alexander Vasilevsky, ULET Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
/*
 * Desc: Balloon Plugin
 * Author: Alexander Vasilevsky <a.kalaider@yandex.ru>
 */

#ifndef _GAZEBO_BALLOON_PLUGIN_HH_
#define _GAZEBO_BALLOON_PLUGIN_HH_

#include <string>

#include "gazebo/gazebo.hh"
#include "gazebo/sensors/sensors.hh"

namespace gazebo
{
  /// \brief A Balloon Plugin
  class GAZEBO_VISIBLE BalloonPlugin : public SensorPlugin
  {
    /// \brief Constructor
    public: BalloonPlugin();

    /// \brief Destructor
    public: virtual ~BalloonPlugin();

    /// \brief Update callback
    public: virtual void OnUpdate();

    /// \brief Load the plugin
    /// \param take in SDF root element
    public: void Load(sensors::SensorPtr _parent, sdf::ElementPtr _sdf);

    /// \brief The parent sensor
    private:
      sensors::ContactSensorPtr parentSensor;
      physics::ModelPtr parentModel;
      physics::WorldPtr world;

    private:
      event::ConnectionPtr updateConnection;
  };
}
#endif
