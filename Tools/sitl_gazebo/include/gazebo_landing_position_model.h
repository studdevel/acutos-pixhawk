/*
 * Copyright 2019 Alexander Vasilevsky, ULET Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



#include <stdio.h>

#include <vector>

#include <boost/bind.hpp>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <gazebo/common/Plugin.hh>

#include <ignition/math.hh>

namespace gazebo {

class LandingPositionModel : public ModelPlugin {
 public:
  LandingPositionModel()
      : ModelPlugin()
      , prev_sim_time_(0)
      , sampling_time_(0.01)
      , point_idx_(0)
      , time_in_point_(0)
      , time_in_point_target_(5)  {
  }

  virtual ~LandingPositionModel();

 protected:
  virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf);
  virtual void OnUpdate(const common::UpdateInfo & /*_info*/);

 private:
  double prev_sim_time_;
  double sampling_time_;
  physics::ModelPtr model_;
  event::ConnectionPtr updateConnection_;
 private:
  std::vector<ignition::math::Vector2d> points_;
  size_t point_idx_;
  double time_in_point_;
  double time_in_point_target_;
};
}
