/*
 * Copyright 2019 Alexander Vasilevsky, ULET Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NanoPi companion computer model simulation.
 *
 * Note: implements relaxed ack scheme -- the plugin
 *       does not wait for any acks from underlying camera sensors.
 */

#include <stdio.h>

#include <queue>

#include <boost/bind.hpp>
#include <gazebo/gazebo.hh>
#include <gazebo/common/common.hh>
#include <gazebo/common/Plugin.hh>
#include <gazebo/sensors/CameraSensor.hh>
#include <gazebo/transport/transport.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/msgs/msgs.hh>

#include <sdf/sdf.hh>
#include <common.h>

#include <MAVLinkCommand.pb.h>
#include <MAVLinkCommandAck.pb.h>
#include <CameraCommand.pb.h>
#include <OpticalRec.pb.h>

namespace gazebo {

typedef const boost::shared_ptr<const mavlink_msgs::msgs::MAVLinkCommand> MavCmdPtr;
typedef const boost::shared_ptr<const mavlink_msgs::msgs::MAVLinkCommandAck> MavAckPtr;
typedef const boost::shared_ptr<const sensor_msgs::msgs::OpticalRec> OptRecPtr;

static const std::string kDefaultMavCmdTopic = "/mavlink/command";
static const std::string kDefaultMavAckTopic = "/mavlink/ack";
static const std::string kDefaultDownCamCmdTopic = "/optrecDown/link/command";
static const std::string kDefaultForwardCamCmdTopic = "/optrecForward/link/command";
static const std::string kDefaultDownCamRecTopic = "/optrecDown/link/opticalRec";
static const std::string kDefaultForwardCamRecTopic = "/optrecForward/link/opticalRec";
static const std::string kDefaultOptrecResultTopic = "/optrecResult";

class NanopiModel : public ModelPlugin {
 private:
  enum CameraType { CAM_DOWN, CAM_FORWARD };
  enum State { ST_ON, ST_OFF, ST_UNK };
 public:
  NanopiModel()
      : ModelPlugin()
      , current_cam_(CAM_DOWN)
      , current_state_(ST_UNK)
      , mav_ack_pub_topic_(kDefaultMavAckTopic)
      , mav_cmd_sub_topic_(kDefaultMavCmdTopic)
      , down_cam_cmd_pub_topic_(kDefaultDownCamCmdTopic)
      , forward_cam_cmd_pub_topic_(kDefaultForwardCamCmdTopic)
      , down_cam_rec_sub_topic_(kDefaultDownCamRecTopic)
      , forward_cam_rec_sub_topic_(kDefaultForwardCamRecTopic)
      , rec_pub_topic_(kDefaultOptrecResultTopic) {
  }

  virtual ~NanopiModel();

 protected:
  virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf);
  virtual void OnUpdate(const common::UpdateInfo & /*_info*/);
  void MavCmdCallback(MavCmdPtr& mav_cmd);
  void OptRecCallback(OptRecPtr& rec_msg);

 private:
  physics::ModelPtr model_;
  size_t cam_down_id_;
  size_t cam_forward_id_;
  event::ConnectionPtr updateConnection_;

  CameraType current_cam_;
  State current_state_;

  std::deque<mavlink_msgs::msgs::MAVLinkCommand> msg_cmd_queue_;

  std::string mav_ack_pub_topic_;
  std::string mav_cmd_sub_topic_;
  std::string down_cam_cmd_pub_topic_;
  std::string forward_cam_cmd_pub_topic_;
  std::string down_cam_rec_sub_topic_;
  std::string forward_cam_rec_sub_topic_;
  std::string rec_pub_topic_;

  transport::NodePtr node_handle_;
  transport::SubscriberPtr mav_cmd_sub_;
  transport::PublisherPtr  mav_ack_pub_;
  transport::PublisherPtr  down_cam_cmd_pub_;
  transport::PublisherPtr  forward_cam_cmd_pub_;
  transport::SubscriberPtr down_cam_rec_sub_;
  transport::SubscriberPtr forward_cam_rec_sub_;
  transport::PublisherPtr  rec_pub_;

  std::string namespace_;
};
}
