#pragma once

#include <iostream>
#include <cv.h>
#include <cmath>
#include <vector>

class OpticalRecOpenCV
{
private:
	static const size_t kMaxBalloonObjects = 3;
	static const size_t kMaxLandingPosObjects = 1;
	static const size_t kMaxBalloonBorderPoints = 6;
	static const size_t kMaxLandingPosBorderPoints = 6;
public:
	enum ObjectType { LANDING_POS, BALLOON };
	struct Object
	{
		cv::Point2f center;
		std::vector < cv::Point2f > border;
	};

private:
	//params which can be set
	int image_width;
	int image_height;
	float focal_length_x; //[pixel]
	float focal_length_y; //[pixel]

public:

	inline void setImageWidth(int img_width) { image_width = img_width; };
	inline void setImageHeight(int img_height) { image_height = img_height; };
	inline void setFocalLengthX(float f_lengh) { focal_length_x = f_lengh; };
	inline void setFocalLengthY(float f_lengh) { focal_length_y = f_lengh; };

	inline int getImageWidth() { return image_width; };
	inline int getImageHeight() { return image_height; };
	inline int getFocalLengthX() { return focal_length_x; };
	inline int getFocalLengthy() { return focal_length_y; };

	OpticalRecOpenCV(float f_length_x, float f_length_y,
			  int img_width, int img_height);
	~OpticalRecOpenCV();

	bool findObject(uint8_t *img_current, ObjectType type, std::vector < Object > & objects);
};
