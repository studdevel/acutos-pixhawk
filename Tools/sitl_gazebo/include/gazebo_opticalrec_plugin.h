/*
 * Copyright (C) 2012-2015 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#ifndef _GAZEBO_OPTICAL_REC_PLUGIN_HH_
#define _GAZEBO_OPTICAL_REC_PLUGIN_HH_

#include <string>

#include "gazebo/common/Plugin.hh"
#include "gazebo/sensors/CameraSensor.hh"
#include "gazebo/gazebo.hh"
#include "gazebo/common/common.hh"
#include "gazebo/rendering/Camera.hh"
#include "gazebo/util/system.hh"
#include "gazebo/transport/transport.hh"
#include "gazebo/msgs/msgs.hh"
#include "gazebo/physics/physics.hh"

#include <sdf/sdf.hh>
#include <common.h>

#include "OpticalRec.pb.h"
#include "CameraCommand.pb.h"

#include <opencv2/opencv.hpp>
#include <iostream>
#include <ignition/math.hh>

#include "optical_rec.h"

using namespace cv;
using namespace std;

typedef const boost::shared_ptr<const cam_msgs::msgs::CameraCommand> CamCmdPtr;

namespace gazebo
{
  class GAZEBO_VISIBLE OpticalRecPlugin : public SensorPlugin
  {
    private:
      enum CameraType { CAM_DOWN, CAM_FORWARD };
    public:
      OpticalRecPlugin();
      virtual ~OpticalRecPlugin();
      virtual void Load(sensors::SensorPtr _sensor, sdf::ElementPtr _sdf);
      virtual void OnNewFrame(const unsigned char *_image,
                              unsigned int _width, unsigned int _height,
                              unsigned int _depth, const std::string &_format);

    private:
      void CamCmdCallback(CamCmdPtr& cam_cmd);

    protected:
      unsigned int width, height, depth;
      std::string format;
      sensors::CameraSensorPtr parentSensor;
      rendering::CameraPtr camera;
      physics::WorldPtr world;

    private:
      event::ConnectionPtr newFrameConnection;
      transport::PublisherPtr opticalRec_pub_;
      transport::SubscriberPtr cam_cmd_sub_;
      transport::NodePtr node_handle_;
      sensor_msgs::msgs::OpticalRec opticalRec_message;
      std::string namespace_;
      OpticalRecOpenCV *optical_rec_;

      CameraType cam_type_;

      float hfov_;
      float vfov_;
      float focal_length_x_;
      float focal_length_y_;
  };
}
#endif
