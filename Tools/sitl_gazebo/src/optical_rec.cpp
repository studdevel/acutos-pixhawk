#include "optical_rec.h"

#include <highgui.h>				// debug: cv::imshow("", img)
#include "gazebo/common/common.hh"	// debug: gzerr << "\n"

#include <nanopi/objdetect.hpp>
#include <nanopi/objparams.hpp>

OpticalRecOpenCV::OpticalRecOpenCV(float f_length_x, float f_length_y, int img_width, int img_height)
{
	setImageWidth(img_width);
	setImageHeight(img_height);
	setFocalLengthX(f_length_x);
	setFocalLengthY(f_length_y);
}

OpticalRecOpenCV::~OpticalRecOpenCV(void)
{

}

bool OpticalRecOpenCV::findObject(uint8_t *img_current, ObjectType type, std::vector < Object > & objects)
{
	cv::Mat frame = cv::Mat(image_height, image_width, CV_8UC3);
	frame.data = (uchar *)img_current;

	nanopi::objdetect detector(
		(type == BALLOON)
		? nanopi::objparams::make_config < nanopi::objparams::KO_BALLOON_SITL > ()
		: nanopi::objparams::make_config < nanopi::objparams::KO_LANDING_POS_SITL > ()
	);

	nanopi::objdetect::debug_t debug;
	std::vector < nanopi::objdetect::obj_t > objs;

	detector.detect(frame, objs, &debug);

	if (!debug.debug.empty())
	{
		cv::Mat mask, two;
		cv::cvtColor(debug.mask, mask, cv::COLOR_GRAY2RGB);
		cv::hconcat(debug.debug, mask, two);
		cv::resize(two, two, { image_width, image_height / 2 });
		cv::imshow("img", two);
		cv::waitKey(25);
	}

	for (size_t i = 0; i < objs.size(); ++i)
	{
		objects.emplace_back(Object{
			{ objs[i].center.x, objs[i].center.y },
			{ }
		});

		objects.back().border.resize(objs[i].num_of_hull_points);
		for (size_t j = 0; j < objs[i].num_of_hull_points; ++j)
		{
			objects.back().border[j].x = objs[i].hull[j].x;
			objects.back().border[j].y = objs[i].hull[j].y;
		}
	}

	return true;
}
