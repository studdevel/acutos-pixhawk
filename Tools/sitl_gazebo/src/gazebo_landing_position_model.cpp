/*
 * Copyright 2019 Alexander Vasilevsky, ULET Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gazebo_landing_position_model.h"
#include <ignition/math.hh>

namespace gazebo {

LandingPositionModel::~LandingPositionModel() {
  updateConnection_->~Connection();
}

void LandingPositionModel::Load(physics::ModelPtr _model, sdf::ElementPtr _sdf) {
  model_ = _model;

  const double max_dist = 10;
  const double max_point_dist = 5;
  const double min_point_dist = 2;
  const size_t points = 100;

  points_.resize(points);

  ignition::math::Vector2d prev(0, 0);
  for (size_t i = 0; i < points; ++i)
  {
    while (true)
    {
      double len = rand() / (RAND_MAX + 1.), ang = rand() / (RAND_MAX + 1.);
      len = min_point_dist + (max_point_dist - min_point_dist) * len;
      ang = ang * 2 * M_PI;

      points_[i].X(prev.X() + len * cos(ang));
      points_[i].Y(prev.Y() + len * sin(ang));

      if (points_[i].Length() <= max_dist) break;
    }

    prev = points_[i];
  }

  updateConnection_ = event::Events::ConnectWorldUpdateBegin(boost::bind(&LandingPositionModel::OnUpdate, this, _1));
}

// This gets called by the world update start event.
void LandingPositionModel::OnUpdate(const common::UpdateInfo& _info) {
  sampling_time_ = _info.simTime.Double() - prev_sim_time_;
  prev_sim_time_ = _info.simTime.Double();

  auto pose = model_->WorldPose();

  auto target = points_[point_idx_];
  auto cur = ignition::math::Vector2d(pose.Pos().X(), pose.Pos().Y());

  if ((cur - target).SquaredLength() < 1e-2)
  {
    point_idx_ = (point_idx_ + 1) % points_.size();
    time_in_point_ = 0;

    const double max_time_in_point = 20;
    const double min_time_in_point = 8;
    time_in_point_target_ = min_time_in_point +
        (max_time_in_point - min_time_in_point) * rand() / (RAND_MAX + 1.);
  }

  if (time_in_point_ < time_in_point_target_)
  {
    time_in_point_ += sampling_time_;
    return;
  }

  auto heading = (target - cur);
  heading.Normalize();

  const double vel_range = 0.3;
  const double vel_mean = 0.35;

  double rn = 0;
  for (size_t i = 0; i < 12; ++i)
    rn += (rand() / (RAND_MAX + 1.) - 0.5);
  rn /= 12;

  double vel = vel_mean + vel_range * rn;

  heading *= vel;

  pose.Pos().X(pose.Pos().X() + heading.X() * sampling_time_);
  pose.Pos().Y(pose.Pos().Y() + heading.Y() * sampling_time_);
  model_->SetWorldPose(pose);
}

GZ_REGISTER_MODEL_PLUGIN(LandingPositionModel);
}
