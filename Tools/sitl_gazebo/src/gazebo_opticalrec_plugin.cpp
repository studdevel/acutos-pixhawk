/*
 * Copyright (C) 2012-2015 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#ifdef _WIN32
  // Ensure that Winsock2.h is included before Windows.h, which can get
  // pulled in by anybody (e.g., Boost).
#include <Winsock2.h>
#endif

#include "gazebo/sensors/DepthCameraSensor.hh"
#include "gazebo_opticalrec_plugin.h"

//#include <highgui.h>
#include <math.h>
#include <string>
#include <iostream>
#include <boost/algorithm/string.hpp>

using namespace cv;
using namespace std;

using namespace gazebo;
GZ_REGISTER_SENSOR_PLUGIN(OpticalRecPlugin)

/////////////////////////////////////////////////
OpticalRecPlugin::OpticalRecPlugin()
: SensorPlugin(), width(0), height(0), depth(0), cam_type_(CAM_DOWN)
{

}

/////////////////////////////////////////////////
OpticalRecPlugin::~OpticalRecPlugin()
{
  this->parentSensor.reset();
  this->camera.reset();
}

/////////////////////////////////////////////////
void OpticalRecPlugin::Load(sensors::SensorPtr _sensor, sdf::ElementPtr _sdf)
{
  if (!_sensor)
    gzerr << "Invalid sensor pointer.\n";

  this->parentSensor = std::dynamic_pointer_cast<sensors::CameraSensor>(_sensor);

  if (!this->parentSensor)
  {
    gzerr << "OpticalRecPlugin requires a CameraSensor.\n";
    if (std::dynamic_pointer_cast<sensors::DepthCameraSensor>(_sensor))
      gzmsg << "It is a depth camera sensor\n";
  }

  if (!this->parentSensor)
  {
    gzerr << "OpticalRecPlugin not attached to a camera sensor\n";
    return;
  }

  this->world = physics::get_world(this->parentSensor->WorldName());

#if GAZEBO_MAJOR_VERSION >= 7
  this->camera = this->parentSensor->Camera();
  this->width = this->camera->ImageWidth();
  this->height = this->camera->ImageHeight();
  this->depth = this->camera->ImageDepth();
  this->format = this->camera->ImageFormat();
  hfov_ = float(this->camera->HFOV().Radian());
  vfov_ = float(this->camera->VFOV().Radian());
  string scopedName = _sensor->ParentName();
#else
  this->camera = this->parentSensor->GetCamera();
  this->width = this->camera->GetImageWidth();
  this->height = this->camera->GetImageHeight();
  this->depth = this->camera->GetImageDepth();
  this->format = this->camera->GetImageFormat();
  hfov_ = float(this->camera->GetHFOV().Radian());
  vfov_ = float(this->camera->GetVFOV().Radian());
  string scopedName = _sensor->GetParentName();
#endif

  focal_length_x_ = (this->width/2)/tan(hfov_/2);
  focal_length_y_ = (this->height/2)/tan(vfov_/2);

  gzmsg << "[gazebo_optical_rec_plugin] Using (fovx,fovy) = (" << hfov_ << ", " << vfov_ << ")\n";

  if (_sdf->HasElement("robotNamespace"))
    namespace_ = _sdf->GetElement("robotNamespace")->Get<std::string>();
  else
    gzwarn << "[gazebo_optical_rec_plugin] Please specify a robotNamespace.\n";

  boost::replace_all(scopedName, "::", "/");

  auto cmp = [] (auto a, auto b) { return tolower(a) == tolower(b); };

  if (boost::ends_with(scopedName, "down/link", cmp)) {
    cam_type_ = CAM_DOWN;
  } else if (boost::ends_with(scopedName, "forward/link", cmp)) {
    cam_type_ = CAM_FORWARD;
  } else {
    gzwarn << "[gazebo_optical_rec_plugin] Cannot determine camera type from model name (" << scopedName << ").\n";
  }

  node_handle_ = transport::NodePtr(new transport::Node());
  node_handle_->Init(namespace_);

  string topicName = "~/" + scopedName + "/opticalRec";
  opticalRec_pub_ = node_handle_->Advertise<sensor_msgs::msgs::OpticalRec>(topicName, 10);

  topicName = "~/" + scopedName + "/command";
  cam_cmd_sub_ = node_handle_->Subscribe(topicName, &OpticalRecPlugin::CamCmdCallback, this);

  this->newFrameConnection = this->camera->ConnectNewImageFrame(
      boost::bind(&OpticalRecPlugin::OnNewFrame, this, _1, this->width, this->height, this->depth, this->format));

  this->parentSensor->SetActive(false);

  // init rec
  optical_rec_ = new OpticalRecOpenCV(focal_length_x_, focal_length_y_, this->width, this->height);
}

/////////////////////////////////////////////////
void OpticalRecPlugin::OnNewFrame(const unsigned char * _image,
                              unsigned int _width,
                              unsigned int _height,
                              unsigned int _depth,
                              const std::string &_format)
{

  //get data depending on gazebo version
  #if GAZEBO_MAJOR_VERSION >= 7
    _image = this->camera->ImageData(0);
  #else
    _image = this->camera->GetImageData(0);
  #endif

  std::vector < OpticalRecOpenCV::Object > objects;

  auto objType = (cam_type_ == CAM_DOWN) ?
    OpticalRecOpenCV::LANDING_POS : OpticalRecOpenCV::BALLOON;
  auto objMsgType = (cam_type_ == CAM_DOWN) ?
    sensor_msgs::msgs::OpticalRec::LANDING : sensor_msgs::msgs::OpticalRec::BALLOON;

  if (optical_rec_->findObject((uchar*)_image, objType, objects)) {
    #if GAZEBO_MAJOR_VERSION >= 9
      common::Time now = world->SimTime();
    #else
      common::Time now = world->GetSimTime();
    #endif

    opticalRec_message.Clear();

    opticalRec_message.set_time_usec(now.Double() * 1e6);
    opticalRec_message.set_type(objMsgType);
    for (size_t i = 0; i < objects.size(); ++i) {
      auto obj = opticalRec_message.add_objects();
      obj->mutable_center()->set_x((double)objects[i].center.x);
      obj->mutable_center()->set_y((double)objects[i].center.y);
      for (size_t j = 0; j < objects[i].border.size(); ++j) {
        auto pt = obj->add_border();
        pt->set_x((double)objects[i].border[j].x);
        pt->set_y((double)objects[i].border[j].y);
      }
    }
    //send message
    opticalRec_pub_->Publish(opticalRec_message);
  }
}

// queue all incoming commands
void OpticalRecPlugin::CamCmdCallback(CamCmdPtr& cam_cmd) {
  switch (cam_cmd->command()) {
    case cam_msgs::msgs::CameraCommand::CAMERA_ON:
      this->parentSensor->SetActive(true);
      gzmsg << "[gazebo_optical_rec_plugin] Camera " << this->parentSensor->ParentName() << " got activation command.\n";
      break;
    case cam_msgs::msgs::CameraCommand::CAMERA_OFF:
      this->parentSensor->SetActive(false);
      gzmsg << "[gazebo_optical_rec_plugin] Camera " << this->parentSensor->ParentName() << " got deactivation command.\n";
      break;
  }
}

/* vim: set et fenc=utf-8 ff=unix sts=0 sw=2 ts=2 : */
