/*
 * Copyright 2019 Alexander Vasilevsky, ULET Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gazebo_nanopi_model.h"

#include <mavlink/v2.0/common/mavlink.h>

namespace gazebo {

NanopiModel::~NanopiModel() {
  updateConnection_->~Connection();
}

void NanopiModel::Load(physics::ModelPtr _model, sdf::ElementPtr _sdf) {
  model_ = _model;

  namespace_.clear();
  if (_sdf->HasElement("robotNamespace")) {
    namespace_ = _sdf->GetElement("robotNamespace")->Get<std::string>();
  } else {
    gzerr << "[nanopi_model] Please specify a robotNamespace.\n";
  }

  getSdfParam<std::string>(_sdf, "mavAckPubTopic", mav_ack_pub_topic_, mav_ack_pub_topic_);
  getSdfParam<std::string>(_sdf, "mavCmdSubTopic", mav_cmd_sub_topic_, mav_cmd_sub_topic_);

  getSdfParam<std::string>(_sdf, "downCamCmdPubTopic", down_cam_cmd_pub_topic_, down_cam_cmd_pub_topic_);
  getSdfParam<std::string>(_sdf, "forwardCamCmdPubTopic", forward_cam_cmd_pub_topic_, forward_cam_cmd_pub_topic_);

  getSdfParam<std::string>(_sdf, "downCamRecSubTopic", down_cam_rec_sub_topic_, down_cam_rec_sub_topic_);
  getSdfParam<std::string>(_sdf, "forwardCamRecSubTopic", forward_cam_rec_sub_topic_, forward_cam_rec_sub_topic_);

  getSdfParam<std::string>(_sdf, "recPubTopic", rec_pub_topic_, rec_pub_topic_);

  node_handle_ = transport::NodePtr(new transport::Node());
  node_handle_->Init(namespace_);

  updateConnection_ = event::Events::ConnectWorldUpdateBegin(boost::bind(&NanopiModel::OnUpdate, this, _1));

  mav_cmd_sub_ = node_handle_->Subscribe("~/" + model_->GetName() + mav_cmd_sub_topic_, &NanopiModel::MavCmdCallback, this);
  mav_ack_pub_ = node_handle_->Advertise<mavlink_msgs::msgs::MAVLinkCommandAck>("~/" + model_->GetName() + mav_ack_pub_topic_, 2);
  down_cam_cmd_pub_ = node_handle_->Advertise<cam_msgs::msgs::CameraCommand>("~/" + model_->GetName() + down_cam_cmd_pub_topic_, 2);
  forward_cam_cmd_pub_ = node_handle_->Advertise<cam_msgs::msgs::CameraCommand>("~/" + model_->GetName() + forward_cam_cmd_pub_topic_, 2);
  down_cam_rec_sub_ = node_handle_->Subscribe("~/" + model_->GetName() + down_cam_rec_sub_topic_, &NanopiModel::OptRecCallback, this);
  forward_cam_rec_sub_ = node_handle_->Subscribe("~/" + model_->GetName() + forward_cam_rec_sub_topic_, &NanopiModel::OptRecCallback, this);
  rec_pub_ = node_handle_->Advertise<sensor_msgs::msgs::OpticalRec>("~/" + model_->GetName() + rec_pub_topic_, 10);
}

// emit secondary commands and acks here
void NanopiModel::OnUpdate(const common::UpdateInfo& _info) {
  cam_msgs::msgs::CameraCommand cam_down_cmd;
  cam_msgs::msgs::CameraCommand cam_forward_cmd;
  mavlink_msgs::msgs::MAVLinkCommandAck ack;

  if (current_state_ == ST_UNK) {
    cam_down_cmd.set_command(cam_msgs::msgs::CameraCommand::CAMERA_OFF);
    cam_forward_cmd.set_command(cam_msgs::msgs::CameraCommand::CAMERA_OFF);
    down_cam_cmd_pub_->Publish(cam_down_cmd);
    forward_cam_cmd_pub_->Publish(cam_forward_cmd);
    current_state_ = ST_OFF;
  }

  while (!msg_cmd_queue_.empty()) {
    mavlink_msgs::msgs::MAVLinkCommand cmd = msg_cmd_queue_.front();
    ack.set_command(cmd.command());
    ack.set_result(MAV_RESULT_ACCEPTED);
    switch (cmd.command())
    {
    // enable or disable all cameras
    case MAV_CMD_VIDEO_START_CAPTURE:
      cam_down_cmd.set_command(current_cam_ == CAM_DOWN ?
        cam_msgs::msgs::CameraCommand::CAMERA_ON :
        cam_msgs::msgs::CameraCommand::CAMERA_OFF);
      cam_forward_cmd.set_command(current_cam_ == CAM_FORWARD ?
        cam_msgs::msgs::CameraCommand::CAMERA_ON :
        cam_msgs::msgs::CameraCommand::CAMERA_OFF);
      down_cam_cmd_pub_->Publish(cam_down_cmd);
      forward_cam_cmd_pub_->Publish(cam_forward_cmd);
      mav_ack_pub_->Publish(ack);
      current_state_ = ST_ON;
      gzmsg << "[nanopi_model] activating " << current_cam_ << " camera" << std::endl;
      break;
    case MAV_CMD_VIDEO_STOP_CAPTURE:
      cam_down_cmd.set_command(cam_msgs::msgs::CameraCommand::CAMERA_OFF);
      cam_forward_cmd.set_command(cam_msgs::msgs::CameraCommand::CAMERA_OFF);
      down_cam_cmd_pub_->Publish(cam_down_cmd);
      forward_cam_cmd_pub_->Publish(cam_forward_cmd);
      mav_ack_pub_->Publish(ack);
      current_state_ = ST_OFF;
      gzmsg << "[nanopi_model] deactivating cameras" << std::endl;
      break;
    // enable or disable all cameras
    case MAV_CMD_SET_CAMERA_MODE:
      if ((int)round(cmd.param1()) == 0) { // down camera
        cam_down_cmd.set_command(current_state_ == ST_ON ?
          cam_msgs::msgs::CameraCommand::CAMERA_ON :
          cam_msgs::msgs::CameraCommand::CAMERA_OFF);
        cam_forward_cmd.set_command(cam_msgs::msgs::CameraCommand::CAMERA_OFF);
        current_cam_ = CAM_DOWN;
      } else { // forward camera
        cam_down_cmd.set_command(cam_msgs::msgs::CameraCommand::CAMERA_OFF);
        cam_forward_cmd.set_command(current_state_ == ST_ON ?
          cam_msgs::msgs::CameraCommand::CAMERA_ON :
          cam_msgs::msgs::CameraCommand::CAMERA_OFF);
        current_cam_ = CAM_FORWARD;
      }
      down_cam_cmd_pub_->Publish(cam_down_cmd);
      forward_cam_cmd_pub_->Publish(cam_forward_cmd);
      mav_ack_pub_->Publish(ack);
      gzmsg << "[nanopi_model] setting current camera to " << current_cam_ << std::endl;
      break;
    // start/stop streaming
    // not simulated, just ack
    case MAV_CMD_VIDEO_START_STREAMING:
    case MAV_CMD_VIDEO_STOP_STREAMING:
      mav_ack_pub_->Publish(ack);
      break;
    }
    msg_cmd_queue_.pop_front();
  }
}

// queue all incoming commands
void NanopiModel::MavCmdCallback(MavCmdPtr& mav_cmd) {
  msg_cmd_queue_.push_back(*mav_cmd);
}

// retarget all incoming messages
void NanopiModel::OptRecCallback(OptRecPtr& rec_msg) {
  rec_pub_->Publish(*rec_msg);
}

GZ_REGISTER_MODEL_PLUGIN(NanopiModel);
}
