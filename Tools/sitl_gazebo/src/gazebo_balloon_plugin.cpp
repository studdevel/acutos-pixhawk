/*
 * Copyright 2019 Alexander Vasilevsky, ULET Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
/*
 * Desc: Balloon plugin
 * Author: Alexander Vasilevsky <a.kalaider@yandex.ru>
 */

#include "gazebo/physics/physics.hh"
#include "gazebo_balloon_plugin.h"

#include <gazebo/common/common.hh>
#include <gazebo/common/Plugin.hh>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/physics/Link.hh>
#include "gazebo/transport/transport.hh"
#include "gazebo/msgs/msgs.hh"

#include <chrono>
#include <cmath>
#include <iostream>
#include <memory>
#include <stdio.h>
#include <boost/algorithm/string.hpp>

using namespace gazebo;
using namespace std;

// Register this plugin with the simulator
GZ_REGISTER_SENSOR_PLUGIN(BalloonPlugin)

/////////////////////////////////////////////////
BalloonPlugin::BalloonPlugin()
{
}

/////////////////////////////////////////////////
BalloonPlugin::~BalloonPlugin()
{
  this->parentSensor.reset();
  this->parentModel.reset();
  this->world.reset();
}

/////////////////////////////////////////////////
void BalloonPlugin::Load(sensors::SensorPtr _parent, sdf::ElementPtr _sdf)
{
  this->parentSensor = std::dynamic_pointer_cast<sensors::ContactSensor>(_parent);

  if (!this->parentSensor)
    gzthrow("BalloonPlugin requires a Contact Sensor as its parent");

  this->world = physics::get_world(this->parentSensor->WorldName());

  this->parentModel = boost::dynamic_pointer_cast<physics::Link>(
    this->world->EntityByName(this->parentSensor->ParentName())
  )->GetModel();

  this->parentSensor->SetActive(false);
  this->updateConnection = this->parentSensor->ConnectUpdated(boost::bind(&BalloonPlugin::OnUpdate, this));
  this->parentSensor->SetActive(true);
}

void BalloonPlugin::OnUpdate()
{
  if (this->parentSensor->GetCollisionContactCount(
    this->parentSensor->GetCollisionName(0)
  ))
  {
    auto pose = this->parentModel->WorldPose();
    pose.Pos().X(-pose.Pos().X());
    pose.Pos().Y(-pose.Pos().Y());
    this->parentModel->SetWorldPose(pose);
  }
}
