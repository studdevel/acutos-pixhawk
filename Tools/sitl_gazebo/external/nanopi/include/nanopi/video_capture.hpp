#pragma once

#include <nanopi/serialization.hpp>

#include <opencv2/opencv.hpp>

#include <string>

namespace nanopi {

/**
 * Abstract configurable video capture.
 *
 * Allows to choose between real camera device
 * or some file on disk. Supports preferred
 * frame size tweaking.
 *
 * Configuration registry stores all supported variants
 * of configuration. One should set it up prior to
 * using this class.
 */
class video_capture
{
public:
    enum camera_id_t { CAM_ID_DOWN, CAM_ID_FORWARD, _CAM_ID_MAX };
public:
    enum dev_src_t { DEV_UNUSED, DEV_SRC_ID, DEV_SRC_FILE };
    struct dev_t
    {
        dev_src_t source;
        int index;
        std::string filename;
        int pref_width;
        int pref_height;
        bool bgr_source;
    };
    struct registry_t
    {
        dev_t cams[_CAM_ID_MAX];
    };
private:
    static registry_t make_default_registry()
    {
        dev_t unused = { DEV_UNUSED };
        unused.pref_height = unused.pref_width = 0;
        unused.bgr_source = true;
        return { unused, unused };
    }
public:
    static registry_t g_registry;
private:
    cv::VideoCapture _cv_capture;
    camera_id_t _mode;
public:
    /**
     * Creates video capture device for the
     * given configuration type. The actual
     * config values are read from the repository.
     */
    video_capture(camera_id_t mode);
public:
    /**
     * Captures the new frame and stores it to `frame`.
     *
     * The returned image is of type CV_8UC3
     * and is represented in BGR color space. `bgr_source`
     * option specifies if the underlying video
     * source is originally BGR- (`true`) or
     * RGB-encoded (`false`). In the last case it is
     * converted to BGR.
     *
     * @return `true` on success
     */
    bool capture(cv::Mat & frame);
public:
    bool is_open() const { return _cv_capture.isOpened(); }
    operator bool() const { return is_open(); }
};

inline void to_json(json_t & j, const video_capture::dev_t & c)
{
    j = json_t::object_t();
    j.push_back({ "dev.idx", nullptr });
    j.push_back({ "dev.file", nullptr });
    switch (c.source)
    {
    case video_capture::DEV_SRC_ID:
        j.push_back({ "dev.idx", c.index }); break;
    case video_capture::DEV_SRC_FILE:
        j.push_back({ "dev.file", c.filename }); break;
    case video_capture::DEV_UNUSED:
    default:
        break;
    }
    j.push_back({ "pref.width", c.pref_width });
    j.push_back({ "pref.height", c.pref_height });
    j.push_back({ "source.bgr", c.bgr_source });
}

inline void from_json(const json_t & j, video_capture::dev_t & c)
{
    auto idx = j.at("dev.idx");
    auto file = j.at("dev.file");

    if (idx.is_null() && file.is_null()) c.source = video_capture::DEV_UNUSED;
    else if (!idx.is_null())
    {
        c.source = video_capture::DEV_SRC_ID;
        idx.get_to(c.index);
    }
    else
    {
        c.source = video_capture::DEV_SRC_FILE;
        file.get_to(c.filename);
    }

    j.at("pref.width").get_to(c.pref_width);
    j.at("pref.height").get_to(c.pref_height);
    j.at("source.bgr").get_to(c.bgr_source);
}

}
