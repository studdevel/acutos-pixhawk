#pragma once

#include <mavlink/v2.0/common/mavlink.h>

/**
 * MAVLink system and component ID
 * (arbitrary values -- not checked anywhere)
 */
#define NANOPI_MAV_SYS_ID 0
#define NANOPI_MAV_COMP_ID MAV_COMP_ID_CAMERA

namespace nanopi {

/**
 * Abstract message -> MAVLink message conversion
 */
template < typename _Msg >
bool pack(const _Msg & msg, mavlink_message_t & pack);

/**
 * Abstract message <- MAVLink message conversion
 */
template < typename _Msg >
bool unpack(_Msg & msg, const mavlink_message_t & pack);

}
