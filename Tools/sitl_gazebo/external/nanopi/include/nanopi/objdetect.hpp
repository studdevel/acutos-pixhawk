#pragma once

#include <nanopi/video_capture.hpp>
#include <nanopi/serialization.hpp>

#include <opencv2/opencv.hpp>

#define NANOPI_MAX_NUM_OF_HULL_POINTS 6

namespace nanopi {

/**
 * Simple configurable object detector.
 */
class objdetect
{
public:
    enum color_space_t { COLOR_SPACE_RGB, COLOR_SPACE_HSV };
    enum object_shape_t { OBJECT_SHAPE_SPHER, OBJECT_SHAPE_RECT };
    /**
     * Detector configuration
     */
    struct config_t
    {
        size_t max_objects;
        bool color_composite;
        cv::Scalar color_low1;
        cv::Scalar color_low2;
        cv::Scalar color_high1;
        cv::Scalar color_high2;
        cv::Scalar color_stddev1;
        cv::Scalar color_stddev2;
        color_space_t color_space;
        object_shape_t object_shape;
        size_t filter_ker_size;
        int min_contour_length;
        float expected_object_size;
        float expected_object_size_delta;
        float max_circle_ratio;
        size_t canny_thres_low;
        size_t canny_thres_high;
        size_t canny_ker_size;
        size_t dilation_ker_size;
        cv::Point2f camera_fov;
        size_t hull_compression_iters;
    };
public:
    /**
     * Structure to store center
     * coordinates and object size in
     */
    struct obj_t
    {
        cv::Point2f center;                                   ///< coordinates of blob center, 0..1
        cv::Point2f hull[NANOPI_MAX_NUM_OF_HULL_POINTS];      ///< coordinates of borders, 0..1
        size_t num_of_hull_points;                            ///< actual size of hull array
        float estimated_distance;                             ///< estimated distance to object, m
    };
    /**
     * Debug output container
     */
    struct debug_t
    {
        cv::Mat mask;     ///< color filter output (binary mask)
        cv::Mat debug;    ///< original frame with some debug graphics
    };
private:
    const config_t _config;
public:
    objdetect(config_t cfg);
public:
    /**
     * Tries to detect `config_t::max_objects` objects
     * on input image and stores them to `res`.
     *
     * If `debug` is specified, debug graphics is prepared
     * and stored to `debug`.
     *
     * Expects BGR or BGRA-encoded frame. Produces RGB
     * 8-bit gray (mask) and RGB-encoded (debug) output.
     *
     * @return `true` if at least one object detected
     */
    bool detect(const cv::Mat & frame,
        std::vector < obj_t > & res,
        debug_t * debug = nullptr);
private:
    /**
     * Retains at most N points of the given convex hull that are
     * the most informative ones -- bunch of N most distant points.
     */
    void retain_n(std::vector < cv::Point2i > & hull, size_t n) const;
    bool detect_spher(const cv::Mat & frame, std::vector < obj_t > & res, debug_t * debug = nullptr);
    bool detect_rect(const cv::Mat & frame, std::vector < obj_t > & res, debug_t * debug = nullptr);
    bool in_range(const cv::Scalar & mean, const cv::Scalar & stddev) const;
private:
    cv::Mat _hsv;
    cv::Mat _mask, _mask_sup;
    cv::Mat _debug;
    cv::Mat _detected_edges;
};

NLOHMANN_JSON_SERIALIZE_ENUM(objdetect::color_space_t, {
    { objdetect::COLOR_SPACE_HSV, "hsv" },
    { objdetect::COLOR_SPACE_RGB, "rgb" }
})

NLOHMANN_JSON_SERIALIZE_ENUM(objdetect::object_shape_t, {
    { objdetect::OBJECT_SHAPE_RECT, "rect" },
    { objdetect::OBJECT_SHAPE_SPHER, "spher" }
})

inline void to_json(json_t & j, const objdetect::config_t & c)
{
    j = {
        { "objects.max", c.max_objects },
        { "filter.color.space", c.color_space },
        { "filter.a.low.color", { (int)c.color_low1[0], (int)c.color_low1[1], (int)c.color_low1[2] } },
        { "filter.a.high.color", { (int)c.color_high1[0], (int)c.color_high1[1], (int)c.color_high1[2] } },
        { "filter.a.stddev.color", { (int)c.color_stddev1[0], (int)c.color_stddev1[1], (int)c.color_stddev1[2] } },
        { "filter.b.low.color", { (int)c.color_low2[0], (int)c.color_low2[1], (int)c.color_low2[2] } },
        { "filter.b.high.color", { (int)c.color_high2[0], (int)c.color_high2[1], (int)c.color_high2[2] } },
        { "filter.b.stddev.color", { (int)c.color_stddev2[0], (int)c.color_stddev2[1], (int)c.color_stddev2[2] } },
        { "blur.ker.size", c.filter_ker_size },
        { "dilation.ker.size", c.dilation_ker_size },
        { "canny.thres.low", c.canny_thres_low },
        { "canny.thres.high", c.canny_thres_high },
        { "canny.ker.size", c.canny_ker_size },
        { "camera.fov", { c.camera_fov.x, c.camera_fov.y } },
        { "object.shape", c.object_shape },
        { "object.size.expected", c.expected_object_size },
        { "object.size.delta", c.expected_object_size_delta },
        { "object.circle.ratio", c.max_circle_ratio },
        { "object.contour.length.min", c.min_contour_length },
        { "object.hull.compr.iters", c.hull_compression_iters },
    };
    if (!c.color_composite)
    {
        j["filter.b.low.color"] = nullptr;
        j["filter.b.high.color"] = nullptr;
        j["filter.b.stddev.color"] = nullptr;
    }
}

inline void from_json(const json_t & j, objdetect::config_t & c)
{
    int buf;
    j.at("objects.max").get_to(c.max_objects);
    j.at("filter.color.space").get_to(c.color_space);
    c.color_composite = !(
        j.at("filter.b.low.color").is_null()  ||
        j.at("filter.b.high.color").is_null() ||
        j.at("filter.b.stddev.color").is_null());
    for (size_t i = 0; i < 3; ++i)
    {
        j.at("filter.a.low.color").at(i).get_to(buf); c.color_low1[i] = (float)buf;
        j.at("filter.a.high.color").at(i).get_to(buf); c.color_high1[i] = (float)buf;
        j.at("filter.a.stddev.color").at(i).get_to(buf); c.color_stddev1[i] = (float)buf;
        if (c.color_composite)
        {
            j.at("filter.b.low.color").at(i).get_to(buf); c.color_low2[i] = (float)buf;
            j.at("filter.b.high.color").at(i).get_to(buf); c.color_high2[i] = (float)buf;
            j.at("filter.b.stddev.color").at(i).get_to(buf); c.color_stddev2[i] = (float)buf;
        }
    }
    j.at("blur.ker.size").get_to(c.filter_ker_size);
    j.at("dilation.ker.size").get_to(c.dilation_ker_size);
    j.at("canny.ker.size").get_to(c.canny_ker_size);
    j.at("canny.thres.low").get_to(c.canny_thres_low);
    j.at("canny.thres.high").get_to(c.canny_thres_high);
    j.at("camera.fov").at(0).get_to(c.camera_fov.x);
    j.at("camera.fov").at(1).get_to(c.camera_fov.y);
    j.at("object.shape").get_to(c.object_shape);
    j.at("object.size.expected").get_to(c.expected_object_size);
    j.at("object.size.delta").get_to(c.expected_object_size_delta);
    j.at("object.circle.ratio").get_to(c.max_circle_ratio);
    j.at("object.contour.length.min").get_to(c.min_contour_length);
    j.at("object.hull.compr.iters").get_to(c.hull_compression_iters);
}

}
