#pragma once

#include <nanopi/serialization.hpp>

#include <mutex>
#include <thread>

#include <opencv2/opencv.hpp>

namespace nanopi {

class video_stream_impl;

class video_stream
{
public:
    struct config_t
    {
        int width;
        int height;
        float framerate;
        int bitrate;
        int quality;
        int udp_port;
        std::string udp_host;
    };
public:
    static config_t make_default_config()
    {
        return { 640, 480, 1.f, 30, 0, 6500, "127.0.0.1" };
    };
private:
    video_stream_impl * _impl;
    config_t _cfg;
public:
    video_stream(config_t cfg);
    ~video_stream();
public:
    bool is_open();
    operator bool() { return is_open(); }
    void close();
    void set_active(bool active);
    bool is_active() const;
public:
    bool supply(const cv::Mat & frame);
private:
    friend class video_stream_impl;
};

inline void to_json(json_t & j, const video_stream::config_t & c)
{
    j = {
        { "width", c.width },
        { "height", c.height },
        { "framerate", c.framerate },
        { "bitrate", c.bitrate },
        { "quality", c.quality },
        { "port.udp", c.udp_port },
        { "host.udp", c.udp_host },
    };
}

inline void from_json(const json_t & j, video_stream::config_t & c)
{
    j.at("width").get_to(c.width);
    j.at("height").get_to(c.height);
    j.at("framerate").get_to(c.framerate);
    j.at("bitrate").get_to(c.bitrate);
    j.at("quality").get_to(c.quality);
    j.at("port.udp").get_to(c.udp_port);
    j.at("host.udp").get_to(c.udp_host);
}

}
