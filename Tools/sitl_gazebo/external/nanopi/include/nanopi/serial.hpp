#pragma once

#include <nanopi/serialization.hpp>

#include <string>

namespace nanopi {

class serial
{
public:
    struct params_t
    {
        std::string dev;
        int baud_rate;
    };
private:
    int _fd;
public:
    serial() : _fd(0) { }
    serial(params_t p) { open(p); }
    serial(const serial &) = delete;
    serial(serial && o) : _fd(o._fd) { o._fd = 0; }
    serial & operator = (const serial &) = delete;
    serial & operator = (serial && o) { _fd = o._fd; o._fd = 0; }
    ~serial() { close(); }
public:
    bool open(params_t p);
    void close();
public:
    bool is_open() const { return _fd != 0; }
    operator bool() const { return is_open(); }
public:
    int get_fd() const { return _fd; }
    operator int() const { return get_fd(); }
};

inline void to_json(json_t & j, const serial::params_t & c)
{
    j = { { "dev", c.dev }, { "baud", c.baud_rate } };
}

inline void from_json(const json_t & j, serial::params_t & c)
{
    j.at("dev").get_to(c.dev);
    j.at("baud").get_to(c.baud_rate);
}

}
