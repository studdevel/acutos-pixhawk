#pragma once

#include <nanopi/objdetect.hpp>

namespace nanopi {

/**
 * Predefined set of parameters for detection of
 * different types of objects.
 */
namespace objparams
{
    enum known_obj_t
    {
        KO_BALLOON, KO_LANDING_POS,
        KO_BALLOON_SITL, KO_LANDING_POS_SITL,
        _KO_MAX
    };

    template < known_obj_t _Obj >
    objdetect::config_t make_config();

    template <> inline objdetect::config_t make_config < KO_BALLOON > ()
    {
        objdetect::config_t cfg = {};
        cfg.max_objects = 3;
        cfg.color_composite = true;
        cfg.color_low1  = {   0,  70,  50 };
        cfg.color_high1 = {  10, 255, 255 };
        cfg.color_low2  = { 170,  70,  50 };
        cfg.color_high2 = { 180, 255, 255 };
        cfg.camera_fov  = { 45, 45 };
        cfg.color_space = objdetect::COLOR_SPACE_HSV;
        cfg.expected_object_size = 0.7;
        cfg.filter_ker_size = 5;
        cfg.object_shape = objdetect::OBJECT_SHAPE_SPHER;
        cfg.min_contour_length = 40;
        cfg.max_circle_ratio = 1.4;
        cfg.hull_compression_iters = 100;
        return cfg;
    }

    template <> inline objdetect::config_t make_config < KO_LANDING_POS > ()
    {
        objdetect::config_t cfg = {};
        cfg.max_objects = 1;
        cfg.color_low1  = { 140, 140, 140 };
        cfg.color_high1 = { 255, 255, 255 };
        cfg.color_stddev1 = { 105, 105, 105 };
        cfg.camera_fov  = { 45, 45 };
        cfg.color_space = objdetect::COLOR_SPACE_RGB;
        cfg.expected_object_size = 1.0;
        cfg.expected_object_size_delta = 0.3;
        cfg.filter_ker_size = 5;
        cfg.dilation_ker_size = 5;
        cfg.canny_thres_low = 100;
        cfg.canny_thres_high = 255;
        cfg.canny_ker_size = 3;
        cfg.object_shape = objdetect::OBJECT_SHAPE_RECT;
        cfg.min_contour_length = 120;
        return cfg;
    }

    template <> inline objdetect::config_t make_config < KO_BALLOON_SITL > ()
    {
        return make_config < KO_BALLOON > ();
    }

    template <> inline objdetect::config_t make_config < KO_LANDING_POS_SITL > ()
    {
        return make_config < KO_LANDING_POS > ();
    }
};

}
