#pragma once
// MESSAGE OPTICAL_RECOGNITION PACKING

#define MAVLINK_MSG_ID_OPTICAL_RECOGNITION 400

MAVPACKED(
typedef struct __mavlink_optical_recognition_t {
 uint64_t time_usec; /*< [us] Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude the number.*/
 float center_x[3]; /*<  X coordinate of i-th object center, 0 .. 1*/
 float center_y[3]; /*<  Y coordinate of i-th object center, 0 .. 1*/
 float border_x[18]; /*<  X coordinates of border points of i-th object (3 blocks of 6 elements), 0 .. 1*/
 float border_y[18]; /*<  Y coordinates of border points of i-th object (3 blocks of 6 elements), 0 .. 1*/
 uint8_t type; /*<  Type of objects this message describes*/
 uint8_t objects; /*<  Number of regognized objects*/
 uint8_t points[3]; /*<  Number of border points per object (4 .. 6)*/
}) mavlink_optical_recognition_t;

#define MAVLINK_MSG_ID_OPTICAL_RECOGNITION_LEN 181
#define MAVLINK_MSG_ID_OPTICAL_RECOGNITION_MIN_LEN 181
#define MAVLINK_MSG_ID_400_LEN 181
#define MAVLINK_MSG_ID_400_MIN_LEN 181

#define MAVLINK_MSG_ID_OPTICAL_RECOGNITION_CRC 33
#define MAVLINK_MSG_ID_400_CRC 33

#define MAVLINK_MSG_OPTICAL_RECOGNITION_FIELD_CENTER_X_LEN 3
#define MAVLINK_MSG_OPTICAL_RECOGNITION_FIELD_CENTER_Y_LEN 3
#define MAVLINK_MSG_OPTICAL_RECOGNITION_FIELD_BORDER_X_LEN 18
#define MAVLINK_MSG_OPTICAL_RECOGNITION_FIELD_BORDER_Y_LEN 18
#define MAVLINK_MSG_OPTICAL_RECOGNITION_FIELD_POINTS_LEN 3

#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_OPTICAL_RECOGNITION { \
    400, \
    "OPTICAL_RECOGNITION", \
    8, \
    {  { "time_usec", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_optical_recognition_t, time_usec) }, \
         { "type", NULL, MAVLINK_TYPE_UINT8_T, 0, 176, offsetof(mavlink_optical_recognition_t, type) }, \
         { "objects", NULL, MAVLINK_TYPE_UINT8_T, 0, 177, offsetof(mavlink_optical_recognition_t, objects) }, \
         { "center_x", NULL, MAVLINK_TYPE_FLOAT, 3, 8, offsetof(mavlink_optical_recognition_t, center_x) }, \
         { "center_y", NULL, MAVLINK_TYPE_FLOAT, 3, 20, offsetof(mavlink_optical_recognition_t, center_y) }, \
         { "points", NULL, MAVLINK_TYPE_UINT8_T, 3, 178, offsetof(mavlink_optical_recognition_t, points) }, \
         { "border_x", NULL, MAVLINK_TYPE_FLOAT, 18, 32, offsetof(mavlink_optical_recognition_t, border_x) }, \
         { "border_y", NULL, MAVLINK_TYPE_FLOAT, 18, 104, offsetof(mavlink_optical_recognition_t, border_y) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_OPTICAL_RECOGNITION { \
    "OPTICAL_RECOGNITION", \
    8, \
    {  { "time_usec", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_optical_recognition_t, time_usec) }, \
         { "type", NULL, MAVLINK_TYPE_UINT8_T, 0, 176, offsetof(mavlink_optical_recognition_t, type) }, \
         { "objects", NULL, MAVLINK_TYPE_UINT8_T, 0, 177, offsetof(mavlink_optical_recognition_t, objects) }, \
         { "center_x", NULL, MAVLINK_TYPE_FLOAT, 3, 8, offsetof(mavlink_optical_recognition_t, center_x) }, \
         { "center_y", NULL, MAVLINK_TYPE_FLOAT, 3, 20, offsetof(mavlink_optical_recognition_t, center_y) }, \
         { "points", NULL, MAVLINK_TYPE_UINT8_T, 3, 178, offsetof(mavlink_optical_recognition_t, points) }, \
         { "border_x", NULL, MAVLINK_TYPE_FLOAT, 18, 32, offsetof(mavlink_optical_recognition_t, border_x) }, \
         { "border_y", NULL, MAVLINK_TYPE_FLOAT, 18, 104, offsetof(mavlink_optical_recognition_t, border_y) }, \
         } \
}
#endif

/**
 * @brief Pack a optical_recognition message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time_usec [us] Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude the number.
 * @param type  Type of objects this message describes
 * @param objects  Number of regognized objects
 * @param center_x  X coordinate of i-th object center, 0 .. 1
 * @param center_y  Y coordinate of i-th object center, 0 .. 1
 * @param points  Number of border points per object (4 .. 6)
 * @param border_x  X coordinates of border points of i-th object (3 blocks of 6 elements), 0 .. 1
 * @param border_y  Y coordinates of border points of i-th object (3 blocks of 6 elements), 0 .. 1
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_optical_recognition_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint64_t time_usec, uint8_t type, uint8_t objects, const float *center_x, const float *center_y, const uint8_t *points, const float *border_x, const float *border_y)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_OPTICAL_RECOGNITION_LEN];
    _mav_put_uint64_t(buf, 0, time_usec);
    _mav_put_uint8_t(buf, 176, type);
    _mav_put_uint8_t(buf, 177, objects);
    _mav_put_float_array(buf, 8, center_x, 3);
    _mav_put_float_array(buf, 20, center_y, 3);
    _mav_put_float_array(buf, 32, border_x, 18);
    _mav_put_float_array(buf, 104, border_y, 18);
    _mav_put_uint8_t_array(buf, 178, points, 3);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_LEN);
#else
    mavlink_optical_recognition_t packet;
    packet.time_usec = time_usec;
    packet.type = type;
    packet.objects = objects;
    mav_array_memcpy(packet.center_x, center_x, sizeof(float)*3);
    mav_array_memcpy(packet.center_y, center_y, sizeof(float)*3);
    mav_array_memcpy(packet.border_x, border_x, sizeof(float)*18);
    mav_array_memcpy(packet.border_y, border_y, sizeof(float)*18);
    mav_array_memcpy(packet.points, points, sizeof(uint8_t)*3);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_OPTICAL_RECOGNITION;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_MIN_LEN, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_LEN, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_CRC);
}

/**
 * @brief Pack a optical_recognition message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time_usec [us] Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude the number.
 * @param type  Type of objects this message describes
 * @param objects  Number of regognized objects
 * @param center_x  X coordinate of i-th object center, 0 .. 1
 * @param center_y  Y coordinate of i-th object center, 0 .. 1
 * @param points  Number of border points per object (4 .. 6)
 * @param border_x  X coordinates of border points of i-th object (3 blocks of 6 elements), 0 .. 1
 * @param border_y  Y coordinates of border points of i-th object (3 blocks of 6 elements), 0 .. 1
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_optical_recognition_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint64_t time_usec,uint8_t type,uint8_t objects,const float *center_x,const float *center_y,const uint8_t *points,const float *border_x,const float *border_y)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_OPTICAL_RECOGNITION_LEN];
    _mav_put_uint64_t(buf, 0, time_usec);
    _mav_put_uint8_t(buf, 176, type);
    _mav_put_uint8_t(buf, 177, objects);
    _mav_put_float_array(buf, 8, center_x, 3);
    _mav_put_float_array(buf, 20, center_y, 3);
    _mav_put_float_array(buf, 32, border_x, 18);
    _mav_put_float_array(buf, 104, border_y, 18);
    _mav_put_uint8_t_array(buf, 178, points, 3);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_LEN);
#else
    mavlink_optical_recognition_t packet;
    packet.time_usec = time_usec;
    packet.type = type;
    packet.objects = objects;
    mav_array_memcpy(packet.center_x, center_x, sizeof(float)*3);
    mav_array_memcpy(packet.center_y, center_y, sizeof(float)*3);
    mav_array_memcpy(packet.border_x, border_x, sizeof(float)*18);
    mav_array_memcpy(packet.border_y, border_y, sizeof(float)*18);
    mav_array_memcpy(packet.points, points, sizeof(uint8_t)*3);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_OPTICAL_RECOGNITION;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_MIN_LEN, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_LEN, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_CRC);
}

/**
 * @brief Encode a optical_recognition struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param optical_recognition C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_optical_recognition_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_optical_recognition_t* optical_recognition)
{
    return mavlink_msg_optical_recognition_pack(system_id, component_id, msg, optical_recognition->time_usec, optical_recognition->type, optical_recognition->objects, optical_recognition->center_x, optical_recognition->center_y, optical_recognition->points, optical_recognition->border_x, optical_recognition->border_y);
}

/**
 * @brief Encode a optical_recognition struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param optical_recognition C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_optical_recognition_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_optical_recognition_t* optical_recognition)
{
    return mavlink_msg_optical_recognition_pack_chan(system_id, component_id, chan, msg, optical_recognition->time_usec, optical_recognition->type, optical_recognition->objects, optical_recognition->center_x, optical_recognition->center_y, optical_recognition->points, optical_recognition->border_x, optical_recognition->border_y);
}

/**
 * @brief Send a optical_recognition message
 * @param chan MAVLink channel to send the message
 *
 * @param time_usec [us] Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude the number.
 * @param type  Type of objects this message describes
 * @param objects  Number of regognized objects
 * @param center_x  X coordinate of i-th object center, 0 .. 1
 * @param center_y  Y coordinate of i-th object center, 0 .. 1
 * @param points  Number of border points per object (4 .. 6)
 * @param border_x  X coordinates of border points of i-th object (3 blocks of 6 elements), 0 .. 1
 * @param border_y  Y coordinates of border points of i-th object (3 blocks of 6 elements), 0 .. 1
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_optical_recognition_send(mavlink_channel_t chan, uint64_t time_usec, uint8_t type, uint8_t objects, const float *center_x, const float *center_y, const uint8_t *points, const float *border_x, const float *border_y)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_OPTICAL_RECOGNITION_LEN];
    _mav_put_uint64_t(buf, 0, time_usec);
    _mav_put_uint8_t(buf, 176, type);
    _mav_put_uint8_t(buf, 177, objects);
    _mav_put_float_array(buf, 8, center_x, 3);
    _mav_put_float_array(buf, 20, center_y, 3);
    _mav_put_float_array(buf, 32, border_x, 18);
    _mav_put_float_array(buf, 104, border_y, 18);
    _mav_put_uint8_t_array(buf, 178, points, 3);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_OPTICAL_RECOGNITION, buf, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_MIN_LEN, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_LEN, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_CRC);
#else
    mavlink_optical_recognition_t packet;
    packet.time_usec = time_usec;
    packet.type = type;
    packet.objects = objects;
    mav_array_memcpy(packet.center_x, center_x, sizeof(float)*3);
    mav_array_memcpy(packet.center_y, center_y, sizeof(float)*3);
    mav_array_memcpy(packet.border_x, border_x, sizeof(float)*18);
    mav_array_memcpy(packet.border_y, border_y, sizeof(float)*18);
    mav_array_memcpy(packet.points, points, sizeof(uint8_t)*3);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_OPTICAL_RECOGNITION, (const char *)&packet, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_MIN_LEN, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_LEN, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_CRC);
#endif
}

/**
 * @brief Send a optical_recognition message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_optical_recognition_send_struct(mavlink_channel_t chan, const mavlink_optical_recognition_t* optical_recognition)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_optical_recognition_send(chan, optical_recognition->time_usec, optical_recognition->type, optical_recognition->objects, optical_recognition->center_x, optical_recognition->center_y, optical_recognition->points, optical_recognition->border_x, optical_recognition->border_y);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_OPTICAL_RECOGNITION, (const char *)optical_recognition, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_MIN_LEN, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_LEN, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_CRC);
#endif
}

#if MAVLINK_MSG_ID_OPTICAL_RECOGNITION_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_optical_recognition_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint64_t time_usec, uint8_t type, uint8_t objects, const float *center_x, const float *center_y, const uint8_t *points, const float *border_x, const float *border_y)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint64_t(buf, 0, time_usec);
    _mav_put_uint8_t(buf, 176, type);
    _mav_put_uint8_t(buf, 177, objects);
    _mav_put_float_array(buf, 8, center_x, 3);
    _mav_put_float_array(buf, 20, center_y, 3);
    _mav_put_float_array(buf, 32, border_x, 18);
    _mav_put_float_array(buf, 104, border_y, 18);
    _mav_put_uint8_t_array(buf, 178, points, 3);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_OPTICAL_RECOGNITION, buf, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_MIN_LEN, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_LEN, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_CRC);
#else
    mavlink_optical_recognition_t *packet = (mavlink_optical_recognition_t *)msgbuf;
    packet->time_usec = time_usec;
    packet->type = type;
    packet->objects = objects;
    mav_array_memcpy(packet->center_x, center_x, sizeof(float)*3);
    mav_array_memcpy(packet->center_y, center_y, sizeof(float)*3);
    mav_array_memcpy(packet->border_x, border_x, sizeof(float)*18);
    mav_array_memcpy(packet->border_y, border_y, sizeof(float)*18);
    mav_array_memcpy(packet->points, points, sizeof(uint8_t)*3);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_OPTICAL_RECOGNITION, (const char *)packet, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_MIN_LEN, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_LEN, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_CRC);
#endif
}
#endif

#endif

// MESSAGE OPTICAL_RECOGNITION UNPACKING


/**
 * @brief Get field time_usec from optical_recognition message
 *
 * @return [us] Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude the number.
 */
static inline uint64_t mavlink_msg_optical_recognition_get_time_usec(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  0);
}

/**
 * @brief Get field type from optical_recognition message
 *
 * @return  Type of objects this message describes
 */
static inline uint8_t mavlink_msg_optical_recognition_get_type(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  176);
}

/**
 * @brief Get field objects from optical_recognition message
 *
 * @return  Number of regognized objects
 */
static inline uint8_t mavlink_msg_optical_recognition_get_objects(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  177);
}

/**
 * @brief Get field center_x from optical_recognition message
 *
 * @return  X coordinate of i-th object center, 0 .. 1
 */
static inline uint16_t mavlink_msg_optical_recognition_get_center_x(const mavlink_message_t* msg, float *center_x)
{
    return _MAV_RETURN_float_array(msg, center_x, 3,  8);
}

/**
 * @brief Get field center_y from optical_recognition message
 *
 * @return  Y coordinate of i-th object center, 0 .. 1
 */
static inline uint16_t mavlink_msg_optical_recognition_get_center_y(const mavlink_message_t* msg, float *center_y)
{
    return _MAV_RETURN_float_array(msg, center_y, 3,  20);
}

/**
 * @brief Get field points from optical_recognition message
 *
 * @return  Number of border points per object (4 .. 6)
 */
static inline uint16_t mavlink_msg_optical_recognition_get_points(const mavlink_message_t* msg, uint8_t *points)
{
    return _MAV_RETURN_uint8_t_array(msg, points, 3,  178);
}

/**
 * @brief Get field border_x from optical_recognition message
 *
 * @return  X coordinates of border points of i-th object (3 blocks of 6 elements), 0 .. 1
 */
static inline uint16_t mavlink_msg_optical_recognition_get_border_x(const mavlink_message_t* msg, float *border_x)
{
    return _MAV_RETURN_float_array(msg, border_x, 18,  32);
}

/**
 * @brief Get field border_y from optical_recognition message
 *
 * @return  Y coordinates of border points of i-th object (3 blocks of 6 elements), 0 .. 1
 */
static inline uint16_t mavlink_msg_optical_recognition_get_border_y(const mavlink_message_t* msg, float *border_y)
{
    return _MAV_RETURN_float_array(msg, border_y, 18,  104);
}

/**
 * @brief Decode a optical_recognition message into a struct
 *
 * @param msg The message to decode
 * @param optical_recognition C-struct to decode the message contents into
 */
static inline void mavlink_msg_optical_recognition_decode(const mavlink_message_t* msg, mavlink_optical_recognition_t* optical_recognition)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    optical_recognition->time_usec = mavlink_msg_optical_recognition_get_time_usec(msg);
    mavlink_msg_optical_recognition_get_center_x(msg, optical_recognition->center_x);
    mavlink_msg_optical_recognition_get_center_y(msg, optical_recognition->center_y);
    mavlink_msg_optical_recognition_get_border_x(msg, optical_recognition->border_x);
    mavlink_msg_optical_recognition_get_border_y(msg, optical_recognition->border_y);
    optical_recognition->type = mavlink_msg_optical_recognition_get_type(msg);
    optical_recognition->objects = mavlink_msg_optical_recognition_get_objects(msg);
    mavlink_msg_optical_recognition_get_points(msg, optical_recognition->points);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_OPTICAL_RECOGNITION_LEN? msg->len : MAVLINK_MSG_ID_OPTICAL_RECOGNITION_LEN;
        memset(optical_recognition, 0, MAVLINK_MSG_ID_OPTICAL_RECOGNITION_LEN);
    memcpy(optical_recognition, _MAV_PAYLOAD(msg), len);
#endif
}
