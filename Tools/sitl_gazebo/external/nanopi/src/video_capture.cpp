#include <nanopi/video_capture.hpp>

namespace nanopi {

video_capture::registry_t video_capture::g_registry =
    video_capture::make_default_registry();

video_capture::video_capture(camera_id_t mode)
    : _mode(mode)
{
    switch (g_registry.cams[mode].source)
    {
        case DEV_SRC_ID:
            _cv_capture.open(g_registry.cams[mode].index);
            break;
        case DEV_SRC_FILE:
            _cv_capture.open(g_registry.cams[mode].filename);
            break;
        default:
            return;
    }
    if (_cv_capture.isOpened())
    {
        size_t pref_width = g_registry.cams[_mode].pref_width;
        size_t pref_height = g_registry.cams[_mode].pref_height;
        if ((pref_width != 0) && (pref_height != 0))
        {
            _cv_capture.set(CV_CAP_PROP_FRAME_WIDTH, pref_width);
            _cv_capture.set(CV_CAP_PROP_FRAME_HEIGHT, pref_height);
        }
    }
}

bool video_capture::capture(cv::Mat & dest)
{
    if (!is_open()) return false;
    if (!_cv_capture.read(dest)) return false;

    size_t pref_width = g_registry.cams[_mode].pref_width;
    size_t pref_height = g_registry.cams[_mode].pref_height;

    if ((pref_width != 0) && (pref_height != 0) &&
        (dest.rows != pref_height || dest.cols != pref_width))
    {
        cv::resize(dest, dest, { (int)pref_width, (int)pref_height });
    }

    if (dest.channels() == 3)
    {
        if (!g_registry.cams[_mode].bgr_source)
            cv::cvtColor(dest, dest, cv::COLOR_RGB2BGR);
    }
    else if (dest.channels() == 4)
    {
        if (!g_registry.cams[_mode].bgr_source)
            cv::cvtColor(dest, dest, cv::COLOR_RGBA2BGR);
        else
            cv::cvtColor(dest, dest, cv::COLOR_BGRA2BGR);
    }
    else return false;

    return true;
}

}
