#include <nanopi/objdetect.hpp>

#define NANOPI_DEBUG_MIN_COLOR_VALUE      0    ///< Minimal value of color channel
#define NANOPI_DEBUG_MAX_COLOR_VALUE      255  ///< Maximal value of color channel
#define NANOPI_DEBUG_ACC_COLOR_VALUE      80   ///< Accent color value
#define NANOPI_DEBUG_CONTOUR_THICKNESS    3    ///< Debug contour thickness

namespace nanopi {

objdetect::objdetect(config_t cfg)
    : _config(cfg)
{
}

bool objdetect::detect(
    const cv::Mat & frame,
    std::vector < obj_t > & res,
    debug_t * debug)
{
    if (_config.object_shape == OBJECT_SHAPE_SPHER)
        return detect_spher(frame, res, debug);
    else
        return detect_rect(frame, res, debug);
}

bool objdetect::detect_spher(
    const cv::Mat & frame,
    std::vector < obj_t > & res,
    debug_t * debug)
{
    bool result_of_detection = false;

    if (_config.color_space == COLOR_SPACE_HSV)
        cv::cvtColor(frame, _hsv, cv::COLOR_BGR2HSV);
    else
        _hsv = frame.clone();

    inRange(_hsv, _config.color_low1, _config.color_high1, _mask);
    if (_config.color_composite)
    {
        inRange(_hsv, _config.color_low2, _config.color_high2, _mask_sup);
        _mask |= _mask_sup;
    }

    if (debug != nullptr) debug->mask = _mask.clone();

    cv::medianBlur(_mask, _mask, _config.filter_ker_size);

    // find contours
    std::vector<std::vector<cv::Point>> contours, hull_envelop;
    cv::findContours(_mask, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

    res.clear();

    int num_of_objects = 0;

    // filter contours by size, shape and other
    for (auto it = contours.begin(); it != contours.end(); ++it)
    {
        size_t contour_length = (*it).size(); // contour length

        if (contour_length <= _config.min_contour_length) continue; // filter by length

        std::vector<cv::Point> hull; // arounding hull
        cv::convexHull(*it, hull);

        cv::Point2f center;
        float radius;
        cv::minEnclosingCircle(hull, center, radius);

        double estimated_distance;

        double object_area = cv::contourArea(hull);
        double theory_object_area = CV_PI * radius * radius;

        double circle_ratio = theory_object_area / object_area;

        if (circle_ratio > _config.max_circle_ratio) continue;

        ++num_of_objects;

        double object_size = radius * 2 * _config.camera_fov.x / frame.cols;
        estimated_distance = (_config.expected_object_size / 2) * std::tan(object_size / 2);

        // fill in object info

        obj_t detected_object;

        detected_object.estimated_distance = estimated_distance;

        detected_object.center.x = center.x / frame.cols;
        detected_object.center.y = center.y / frame.rows;

        retain_n(hull, NANOPI_MAX_NUM_OF_HULL_POINTS);
        cv::convexHull(hull, hull);

        detected_object.num_of_hull_points = hull.size();
        for (size_t i = 0; i < detected_object.num_of_hull_points; ++i)
        {
            detected_object.hull[i].x = (float)hull[i].x / frame.cols;
            detected_object.hull[i].y = (float)hull[i].y / frame.rows;
        }

        res.push_back(detected_object);
        result_of_detection = true;

        hull_envelop.push_back(std::move(hull));

        if (num_of_objects == _config.max_objects) break;
    }

    // draw detected contours
    if (debug != nullptr)
    {
        cv::Mat debug_frame = frame.clone();

        cv::drawContours(debug_frame, hull_envelop, -1,
            cv::Scalar(NANOPI_DEBUG_MIN_COLOR_VALUE, NANOPI_DEBUG_MIN_COLOR_VALUE, NANOPI_DEBUG_MAX_COLOR_VALUE),
            NANOPI_DEBUG_CONTOUR_THICKNESS);

        debug->debug = debug_frame.clone();
    }

    return result_of_detection;
}

bool objdetect::detect_rect(
    const cv::Mat & frame,
    std::vector < obj_t > & res,
    debug_t * debug)
{
    bool result_of_detection = false;

    cv::Mat frame_gray;
    cv::cvtColor(frame, frame_gray, CV_RGB2GRAY);

    // mask of the currently analyzing contour
    _mask = cv::Mat(frame.size(), CV_8U);

    // blurring
    cv::medianBlur(frame_gray, frame_gray, _config.filter_ker_size);

    // canny edge detector
    cv::Canny(frame_gray, _detected_edges, _config.canny_thres_low, _config.canny_thres_high, _config.canny_ker_size);

    // apply dilation - enlarge borders, close contours
    cv::Mat ker = cv::getStructuringElement(cv::MORPH_RECT,
        cv::Size(2 * _config.dilation_ker_size + 1, 2 * _config.dilation_ker_size + 1),
        cv::Point(_config.dilation_ker_size, _config.dilation_ker_size));
    cv::dilate(_detected_edges, _detected_edges, ker);

    if (debug != nullptr) debug->mask = _detected_edges.clone();

    // find all contours, not only external ones
    std::vector<std::vector<cv::Point>> contours, hull_envelop;
    cv::findContours(_detected_edges, contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);

    if (_config.color_space == COLOR_SPACE_HSV)
        cv::cvtColor(frame, _hsv, cv::COLOR_BGR2HSV);

    size_t max_contour = 0;
    double max_area = 0;

    obj_t detected_object;

    res.clear();

    std::vector < cv::Point2i > hull_int(4);

    // filter contours by size, shape and other
    size_t contour = 0;
    for (auto it = contours.begin(); it != contours.end(); ++it, ++contour)
    {
        size_t contour_length = (*it).size();

        if (contour_length <= _config.min_contour_length) continue;

        cv::RotatedRect ellipse = cv::minAreaRect(*it);

        double ellipse_area = ellipse.size.area();

        double contour_area = cv::contourArea(*it);

        // filter by area
        if ((ellipse.size.width / ellipse.size.height > 1 + _config.expected_object_size_delta) ||
            (ellipse.size.width / ellipse.size.height < 1 - _config.expected_object_size_delta) ||
            (ellipse_area / contour_area > 1 + _config.expected_object_size_delta)) continue;

        // create mask of the current contour
        _mask.setTo(0);
        cv::drawContours(_mask, contours, contour, cv::Scalar(255), CV_FILLED);

        // compute mean and stdev for inner contour area color
        cv::Mat frame_roi = _config.color_space == COLOR_SPACE_RGB ? frame : _hsv;
        cv::Scalar roi_mean;
        cv::Scalar roi_stdev;
        cv::meanStdDev(frame_roi, roi_mean, roi_stdev, _mask);

        // range-based color filtration
        if (!in_range(roi_mean, roi_stdev)) continue;

        // select the largest contour
        if (contour_area > max_area)
        {
            max_contour = contour;
            max_area = contour_area;

            cv::Point2f hull[4];
            ellipse.points(hull);

            // fill in object info

            detected_object.center = { ellipse.center.x / frame.cols, ellipse.center.y / frame.rows };

            detected_object.num_of_hull_points = 4;
            for (size_t i = 0; i < detected_object.num_of_hull_points; ++i)
            {
                detected_object.hull[i] = { hull[i].x / frame.cols, hull[i].y / frame.rows };
                hull_int[i] = hull[i];
            }

            double object_size = std::hypot(ellipse.size.width, ellipse.size.height) *
                _config.camera_fov.x / frame.cols;

            detected_object.estimated_distance =
                (_config.expected_object_size / 2) * std::tan(object_size / 2);

            result_of_detection = true;
        }
    }

    if (result_of_detection)
    {
        // calculate more precise contour center
        auto moments = cv::moments(contours[max_contour]);

        float mx, my;
        mx = (float)(moments.m10 / moments.m00 / frame.cols);
        my = (float)(moments.m01 / moments.m00 / frame.rows);

        detected_object.center = { mx, my };

        hull_envelop.push_back(hull_int);
        // fill detected contour with the accent color
        if (debug != nullptr)
        {
            cv::drawContours(debug->mask, contours, max_contour,
                cv::Scalar(NANOPI_DEBUG_ACC_COLOR_VALUE), CV_FILLED);
            cv::drawContours(debug->mask, contours, max_contour,
                cv::Scalar(NANOPI_DEBUG_MAX_COLOR_VALUE), 2 * _config.dilation_ker_size + 1);
        }
        res.push_back(detected_object);
    }

    // draw detected contours
    if (debug != nullptr)
    {
        cv::Mat debug_frame = frame.clone();

        cv::drawContours(debug_frame, hull_envelop, -1,
            cv::Scalar(NANOPI_DEBUG_MIN_COLOR_VALUE, NANOPI_DEBUG_MIN_COLOR_VALUE, NANOPI_DEBUG_MAX_COLOR_VALUE),
            NANOPI_DEBUG_CONTOUR_THICKNESS);

        debug->debug = debug_frame.clone();
    }

    return result_of_detection;
}

void objdetect::retain_n(std::vector < cv::Point2i > & points, size_t n) const
{
	const size_t max_iters = _config.hull_compression_iters;
	if (n >= points.size()) return;

	std::vector < size_t > rand_idx(n), best(n);

	double min_dist_sq = 0;
	for (size_t i = 0; i < max_iters; ++i)
	{
		// generate n random unique indices
		for (size_t j = 0; j < n; ++j)
		{
			for (;;)
			{
				bool ok = true;
				// generate random index
				size_t rnd = rand() % points.size();
				// accept only unique indices
				for (size_t k = 0; k < j; ++k)
					if (rnd == rand_idx[k]) { ok = false; break; }
				if (!ok) continue;
				rand_idx[j] = rnd;
				break;
			}
		}
		// calculate min distance
		bool bad = false;
		double local_min = (std::numeric_limits < double > :: max)();
		for (size_t j = 0; (j < n) && !bad; ++j)
		for (size_t k = 0; (k < n) && !bad; ++k)
		{
			if (j == k) continue;
			auto diff = points[rand_idx[j]] - points[rand_idx[k]];
			double dist = diff.ddot(diff);
			// filter a priori bad point sets
			if (dist < min_dist_sq) { bad = true; break; }
			// record current local min
			if (dist < local_min) { local_min = dist; }
		}
		if (bad) continue;

		// if we are here, the current point set is the best one
		std::swap(rand_idx, best);
		min_dist_sq = local_min;
	}

	std::vector < cv::Point2i > res(n);
	for (size_t i = 0; i < n; ++i)
		res[i] = points[best[i]];

	points = std::move(res);
}

bool objdetect::in_range(const cv::Scalar & mean, const cv::Scalar & stddev) const
{
    for (size_t i = 0; i < 3; ++i)
    {
        if (_config.color_low1[i] <= mean[i] &&
            mean[i] <= _config.color_high1[i] &&
            stddev[i] <= _config.color_stddev1[i]) continue;
        if (_config.color_composite &&
            _config.color_low2[i] <= mean[i] &&
            mean[i] <= _config.color_high2[i] &&
            stddev[i] <= _config.color_stddev2[i]) continue;
        return false;
    }
    return true;
}

}
