#include <nanopi/video_stream.hpp>

#include <sys/unistd.h>

#include <gst/gst.h>

#include <condition_variable>
#include <atomic>

namespace nanopi {

class video_stream_impl
{
private:
    video_stream & _base;
    std::thread _worker;
    std::atomic_bool _active;
    std::atomic_bool _terminating;
    bool _available;
public:
    video_stream_impl(video_stream & base);
    ~video_stream_impl();
public:
    bool supply(const cv::Mat & frame);
    bool is_open() const { return mainLoop != nullptr; }
    bool is_active() const { return _active; }
    void set_active(bool active)
    {
        _active = active;
        suspendResumeCV.notify_one();
    }
private:
    bool prepare();
    void start();
    void release();
    void gst_callback(GstElement * appsrc);
    friend void cb_need_data(GstElement * appsrc, guint unused_size, gpointer user_data);
private:
    GstBuffer * frameBuffer;
    std::mutex frameBufferMutex;
    std::condition_variable suspendResumeCV;
    std::condition_variable dataCV;
    GMainLoop * mainLoop;
    GstClockTime gstTimestamp;
    GstElement * pipeline;
};

void cb_need_data(GstElement *appsrc, guint unused_size, gpointer user_data)
{
    video_stream_impl *self = (video_stream_impl*)user_data;
    self->gst_callback(appsrc);
}

void video_stream_impl::gst_callback(GstElement *appsrc)
{
    GstFlowReturn ret;

    {
        std::unique_lock < std::mutex > lock(frameBufferMutex);

        // sleep until activated
        suspendResumeCV.wait(lock, [this] () {
            return (bool)_active || (bool)_terminating;
        });

        // wait for new frame
        dataCV.wait(lock, [this] () {
            return (frameBuffer != nullptr && _available) || (bool)_terminating;
        });

        // rely on destructor to terminate main loop
        if (_terminating) return;

        // mark frame as not avaliable - it was consumed
        _available = false;

        GST_BUFFER_PTS(frameBuffer) = gstTimestamp;
        GST_BUFFER_DURATION(frameBuffer) = gst_util_uint64_scale_int(1, GST_SECOND, (int)1.f);
        gstTimestamp += GST_BUFFER_DURATION(frameBuffer);

        g_signal_emit_by_name(appsrc, "push-buffer", frameBuffer, &ret);
    }

    if (ret != GST_FLOW_OK)
    {
        g_main_loop_quit(mainLoop);
    }
}

video_stream_impl::video_stream_impl(video_stream & base)
    : _base(base)
    , frameBuffer(nullptr)
    , _active(true)
    , _terminating(false)
    , _available(false)
{
    prepare();
    if (is_open())
    {
        _worker = std::thread(&video_stream_impl::start, this);
    }
}

bool video_stream_impl::prepare()
{
    gst_init(0, 0);

    mainLoop = g_main_loop_new(NULL, FALSE);
    if (!mainLoop) return false;

    pipeline = gst_pipeline_new("sender");
    if (!pipeline) return false;

    GstElement* dataSrc = gst_element_factory_make("appsrc", "AppSrc");
    GstElement* testSrc = gst_element_factory_make("videotestsrc", "FileSrc");
    GstElement* conv    = gst_element_factory_make("videoconvert", "Convert");
    GstElement* encoder = gst_element_factory_make("x264enc", "AvcEncoder");
    GstElement* parser  = gst_element_factory_make("h264parse", "Parser");
    GstElement* payload = gst_element_factory_make("rtph264pay", "PayLoad");
    GstElement* sink    = gst_element_factory_make("udpsink", "UdpSink");
    if (!dataSrc || !testSrc || !conv || !encoder || !parser || !payload || !sink)
    {
        return false;
    }

    // config src
    g_object_set(G_OBJECT(dataSrc), "caps",
        gst_caps_new_simple("video/x-raw",
            "format", G_TYPE_STRING, "I420",
            "width", G_TYPE_INT, _base._cfg.width,
            "height", G_TYPE_INT, _base._cfg.height,
            "framerate", GST_TYPE_FRACTION, (unsigned int)_base._cfg.framerate, 1,
            NULL
        ),
        "is-live", TRUE,
        NULL);

    // config encoder
    g_object_set(G_OBJECT(encoder), "bitrate", _base._cfg.bitrate, NULL);
    g_object_set(G_OBJECT(encoder), "speed-preset", _base._cfg.quality, NULL);

    // config payload
    g_object_set(G_OBJECT(payload), "config-interval", 1, NULL);

    // config udpsink
    g_object_set(G_OBJECT(sink), "host", _base._cfg.udp_host.c_str(), NULL);
    g_object_set(G_OBJECT(sink), "port", _base._cfg.udp_port, NULL);
    g_object_set(G_OBJECT(sink), "sync", false, NULL);
    g_object_set(G_OBJECT(sink), "async", true, NULL);

    // connect all elements to pipeline
    gst_bin_add_many(GST_BIN(pipeline), dataSrc, conv, encoder, parser, payload, sink, NULL);

    // link all elements
    if (gst_element_link_many(dataSrc, conv, encoder, parser, payload, sink, NULL) != TRUE)
    {
        return false;
    }

    // set up appsrc
    g_object_set(G_OBJECT(dataSrc), "stream-type", 0, "format", GST_FORMAT_TIME, NULL);
    g_signal_connect(dataSrc, "need-data", G_CALLBACK(cb_need_data), this);
}

void video_stream_impl::start()
{
    // start
    gst_element_set_state(pipeline, GST_STATE_PLAYING);
    g_main_loop_run(mainLoop);
    gst_element_set_state(pipeline, GST_STATE_NULL);
}

void video_stream_impl::release()
{
    // clean up
    gst_object_unref(GST_OBJECT(pipeline));
    g_main_loop_unref(mainLoop);
    mainLoop = nullptr;
    pipeline = nullptr;
}

bool video_stream_impl::supply(const cv::Mat & frame)
{
    bool result = false;

    {
        std::unique_lock<std::mutex> guard(frameBufferMutex);

        // send new frame only if precious was consumed
        if (_available) return result;
        _available = true;

        if (frameBuffer)
        {
            gst_buffer_unref(frameBuffer);
        }

        cv::Mat res;

        if (_base._cfg.width != 0 && _base._cfg.height != 0)
        {
            if (frame.rows != _base._cfg.height || frame.cols != _base._cfg.width)
            {
                cv::resize(frame, res, { _base._cfg.width, _base._cfg.height });
            }
        }
        if (res.empty()) res = frame;

        // alloc buffer
        guint size = res.rows * res.cols * 1.5;
        frameBuffer = gst_buffer_new_allocate(NULL, size, NULL);

        GstMapInfo mapInfo;
        if (gst_buffer_map(frameBuffer, &mapInfo, GST_MAP_WRITE))
        {
            // color conversion from RGB to YUV
            cv::Mat frameYUV;
            cvtColor(res, frameYUV, cv::COLOR_BGR2YUV_I420);

            memcpy(mapInfo.data, frameYUV.data, size);
            gst_buffer_unmap(frameBuffer, &mapInfo);

            result = true;
        }
    }

    // note: it is permitted to call notify under the lock,
    //       however in some implementations it may be inefficient
    if (result) dataCV.notify_one();

    return result;
}

video_stream_impl::~video_stream_impl()
{
    if(mainLoop)
    {
        _terminating = true;
        suspendResumeCV.notify_one();
        dataCV.notify_one();
        g_main_loop_quit(mainLoop);
        _worker.join();
        release();
    }
}

video_stream::video_stream(config_t cfg) : _cfg(cfg)
{
    _impl = new video_stream_impl(*this);
}

bool video_stream::is_open()
{
    return _impl->is_open();
}

void video_stream::close()
{
    delete _impl;
    _impl = nullptr;
}

void video_stream::set_active(bool active)
{
    _impl->set_active(active);
}

bool video_stream::is_active() const
{
    return _impl->is_active();
}

video_stream::~video_stream()
{
    close();
}

bool video_stream::supply(const cv::Mat & mat)
{
    return _impl->supply(mat);
}

}
