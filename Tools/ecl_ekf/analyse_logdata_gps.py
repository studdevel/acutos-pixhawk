import numpy as np
import pandas as pd

# matplotlib don't use Xwindows backend (must be before pyplot import)
import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

def analyze_stats(v, t, w):
    r = pd.Series(v).rolling(w)
    m = { 'y': v, 'x': t, 'mean': np.roll(r.mean().values, -w//2), 'std': np.roll(r.std().values, -w//2) }
    m.update({
        'std.argmin' : np.nanargmin(m['std']),
        'std.argmax' : np.nanargmax(m['std']),
        'std.min'    : np.nanmin(m['std']),
        'std.max'    : np.nanmax(m['std']),
        'mean.argmin': np.nanargmin(m['mean']),
        'mean.argmax': np.nanargmax(m['mean']),
        'mean.min'   : np.nanmin(m['mean']),
        'mean.max'   : np.nanmax(m['mean'])
    })
    m.update({
        'std.xmin' : m['x'][m['std.argmin']],
        'std.xmax' : m['x'][m['std.argmax']],
        'mean.xmin': m['x'][m['mean.argmin']],
        'mean.xmax': m['x'][m['mean.argmax']]
    })
    m.update({
        'std.min.str' : str(round(m['std.min'], 2)),
        'std.max.str' : str(round(m['std.max'], 2)),
        'mean.min.str': str(round(m['mean.min'], 2)),
        'mean.max.str': str(round(m['mean.max'], 2))
    })
    return m

def plot_stats(plt, s):
    plt.plot(s['x'], s['y'], 'b', linewidth=3.0)
    plt.plot(s['x'], s['mean'], 'g-')
    plt.plot(s['x'], s['mean'] + s['std'], 'r-')
    plt.plot(s['x'], s['mean'] - s['std'], 'r-')
    plt.text(s['std.xmax'], s['mean'][s['std.argmax']] + s['std.max'], 'max=' + s['std.max.str'], fontsize=12, horizontalalignment='left',
                verticalalignment='bottom', color='r')
    plt.text(s['std.xmin'], s['mean'][s['std.argmin']] - s['std.min'], 'min=' + s['std.min.str'], fontsize=12, horizontalalignment='left',
                verticalalignment='top', color='r')
    plt.text(s['mean.xmax'], s['mean.max'], 'max=' + s['mean.max.str'], fontsize=12, horizontalalignment='right',
                verticalalignment='bottom', color='g')
    plt.text(s['mean.xmin'], s['mean.min'], 'min=' + s['mean.min.str'], fontsize=12, horizontalalignment='right',
                verticalalignment='top', color='g')

def analyse_gps(gps,
                ts=5,
                plot=False, output_plot_filename=None):
    
    _c = 0

    if plot:
        pp = PdfPages(output_plot_filename)

    if plot:
        tss = 1e-6 * gps['timestamp']
        ts = ts / (np.max(tss) - np.min(tss))
        ts = int(round(ts * np.size(tss)))

    if plot:
        if ('lat' in gps.keys()) and (
            'lon' in gps.keys()) and (
            'alt' in gps.keys()):
            latstat = analyze_stats(gps['lat'], tss, ts)
            lonstat = analyze_stats(gps['lon'], tss, ts)
            altstat = analyze_stats(gps['alt'], tss, ts)

            plt.figure(_c, figsize=(20, 13))
            plt.subplot(3, 1, 1)
            plot_stats(plt, latstat)
            plt.title('GPS Raw Coordinates')
            plt.ylabel('lattitude (deg)')
            plt.xlabel('time (sec)')
            plt.grid()
            plt.subplot(3, 1, 2)
            plot_stats(plt, lonstat)
            plt.ylabel('longitude (deg)')
            plt.xlabel('time (sec)')
            plt.grid()
            plt.subplot(3, 1, 3)
            plot_stats(plt, altstat)
            plt.ylabel('altitude (deg)')
            plt.xlabel('time (sec)')
            plt.grid()
            pp.savefig()
            plt.close(_c)

    _c += 1

    if plot:
        if ('eph' in gps.keys()) and (
            'epv' in gps.keys()):
            eph = analyze_stats(gps['eph'], tss, ts)
            epv = analyze_stats(gps['epv'], tss, ts)

            plt.figure(_c, figsize=(20, 13))
            plt.subplot(2, 1, 1)
            plot_stats(plt, eph)
            plt.title('GPS Reported Accuracy')
            plt.ylabel('horizontal (meters)')
            plt.xlabel('time (sec)')
            plt.grid()
            plt.subplot(2, 1, 2)
            plot_stats(plt, epv)
            plt.ylabel('vertical (deg)')
            plt.xlabel('time (sec)')
            plt.grid()
            pp.savefig()
            plt.close(_c)

    _c += 1

    if plot:
        if ('hdop' in gps.keys()) and (
            'vdop' in gps.keys()):
            hdop = analyze_stats(gps['hdop'], tss, ts)
            vdop = analyze_stats(gps['vdop'], tss, ts)

            plt.figure(_c, figsize=(20, 13))
            plt.subplot(2, 1, 1)
            plot_stats(plt, hdop)
            plt.title('GPS Reported Coordinate Dilution')
            plt.ylabel('horizontal (meters)')
            plt.xlabel('time (sec)')
            plt.grid()
            plt.subplot(2, 1, 2)
            plot_stats(plt, vdop)
            plt.ylabel('vertical (deg)')
            plt.xlabel('time (sec)')
            plt.grid()
            pp.savefig()
            plt.close(_c)

    _c += 1

    if plot:
        if ('noise_per_ms' in gps.keys()) and (
            'satellites_used' in gps.keys()):
            noise_per_ms = analyze_stats(gps['noise_per_ms'], tss, ts)
            satellites_used = analyze_stats(gps['satellites_used'], tss, ts)

            plt.figure(_c, figsize=(20, 13))
            plt.subplot(2, 1, 1)
            plot_stats(plt, noise_per_ms)
            plt.title('GPS Reported Noise per ms')
            plt.ylabel('noise')
            plt.xlabel('time (sec)')
            plt.grid()
            plt.subplot(2, 1, 2)
            plot_stats(plt, satellites_used)
            plt.title('GPS Satellites')
            plt.ylabel('satellites (num)')
            plt.xlabel('time (sec)')
            plt.grid()
            pp.savefig()
            plt.close(_c)

    if plot:
            # close the pdf file
            pp.close()
            # don't display to screen
            # plt.show()
            # clase all figures
            plt.close("all")
