#! /usr/bin/env python

from __future__ import print_function

import argparse
import os, sys

from pyulog import *

from analyse_logdata_gps import analyse_gps

"""
Like xxx_ekf.py, performs gps signal analysis. Produces a number of plots
showing raw data, its mean value and its variance.
"""

parser = argparse.ArgumentParser(description='Analyse the gps quality')
parser.add_argument('filename', metavar='file.ulg', help='ULog input file')
parser.add_argument('--no-plots', action='store_true',
                    help='Whether to only analyse and not plot the summaries for developers.')
parser.add_argument('--time-window', type=float, default=5.0,
                    help='The time window in seconds to use in rolling mean and std calculation.')

def is_valid_directory(parser, arg):
    if os.path.isdir(arg):
        # Directory exists so return the directory
        return arg
    else:
        parser.error('The directory {} does not exist'.format(arg))

args = parser.parse_args()

## load the log and extract the necessary data for the analyses
ulog = ULog(args.filename, None)
data = ulog.data_list

# extract data from EKF status message
vehicle_gps_position_data = {}
try:
    vehicle_gps_position_data = ulog.get_dataset('vehicle_gps_position').data
except (KeyError, IndexError, ValueError) as error:
    print(type(error), "(vehicle_gps_position):", error)

# perform the gps analysis
analyse_gps(
    vehicle_gps_position_data,
    args.time_window,
    plot=not args.no_plots, output_plot_filename=args.filename + ".pdf")
